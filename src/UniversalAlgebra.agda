module _ where

open import Agda.Builtin.Equality
open import Agda.Builtin.Nat
open import Data.Bool
open import Data.Fin using (Fin ; fromℕ ; zero ; suc)
open import Data.Nat
open import Data.Nat.Properties using (+-assoc)
open import Data.Product using (proj₁ ; proj₂ ; _×_)
open import Data.Vec using (Vec ; _∷_ ; [] ; map ; lookup)
open import Function using (case_of_)
open import Level using () renaming (suc to lsuc)
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; trans; sym; cong; cong-app; subst; cong₂)
open Eq.≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)

n-ary : (A : Set) (n : Nat) → Set
n-ary A zero = A
n-ary A (suc n) = A → (n-ary A n)

-- An indexed type, which represents the arity of the symbol.
Language : Set₁
Language = Nat → Set

data Exists (A : Set) : Set where
  exists : ∀ {a : A} → Exists A

-- A language, a set, and an interpretation of the symbols.
record Algebra (F : Language) (U : Set) : Set where
  constructor algebra
  field
    interp : ∀ {n} (f : F n) → n-ary U n
    witness : U -- to make sure A is non-empty.

app-vec : ∀ {U n} → n-ary U n → Vec U n → U
app-vec f [] = f
app-vec f (x ∷ xs) = app-vec (f x) xs

-- interprets the application of a symbol.
evalSymbol : ∀ {F U n}
  → Algebra F U
  → (f : F n)
  → Vec U n
  → U
evalSymbol alg f args = app-vec (Algebra.interp alg f) args

data Zip {A B : Set} (f : A → B → Set) : (n : Nat) → (a : Vec A n) → (b : Vec B n) → Set where
  [] : Zip f 0 [] []
  _∷_ : ∀ {n a b as bs} → f a b → Zip f n as bs → Zip f (suc n) (a ∷ as) (b ∷ bs)

record Equivalence {A : Set} (_∼_ : A → A → Set): Set where
  constructor equivalence
  field
    crefl : ∀ {a} → a ∼ a
    csym : ∀ {a b} → a ∼ b → b ∼ a
    ctrans : ∀ {a b c} → a ∼ b → b ∼ c → a ∼ c

record Congruence {F A}
  (𝔸 : Algebra F A) (_∼_ : A → A → Set): Set where
  constructor congruence
  field
    cequiv : Equivalence _∼_
    compat : ∀ {n} (f : F n) (as bs : Vec A n)
      (r : Zip _∼_ n as bs)
     → evalSymbol 𝔸 f as ∼ evalSymbol 𝔸 f bs

-- A term of a language with a set of variables.
data Term {F : Language} {X} : Set where
  var : X → Term
  app : ∀ {n} (f : F n) → Vec (Term {F} {X}) n → Term

-- interprets a term in an algebra using an assignation of variables.
evalTerm : ∀ {F U X}
   → Algebra F U
   → (X → U)
   → Term {F} {X}
   → U
evalTerm alg assig (var x) = assig x
evalTerm {F} {U} {X} alg assig (app f args) = aux (Algebra.interp alg f) args
  where aux : ∀ {n} → (n-ary U n) → Vec (Term {F} {X}) n → U
        aux ac [] = ac
        aux ac (x ∷ v) = aux (ac (evalTerm alg assig x)) v

data Homomorphism {F A B}
  (𝔸 : Algebra F A)
  (𝔹 : Algebra F B)
  (α : A → B) : Set₁ where
  homomorphism :
   (∀ {n} → (f : F n)
   → (as : Vec A n)
   → α (evalSymbol 𝔸 f as) ≡ evalSymbol 𝔹 f (map α as))
   → Homomorphism 𝔸 𝔹 α

_∘_ : ∀ {A B C : Set} → (B → C) → (A → B) → A → C
f ∘ g = λ x → f (g x)

map-functor : ∀ {n} {A B C : Set} (f : B → C) (g : A → B) (v : Vec A n) → map (f ∘ g) v ≡ map f (map g v)
map-functor f g [] = refl
map-functor f g (x ∷ v) = cong (λ as → f (g x) ∷ as) (map-functor f g v)

-- Theorem: 6.5 (Burris): Composition of homomorphisms gives a homomorphism.
-- TODO: use eqational reasoning module.
homo-∘ : ∀ {F A B C α β}
  {𝔸 : Algebra F A}
  {𝔹 : Algebra F B}
  {ℂ : Algebra F C}
  (h : Homomorphism 𝔸 𝔹 α)
  → (g : Homomorphism 𝔹 ℂ β)
  → Homomorphism 𝔸 ℂ (β ∘ α)
homo-∘ {α = α} {β = β} {ℂ = ℂ} (homomorphism homo-a) (homomorphism homo-b)
  = homomorphism λ { f as →
  let hypa = homo-a f as
      hypb = homo-b f (map α as)
      s1 = cong (evalSymbol ℂ f) (map-functor β α as)
      s2 = trans s1 (sym hypb)
      s3 = sym (cong β hypa)
  in sym (trans s2 s3)}

-- Kernel of a function as a relation.
data Ker {A B : Set} (h : A → B) : A → A → Set where
  ker : ∀ {a b} → h a ≡ h b → Ker h a b

ker-equiv : ∀ {A B : Set} → (α : A → B) → Equivalence (Ker α)
ker-equiv α = equivalence (ker refl)
  (λ { (ker x) → ker (sym x)})
  (λ { (ker x) (ker y) → ker (trans x y)})

-- Theorem 6.8:
-- Let α : 𝔸 → 𝔹 be a homomorphism. Then ker(α) is a congruence on 𝔸.
thm-6,8 : ∀ {F A B α}
  {𝔸 : Algebra F A}
  {𝔹 : Algebra F B}
  (h : Homomorphism 𝔸 𝔹 α)
  → Congruence 𝔸 (Ker α)
thm-6,8 {α = α} {𝔸 = 𝔸} {𝔹 = 𝔹} (homomorphism homo) = congruence
  (ker-equiv α)
  λ f as bs r → ker let
    s1 = homo f as
    s2 = cong (evalSymbol 𝔹 f) (aux as bs α r)
    s3 = trans s1 s2
    s4 = sym (homo f bs)
    in trans s3 s4
  where
    aux : ∀ {n} {A B : Set} →
      (a b : Vec A n) → (f : A → B) → (r : Zip (Ker f) n a b) → map f a ≡ map f b
    aux [] [] f [] = refl
    aux (x ∷ a) (y ∷ b) f (ker fxy ∷ r) = cong₂ _∷_ fxy (aux a b f r)

record ProofSurj {A B : Set} (f : A → B) (b : B) : Set where
  inductive
  constructor proof-surj
  field
    inv : A
    prf : f inv ≡ b → ProofSurj f b

data Surjective {A B : Set} (f : A → B) : Set where
  surjective : (b : B) → ProofSurj f b → Surjective f

data Injective {A B : Set} (f : A → B) : Set where
  injective : ((a : A) → (b : A) → (f a ≡ f b) → a ≡ b) → Injective f

record Bijective {A B : Set} (f : A → B) (f⁻¹ : B → A) : Set where
  constructor bijective
  field
    injec : Injective f
    surjec : Surjective f

record Order {A : Set} (_≤_ : A → A → Set) : Set where
  constructor order
  field
    crefl : ∀ {a : A} → a ≤ a
    cantisym : ∀ {a b : A} → a ≤ b → b ≤ a → a ≡ b
    ctrans : ∀ {a b c : A} → a ≤ b → b ≤ c → a ≤ c

-- Subset of a set defined by a characteristic function.
data Subset (Univ : Set) (c : Univ → Bool): Set where
   subset : Subset Univ c

-- Any member of a subset.
data MemSubset {U} {c} (S : Subset U c) : Set where
  member : (u : U) → c u ≡ true → MemSubset S

unsubset : ∀ {U c} → {S : Subset U c} → MemSubset S → U
unsubset (member u x) = u

-- A proof that an element belongs to a subset.
data InSubset {U} {c} (u : U) (S : Subset U c) : Set where
  proof : c u ≡ true → InSubset u S


data Subalgebra {F U c}
  {𝔸 : Algebra F U}
  {S : Subset U c} : Set where
  subalgebra : ∀ {n} (f : F n) → (args : Vec (MemSubset S) n) → InSubset (evalSymbol 𝔸 f (map unsubset args)) S → Subalgebra

even : Nat → Bool
even zero = true
even (suc n) = not (even n)


-- Example: Associative property
-- associative : ∀ {F τ U X L S}
--   → (Algebra : Algebra {F} {τ} {U} {L} S)
--   → (f : F) → τ f ≡ 2 → (X → U) → (a b c : Term) → Set
-- associative alg op ari v a b c =
--    evalTerm alg v (app-n op ari ((app-n op ari (a ∷ b ∷ [])) ∷ c ∷ []))
--  ≡ evalTerm alg v (app-n op ari (a ∷ (app-n op ari (b ∷ c ∷ [])) ∷ []))


-- Example: Semigroup
-- record Semigroup {F τ U L S} (Algebra : Algebra {F} {τ} {U} {L} S) (op : F) : Set₁ where
--   constructor semigroup
--   field
--     ari : τ op ≡ 2
--     assoc : ∀ {X}
--       → (v : X → U)
--       → (a b c : Term)
--       → associative Algebra op ari v a b c

-- Example: Peano algebra with plus
data PeanoSymb : Nat → Set where
    szero : PeanoSymb 0
    ssuc : PeanoSymb 1
    splus : PeanoSymb 2

algebraPeano : Algebra PeanoSymb Nat
algebraPeano = algebra interp 0
  where
    interp : ∀ {n} (f : PeanoSymb n) → n-ary Nat n
    interp szero = zero
    interp ssuc = suc
    interp splus = _+_

peano-+-assoc : {X : Set} (v : X → ℕ) (a b c : Term) →
  (evalTerm algebraPeano v a + evalTerm algebraPeano v b) + evalTerm algebraPeano v c
  ≡ evalTerm algebraPeano v a + (evalTerm algebraPeano v b + (evalTerm algebraPeano v c))
peano-+-assoc v a b c = +-assoc (evalTerm algebraPeano v a) (evalTerm algebraPeano v b) (evalTerm algebraPeano v c)

-- monoidPeano : Semigroup algebraPeano splus
-- monoidPeano = semigroup refl peano-+-assoc

data EquivalenceClass {A : Set} {_∼_ : A → A → Set} (a : A) (e : Equivalence _∼_) : Set where
  equivClass : (b : A) → a ∼ b → EquivalenceClass a e

data QuotientSet (A : Set) {_∼_ : A → A → Set} (e : Equivalence _∼_) : Set where
  class : (a : A) → EquivalenceClass a e → QuotientSet A e

postulate
  representative : ∀ {A _∼_} → (a b : A) → (e : Equivalence _∼_)
    → a ∼ b → EquivalenceClass a e ≡ EquivalenceClass b e

natural-map : ∀ {A _∼_} (e : Equivalence _∼_) → (a : A) → QuotientSet A e
natural-map e a = class a (equivClass a (Equivalence.crefl e))

natural-homo : ∀ {A B F} {α : A → B}
  {𝔸 : Algebra F A}
  {𝔸/a : Algebra F (QuotientSet A (ker-equiv α))}
  {θ : Congruence 𝔸 (Ker α)}
 → Homomorphism 𝔸 𝔸/a (natural-map (ker-equiv α))
natural-homo {α = α} {θ = congruence cequiv compat}
  = homomorphism λ { f as → {!!} }

thm-6,12 : ∀ {A B F α tbd}
  {𝔸 : Algebra F A}
  {𝔹 : Algebra F B}
  {𝔸/Kerα : Algebra F (QuotientSet A (ker-equiv α))}
  (h : Homomorphism 𝔸 𝔹 α)
  → Surjective α
  → Homomorphism 𝔸/Kerα 𝔹 tbd
  → Nat
thm-6,12 = {!!}
