
open import Relation.Binary.PropositionalEquality using (_≡_)
open import Relation.Binary.Definitions using () renaming (Decidable to Decidable₂)
open import Relation.Nullary using (yes; no; Dec; ¬_)

module DecMultiset {Carrier : Set} (_≟_ : Decidable₂ {A = Carrier} _≡_) where

open import Agda.Builtin.Nat using (Nat; suc; zero; _+_; _-_)
open import Agda.Builtin.Unit using (tt; ⊤)
open import Data.Bool using (Bool; false; true; not; T)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.List using (List; []; _∷_; map; _++_; sum; replicate; drop)
open import Data.List.Relation.Unary.Any using (Any; here; there; tail; any)
open import Data.Product using (Σ; proj₁; proj₂; _×_; _,_)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function using (id; const; _∘_)
open import Relation.Binary using (REL; Rel; IsDecEquivalence; IsEquivalence)
open import Relation.Binary.Definitions using (Reflexive; Symmetric; Transitive; Antisymmetric; Minimum)
open import Relation.Binary.Lattice using (Infimum; IsMeetSemilattice)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; setoid; isDecEquivalence; cong; subst; sym; trans; module ≡-Reasoning)
open import Relation.Binary.Structures using (IsPartialOrder; IsPreorder)
open import Relation.Unary.Properties using (∁?)

open import Relation.Binary.Reasoning.MultiSetoid using ()

open ≡-Reasoning using (begin_; _≡⟨⟩_; step-≡; _∎)

open import Data.List.Membership.Setoid (setoid Carrier) using (_∈_; _∉_) public
open import Data.List.Membership.DecPropositional {A = Carrier} (_≟_) using (_∈?_) public

Multiset : Set
Multiset = List Carrier

-- Deletes the first occurrence of A. Note that it does not necessarily replace
-- the occurrence pointed by A ∈ S. This way some proofs become significantly
-- easier (or even possible), however, we then need decidable equality.
del : ∀ (A : Carrier) (S : Multiset) (_ : A ∈ S) → Multiset
del A (B ∷ S) (here px) = S
del A (B ∷ S) (there x) with A ≟ B
... | yes refl = S
... | no ne = B ∷ (del A S x)

∈→∉→¬≡ : ∀ {A B L} → A ∈ L → B ∉ L → ¬ (A ≡ B)
∈→∉→¬≡ x x₁ refl = x₁ x

del-cases : ∀ (A B S P) → (A ≡ B × del A (B ∷ S) P ≡ S) ⊎ (¬ A ≡ B × Σ (A ∈ S) (λ P' → del A (B ∷ S) P ≡ B ∷ (del A S P')))
del-cases A B S (here px) = inj₁ (px , refl)
del-cases A B S (there P) with A ≟ B
... | yes refl = inj₁ (refl , refl)
... | no ab = inj₂ (ab , (P , refl))


del-head : ∀ {A S} (P) → del A (A ∷ S) P ≡ S
del-head {A} {S} P with del-cases A A S P
... | inj₁ (fst , snd) = snd
... | inj₂ (fst , y) = ⊥-elim (fst refl)


del-∈-irrel : ∀ {A S} (P P' : A ∈ S) → del A S P ≡ del A S P'
del-∈-irrel {A} {B ∷ S} P P' with del-cases A B S P | del-cases A B S P'
... | inj₁ (refl , y1) | inj₁ (refl , y) = trans y1 (sym y)
... | inj₁ (refl , y) | inj₂ (ab , _) = ⊥-elim (ab refl)
... | inj₂ (ab , _) | inj₁ (refl , y) = ⊥-elim (ab refl)
... | inj₂ (_ , (P1 , b)) | inj₂ (ab , (P2 , c)) =
  trans (trans b (cong (B ∷_) (del-∈-irrel P1 P2))) (sym c)


del-tail' : ∀ {A B R} (ab : ¬ A ≡ B) (P P') → del A (B ∷ R) P ≡ B ∷ del A R P'
del-tail' {A} {B} {R} ab P P' with del-cases A B R P
... | inj₁ (fst , snd) = ⊥-elim (ab fst)
... | inj₂ (fst , p , snd) = trans snd (cong (B ∷_) (del-∈-irrel p P'))

del-tail : ∀ {A B R} (ab : ¬ A ≡ B) (P) → del A (B ∷ R) P ≡ B ∷ del A R (tail ab P)
del-tail ab P = del-tail' ab P (tail ab P)

infix 0 _⊆_
_⊆_ : Multiset → Multiset → Set
[] ⊆ B = ⊤
x ∷ A ⊆ B = Σ (x ∈ B) (λ P → A ⊆ (del x B P))

del-S-subst : ∀ {S S' A P P'} → S ≡ S' → del A S P ≡ del A S' P'
del-S-subst {_} {_} {_} {P} {P'} refl = del-∈-irrel P P'

del-commut : ∀ {A B S} (PA PB PB' PA') → del B (del A S PA) PB ≡ del A (del B S PB') PA'
del-commut {A} {B} {C ∷ S} PA PB PB' PA' with del-cases A C S PA | del-cases B C S PB'
... | inj₁ (refl , snd) | inj₁ (refl , snd') =
  begin
  del A (del A (A ∷ S) PA) PB ≡⟨ del-S-subst snd ⟩
  del A S PBS ≡⟨ del-S-subst (sym snd') ⟩
  del A (del A (A ∷ S) PB') PA' ∎
  where PBS : A ∈ S
        PBS = subst (A ∈_) snd PB
... | inj₁ (refl , snd) | inj₂ (bc , P , snd') =
  begin
  del B (del A (A ∷ S) PA) PB ≡⟨ del-S-subst (del-head PA) ⟩
  del B S P ≡⟨⟩
  del A (A ∷ del B S P) (here refl) ≡⟨ del-S-subst {P = here refl} (sym snd') ⟩
  del A (del B (A ∷ S) PB') PA' ∎
... | inj₂ (ab , P' , snd) | inj₁ (refl , snd') =
    begin
    del B (del A (B ∷ S) PA) PB ≡⟨ del-S-subst {P' = here refl} (del-tail ab PA) ⟩
    del B (B ∷ del A S (tail ab PA)) (here refl) ≡⟨⟩
    del A S (tail ab PA) ≡⟨ del-S-subst {S} {del B (B ∷ S) PB'} (sym (del-head PB')) ⟩
    del A (del B (B ∷ S) PB') PA' ∎
... | inj₂ (ac , P' , snd) | inj₂ (bc , P , snd') = begin
    del B (del A (C ∷ S) PA) PB ≡⟨ del-S-subst {P' = TB} (del-tail ac PA) ⟩
    del B (C ∷ del A S TA) TB ≡⟨ del-tail bc (subst (B ∈_) (del-tail ac PA) PB) ⟩
    C ∷ del B (del A S TA) TB' ≡⟨ cong (C ∷_) (del-commut TA TB' P TA') ⟩
    C ∷ del A (del B S P) TA' ≡⟨ sym (del-tail' ac TA2 TA') ⟩
    del A (C ∷ del B S P) TA2 ≡⟨ del-S-subst {P = TA2} {P' = PA'} (sym (del-tail' bc PB' P)) ⟩
    del A (del B (C ∷ S) PB') PA' ∎
    where TA : A ∈ S
          TA = tail ac PA
          TA2 : A ∈ C ∷ del B S P
          TA2 = subst (A ∈_) (del-tail' bc PB' P) PA'
          TA' : A ∈ del B S P
          TA' = tail ac TA2
          TB : B ∈ (C ∷ del A S TA)
          TB = subst (B ∈_) (del-tail ac PA) PB
          TB' : B ∈ del A S TA
          TB' = tail bc TB

∈-del-l : ∀ {A B S P} → A ∈ del B S P → A ∈ S
∈-del-l {A} {B} {C ∷ R} {P} x with del-cases B C R P
... | inj₁ (refl , snd) = there (subst (A ∈_) snd x)
... | inj₂ (fst , P' , snd) with A ≟ C
... | yes refl = here refl
... | no ac = there (∈-del-l (tail ac (subst (A ∈_) snd x)))

∈-del-r : ∀ {A B S P} → ¬ A ≡ B → A ∈ S → A ∈ del B S P
∈-del-r {A} {B} {C ∷ S} {P} ab y with del-cases B C S P
... | inj₁ (refl , snd) = subst (A ∈_) (sym snd) (tail ab y)
... | inj₂ (bc , P' , snd) with A ≟ C
... | yes refl = subst (C ∈_) (sym snd) (here refl)
... | no ac = subst (A ∈_) (sym (del-tail bc P)) aux
  where aux : A ∈ C ∷ del B S (tail bc P)
        aux = there (∈-del-r ab (tail ac y))

⊆-del-r : ∀ {A S R P} → S ⊆ del A R P → S ⊆ R
⊆-del-r {A} {[]} {R} {P} x = tt
⊆-del-r {A} {B ∷ S} {R} {P} (fst , snd) with A ≟ B
... | yes refl = P , ⊆-del-r {A} {S} {del A R P} {fst} snd
... | no ab = ∈-del-l fst , ⊆-del-r aux
  where aux : S ⊆ del A (del B R (∈-del-l fst)) (∈-del-r ab P)
        aux = subst (S ⊆_) (del-commut P _ _ _) snd

⊆-del-l : ∀ {A S R P} → S ⊆ R → del A S P ⊆ R
⊆-del-l {A} {B ∷ S} {R} {P} (fst , snd) with del-cases A B S P
... | inj₁ (refl , snd') = subst (_⊆ R) (sym (del-head P)) (⊆-del-r snd)
... | inj₂ (ab , P' , snd') = subst (_⊆ R) (sym (del-tail' ab P P')) aux
  where aux : B ∷ del A S P' ⊆ R
        aux = fst , ⊆-del-l snd


∈→⊆→∈ : ∀ {A S R} → A ∈ S → S ⊆ R → A ∈ R
∈→⊆→∈ {A} {B ∷ S} {R} x (P , snd) with A ≟ B
... | yes refl = P
... | no ab = ∈→⊆→∈ (tail ab x) (⊆-del-r snd)


⊆-del' : ∀ {A S R} (P P') → (SR : S ⊆ R) → del A S P ⊆ del A R P'
⊆-del' {A} {B ∷ S} {R} P P' (B∈R , snd') with del-cases A B S P
... | inj₁ (refl , snd) = subst (_⊆ del A R P') (sym snd) (subst (S ⊆_) (del-∈-irrel B∈R P') snd')
... | inj₂ (ab , T , snd) = subst (_⊆ del A R P') (sym snd) aux
  where aux' : del A S T ⊆ del A (del B R B∈R) (∈→⊆→∈ T snd')
        aux' = ⊆-del' T (∈→⊆→∈ T snd') snd'
        aux : B ∷ del A S T ⊆ del A R P'
        aux = ∈-del-r (ab ∘ sym) B∈R , subst (del A S T ⊆_) (del-commut B∈R _ _ _) aux'

⊆-del : ∀ {A S R} (P) → (SR : S ⊆ R) → del A S P ⊆ del A R (∈→⊆→∈ P SR)
⊆-del P SR = ⊆-del' P (∈→⊆→∈ P SR) SR

⊆-refl : Reflexive _⊆_
⊆-refl {[]} = tt
⊆-refl {A ∷ S} with A ≟ A
... | yes refl = (here refl) , ⊆-refl
... | no a = ⊥-elim (a refl)

⊆-trans : Transitive _⊆_
⊆-trans {[]} {R} {T} x y = tt
⊆-trans {A ∷ S} {R} {T} (fst , snd) y = ∈→⊆→∈ fst y , ⊆-trans snd (⊆-del fst y)

⊆-min : Minimum _⊆_ []
⊆-min x = tt

infix 0 _⊆⊇_
_⊆⊇_ : Multiset → Multiset → Set
R ⊆⊇ S = (R ⊆ S) × (S ⊆ R)

⊆⊇-refl : Reflexive _⊆⊇_
⊆⊇-refl = ⊆-refl , ⊆-refl

⊆⊇-symm : Symmetric _⊆⊇_
⊆⊇-symm (a , b) = b , a

⊆⊇-trans : Transitive _⊆⊇_
⊆⊇-trans (fst , snd) (fst' , snd') = ⊆-trans fst fst' , ⊆-trans snd' snd

⊆-antiSymm : Antisymmetric _⊆⊇_ _⊆_
⊆-antiSymm x y = x , y

⊆⊇-equiv : IsEquivalence _⊆⊇_
⊆⊇-equiv = record { refl = ⊆⊇-refl ; sym = ⊆⊇-symm ; trans = ⊆⊇-trans }

⊆-preorder : IsPreorder _⊆⊇_ _⊆_
⊆-preorder = record { isEquivalence = ⊆⊇-equiv ; reflexive = proj₁ ; trans = ⊆-trans }

⊆-partialOrder : IsPartialOrder _⊆⊇_ _⊆_
⊆-partialOrder = record { isPreorder = ⊆-preorder ; antisym = ⊆-antiSymm }

_∪_ : Multiset → Multiset → Multiset
_∪_ = _++_

_∩_ : Multiset → Multiset → Multiset
[] ∩ R = []
(A ∷ S) ∩ R with A ∈? R
... | yes P = A ∷ S ∩ (del A R P)
... | no n = S ∩ R

∩-cases : ∀ (A S R) → (A ∉ R × (A ∷ S) ∩ R ≡ S ∩ R) ⊎
  (Σ (A ∈ R) (λ P → (A ∷ S) ∩ R ≡ A ∷ (S ∩ del A R P)))
∩-cases A S R with A ∈? R
... | yes P = inj₂ (P , refl)
... | no nP = inj₁ (nP , refl)

⊆-cons-r : ∀ {A S R} → S ⊆ R → S ⊆ A ∷ R
⊆-cons-r {A} {S} {R} x = ⊆-trans x (aux A R)
  where
    aux : ∀ (A S) → S ⊆ A ∷ S
    aux A [] = tt
    aux A (B ∷ S) with del-cases B A (B ∷ S) (there (here refl))
    ... | inj₁ (refl , snd) = (here refl) , aux A S
    ... | inj₂ (fst , P , snd) = there (here refl) , subst (S ⊆_) (sym aux') (aux A S)
      where aux' : del B (A ∷ B ∷ S) (there (here refl)) ≡ A ∷ S
            aux' = begin
              del B (A ∷ B ∷ S) (there (here refl)) ≡⟨ snd ⟩
              A ∷ del B (B ∷ S) P ≡⟨ cong (A ∷_) (del-head P) ⟩
              A ∷ S ∎

⊆-cons : ∀ {A S R} → S ⊆ R → A ∷ S ⊆ A ∷ R
⊆-cons x = here refl , x

∩→∈×∈ : ∀ {A S R} → A ∈ S ∩ R → A ∈ S × A ∈ R
∩→∈×∈ {A} {B ∷ S} {R} x with ∩-cases B S R
... | inj₁ (fst , snd) = there (proj₁ A∈S,A∈R) , proj₂ A∈S,A∈R
  where A∈S,A∈R = ∩→∈×∈ {A} {S} (subst (A ∈_) snd x)
... | inj₂ (fst , snd) with subst (A ∈_) snd x
... | here refl = here refl , fst
... | there th = there (proj₁ r) , ∈-del-l (proj₂ r)
  where r = ∩→∈×∈ {A} {S} th


∩-cons-∉l : ∀ {A S R} → A ∉ R → (A ∷ S) ∩ R ≡ S ∩ R
∩-cons-∉l {A} {S} {R} x with ∩-cases A S R
∩-cons-∉l {A} {S} {R} x | inj₁ (fst , snd) = snd
∩-cons-∉l {A} {S} {R} x | inj₂ (fst , snd) = ⊥-elim (x fst)

⊆-contra : ∀ {A R S} → S ⊆ R → A ∉ R → A ∉ S
⊆-contra {A} {R} {S} x P P' = P (∈→⊆→∈ P' x)

del-distrib-∩ : ∀ {A S R} (P P' P'') → del A (S ∩ R) P ≡ del A S P' ∩ del A R P''
del-distrib-∩ {A} {B ∷ S} {R} P P' P'' with ∩-cases B S R
... | inj₁ (fst , snd) = begin
    del A ((B ∷ S) ∩ R) P  ≡⟨ del-S-subst snd ⟩
    del A (S ∩ R) (subst (A ∈_) snd P) ≡⟨ del-distrib-∩ {A} {S} {R} _ _ _ ⟩
    del A S P0 ∩ del A R P'' ≡⟨ sym (∩-cons-∉l {B} (⊆-contra (⊆-del-l {A} {R} ⊆-refl) fst)) ⟩
    (B ∷ del A S P0) ∩ del A R P'' ≡⟨ cong (_∩ del A R P'') (sym (del-tail' ab (there P0) P0)) ⟩
    del A (B ∷ S) (there P0) ∩ del A R P'' ≡⟨ cong (_∩ del A R P'') (del-∈-irrel (there P0) P') ⟩
    del A (B ∷ S) P' ∩ del A R P'' ∎
    where ab : ¬ (A ≡ B)
          ab = ∈→∉→¬≡ P'' fst
          P0 : A ∈ S
          P0 = tail ab P'
... | inj₂ (fst , snd) with A ≟ B
... | yes refl = begin
    del B ((B ∷ S) ∩ R) P ≡⟨ del-S-subst {P' = here refl} snd ⟩
    del B (B ∷ (S ∩ del B R fst)) (here refl) ≡⟨ cong (S ∩_) (del-∈-irrel fst P'') ⟩
    S ∩ del B R P'' ≡⟨ cong (_∩ del B R P'') (sym (del-head  P')) ⟩
    del B (B ∷ S) P' ∩ del B R P'' ∎
... | no ab = begin
    del A ((B ∷ S) ∩ R) P ≡⟨ del-S-subst {P = P} {there PA} snd ⟩
    del A (B ∷ (S ∩ del B R fst)) (there PA) ≡⟨ del-tail' ab (there PA) PA ⟩
    B ∷ del A (S ∩ del B R fst) PA ≡⟨ cong (B ∷_) (del-distrib-∩ PA (tail ab P') _) ⟩
    B ∷ del A S (tail ab P') ∩ del A (del B R fst) (∈-del-r ab P'') ≡⟨ cong ((B ∷_) ∘ del A S (tail ab P') ∩_) (del-commut fst _ _ _) ⟩
    B ∷ del A S (tail ab P') ∩ del B (del A R P'') (∈-del-r (ab ∘ sym) fst) ≡⟨ ∩-rev {P = (∈-del-r (ab ∘ sym) fst)} ⟩
    (B ∷ del A S (tail ab P')) ∩ del A R P'' ≡⟨ cong (_∩ del A R P'') (sym (del-tail ab P')) ⟩
    del A (B ∷ S) P' ∩ del A R P'' ∎
    where
      PA : A ∈ S ∩ del B R fst
      PA = tail ab (subst (A ∈_) snd P)
      ∩-rev : ∀ {B S R P} → B ∷ (S ∩ del B R P) ≡ (B ∷ S) ∩ R
      ∩-rev {B} {S} {R} {P} with ∩-cases B S R
      ... | inj₁ (a , b) = ⊥-elim (a P)
      ... | inj₂ (a , b) = trans (cong ((B ∷_) ∘ (S ∩_)) (del-∈-irrel P a)) (sym b)

∈×∈→∩ : ∀ {A S R} → A ∈ S → A ∈ R → A ∈ S ∩ R
∈×∈→∩ {A} {B ∷ S} {R} P P' with ∩-cases B S R
... | inj₁ (fst , snd) = subst (A ∈_) (sym snd) (∈×∈→∩ (tail (∈→∉→¬≡ P' fst) P) P')
... | inj₂ (fst , snd) with A ≟ B
... | yes refl = subst (B ∈_) (sym snd) (here refl)
... | no ab = subst (A ∈_) (sym snd) (there (∈×∈→∩ (tail ab P) (∈-del-r ab P')))

⊆-infimum : Infimum _⊆_ _∩_
⊆-infimum S R = inf1 S R , inf2 S R , inf3 S R
  where
  inf1 : ∀ (S R) → S ∩ R ⊆ S
  inf1 [] R = tt
  inf1 (A ∷ S) R with ∩-cases A S R
  ... | inj₁ (fst , snd) = ⊆-cons-r (subst (_⊆ S) (sym snd) (inf1 S R))
  ... | inj₂ (fst , snd) = subst (_⊆ A ∷ S) (sym snd) (⊆-cons (inf1 S _))
  inf2 : ∀ (S R) → S ∩ R ⊆ R
  inf2 [] R = tt
  inf2 (A ∷ S) R with ∩-cases A S R
  ... | inj₁ (fst , snd) = subst (_⊆ R) (sym snd) (inf2 S R)
  ... | inj₂ (fst , snd) = subst (_⊆ R) (sym snd) (fst , (inf2 S _))
  inf3 : ∀ (S R T) → T ⊆ S → T ⊆ R → T ⊆ S ∩ R
  inf3 S R [] x y = tt
  inf3 S R (A ∷ T) (fst , TS) (fst' , TR) = ∈×∈→∩ fst fst' , subst (T ⊆_) (sym (del-distrib-∩ _ fst fst')) (inf3 _ _ _ TS TR)

isMeetSemilattice : IsMeetSemilattice _⊆⊇_ _⊆_ _∩_
isMeetSemilattice = record { isPartialOrder = ⊆-partialOrder ; infimum = ⊆-infimum }

infixl 6 _∖_
_∖_ : ∀ {R : Multiset} → (S : Multiset) → R ⊆ S → Multiset
_∖_ {[]} S x = S
_∖_ {A ∷ R} S (fst , snd) = del A S fst ∖ snd
