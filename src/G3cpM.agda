-- G3cp
module _ where

open import Agda.Builtin.Nat using (Nat; suc; zero; _+_; _-_)
open import Agda.Builtin.Unit using (tt; ⊤)
open import Agda.Primitive using (Level; lzero; lsuc)
open import Data.Bool using (Bool; false; true; not; T) renaming (_∨_ to _||_ ; _∧_ to _&_)
open import Data.Bool.Properties using () renaming (_≟_ to _Bool=?_)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Fin using (Fin)
open import Data.List using (List; []; _∷_; map; _++_; sum)
open import Data.List.Relation.Unary.All using (All; _∷_; [])
open import Data.List.Relation.Unary.Any using (Any; here; there; tail)
open import Data.Nat using (_≤_; z≤n; s≤s; _⊔_)
open import Data.Nat.Properties using (+-assoc; +-comm; suc-injective; ≤-refl; ≤-pred
  ; ≤-trans; ⊔-sel; ⊔-least; ⊔-comm; m≤m⊔n; n≤m⊔n; m≤n+m; ≤-stepsˡ) renaming (_≟_ to _ℕ≟_)
open import Data.Product using (Σ; proj₁; proj₂; _×_; _,_)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function using (id; const; _∘_)
open import Relation.Binary using (REL; Rel; IsDecEquivalence)
open import Relation.Binary.Bundles using (DecPoset; DecSetoid)
open import Relation.Binary.Definitions using () renaming (Decidable to Decidable₂)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; setoid; isDecEquivalence; cong; subst; sym; trans; module ≡-Reasoning)
open import Relation.Binary.Structures using (IsDecPartialOrder; IsPartialOrder; IsPreorder; IsEquivalence)
open import Relation.Nary using (∃⟨_⟩)
open import Relation.Nullary using (yes; no; Dec; ¬_)
open import Relation.Unary using (Decidable; Pred) renaming (_∈_ to _Pred∈_; _∉_ to _Pred∉_)
open import Relation.Unary.Properties using (∁?)
open ≡-Reasoning using (begin_; _≡⟨⟩_; _≡⟨_⟩_; _∎)


Var : Set
Var = Nat

infixr 20 _↝_
infix 30 _∨_
infix 40 _∧_

data Fm : Set where
  _↝_ : Fm → Fm → Fm
  _∧_ : Fm → Fm → Fm
  _∨_ : Fm → Fm → Fm
  ⊥' : Fm
  var : Var → Fm

decSetoid-Fm : DecSetoid lzero lzero
decSetoid-Fm = record
  { Carrier = Fm
  ; _≈_ = _≡_
  ; isDecEquivalence = isDecEquivalence dec }
  -- TODO can this be shorter?
  where
    inj-var : ∀ {x y} → var x ≡ var y → x ≡ y
    inj-var refl = refl
    inj-∨ : ∀ {a b c d} → a ∨ b ≡ c ∨ d → a ≡ c × b ≡ d
    inj-∨ refl = refl , refl
    inj-↝ : ∀ {a b c d} → a ↝ b ≡ c ↝ d → a ≡ c × b ≡ d
    inj-↝ refl = refl , refl
    inj-∧ : ∀ {a b c d} → a ∧ b ≡ c ∧ d → a ≡ c × b ≡ d
    inj-∧ refl = refl , refl
    dec : Decidable₂ _≡_
    dec (a ↝ b) (c ↝ d) with dec a c | dec b d
    ... | yes refl | yes refl = yes refl
    ... | yes refl | no p1 = no λ x → p1 (proj₂ (inj-↝ x))
    ... | no p | _ = no λ x → p (proj₁ (inj-↝ x))
    dec (x ↝ x₁) (y ∧ y₁) = no (λ ())
    dec (x ↝ x₁) (y ∨ y₁) = no (λ ())
    dec (x ↝ x₁) ⊥' = no (λ ())
    dec (x ↝ x₁) (var x₂) = no (λ ())
    dec (x ∧ x₁) (y ↝ y₁) = no (λ ())
    dec (a ∧ b) (c ∧ d) with dec a c | dec b d
    ... | yes refl | yes refl = yes refl
    ... | yes refl | no p1 = no λ x → p1 (proj₂ (inj-∧ x))
    ... | no p | _ = no λ x → p (proj₁ (inj-∧ x))
    dec (x ∧ x₁) (y ∨ y₁) = no (λ ())
    dec (x ∧ x₁) ⊥' = no (λ ())
    dec (x ∧ x₁) (var x₂) = no (λ ())
    dec (x ∨ x₁) (y ↝ y₁) = no (λ ())
    dec (x ∨ x₁) (y ∧ y₁) = no (λ ())
    dec (a ∨ b) (c ∨ d) with dec a c | dec b d
    ... | yes refl | yes refl = yes refl
    ... | yes refl | no p1 = no λ x → p1 (proj₂ (inj-∨ x))
    ... | no p | _ = no λ x → p (proj₁ (inj-∨ x))
    dec (x ∨ x₁) ⊥' = no (λ ())
    dec (x ∨ x₁) (var x₂) = no (λ ())
    dec ⊥' (y ↝ y₁) = no (λ ())
    dec ⊥' (y ∧ y₁) = no (λ ())
    dec ⊥' (y ∨ y₁) = no (λ ())
    dec ⊥' ⊥' = yes refl
    dec ⊥' (var x) = no (λ ())
    dec (var x) (y ↝ y₁) = no (λ ())
    dec (var x) (y ∧ y₁) = no (λ ())
    dec (var x) (y ∨ y₁) = no (λ ())
    dec (var x) ⊥' = no (λ ())
    dec (var x) (var y) with x ℕ≟ y
    ... | yes p = yes (cong var p)
    ... | no np = no λ h → np (inj-var h)

_≟_ : Decidable₂ (_≡_)
_≟_ = IsDecEquivalence._≟_ (DecSetoid.isDecEquivalence decSetoid-Fm)

open import DecMultiset (_≟_) using (Multiset; _∈_; _∉_; del; _⊆_; _⊆⊇_; ∈→⊆→∈)

¬' : Fm → Fm
¬' ϕ = ϕ ↝ ⊥'

FmSet : Set
FmSet = Multiset

infixr 10 _⸴_
_⸴_ : Fm → FmSet → FmSet
(A ⸴ Γ) =  A ∷ Γ


infixl 0 _⊢_⇒_ _≤⊢_⇒_
-- deducibility in n steps.
data _⊢_⇒_ : Nat → FmSet → FmSet → Set₁ where
  L⊥ : ∀ {Γ Δ} → (A : Fm) → ⊥' ∈ Γ → 0 ⊢ Γ ⇒ A ⸴ Δ
  Ax : ∀ {x Γ Δ} → var x ∈ Γ → 0 ⊢ Γ ⇒ var x ⸴ Δ
  L∧ : ∀ {Γ A B A⸴B⸴Γ A∧B⸴Γ Δ n} → A⸴B⸴Γ ⊆⊇ A ⸴ B ⸴ Γ → A∧B⸴Γ ⊆⊇ A ∧ B ⸴ Γ
   → n ⊢ Γ ⇒ Δ → suc n ⊢ A∧B⸴Γ ⇒ Δ
  R∧ : ∀ {A B Δ A⸴Δ B⸴Δ A∧B⸴Δ Γ n} → A⸴Δ ⊆⊇ A ⸴ Δ → B⸴Δ ⊆⊇ B ⸴ Δ → A∧B⸴Δ ⊆⊇ A ∧ B ⸴ Δ
    → n ⊢ Γ ⇒ A⸴Δ → n ⊢ Γ ⇒ B⸴Δ → suc n ⊢ Γ ⇒ A∧B⸴Δ
  L∨ : ∀ {A B Γ A⸴Γ B⸴Γ A∨B⸴Γ Δ n m} → A⸴Γ ⊆⊇ A ⸴ Γ → B⸴Γ ⊆⊇ B⸴Γ → A∨B⸴Γ ⊆⊇ A ∨ B ⸴ Γ
    → n ⊢ A⸴Γ ⇒ Δ → m ⊢ B⸴Γ ⇒ Δ → suc (n ⊔ m) ⊢ A∨B⸴Γ ⇒ Δ
  L→ : ∀ {Γ A B Δ A⸴Δ B⸴Γ A↝B⸴Γ n m} → A⸴Δ ⊆⊇ A ⸴ Δ → B⸴Γ ⊆⊇ B⸴Γ → A↝B⸴Γ ⊆⊇ A ↝ B ⸴ Γ
    → n ⊢ Γ ⇒ A⸴Δ → m ⊢ B⸴Γ ⇒ Δ → suc (n ⊔ m) ⊢ A↝B⸴Γ ⇒ Δ
  R∨ : ∀ {A B Γ Δ A⸴B⸴Δ A∨B⸴Δ n} → A⸴B⸴Δ ⊆⊇ A ⸴ B ⸴ Δ → A∨B⸴Δ ⊆⊇ A ∨ B ⸴ Δ
    → n ⊢ Γ ⇒ A⸴B⸴Δ → n ⊢ Γ ⇒ A∨B⸴Δ
  R→ : ∀ {Γ A B Δ A⸴Γ B⸴Δ A↝B⸴Δ n} → A⸴Γ ⊆⊇ A ⸴ Γ → A↝B⸴Δ ⊆⊇ A ↝ B ⸴ Δ → B⸴Δ ⊆⊇ B⸴Δ
    → n ⊢ A⸴Γ ⇒ B⸴Δ → suc n ⊢ Γ ⇒ A↝B⸴Δ

infixl 0 _⇒_
_⇒_ : FmSet → FmSet → Set₁
Γ ⇒ Δ = Σ Nat (λ n → n ⊢ Γ ⇒ Δ)

-- Deducibility in at most m steps.
_≤⊢_⇒_ : Nat → FmSet → FmSet → Set₁
m ≤⊢ Γ ⇒ Δ = Σ Nat (λ n → n ≤ m × (n ⊢ Γ ⇒ Δ))

≤-distrib-⊔ : ∀ {a b n} → a ⊔ b ≤ n → a ≤ n × b ≤ n
≤-distrib-⊔ {zero} {zero} {zero} x = x , x
≤-distrib-⊔ {zero} {zero} {suc n} x = x , x
≤-distrib-⊔ {zero} {suc b} {suc n} x = z≤n , x
≤-distrib-⊔ {suc a} {zero} {suc n} x = x , z≤n
≤-distrib-⊔ {suc a} {suc b} {suc n} x with ≤-distrib-⊔ {a} {b} {n} (≤-pred x)
... | f , s = s≤s f , s≤s s

inv-L∧ : ∀ {n A B Γ Δ A∧B⸴Γ A⸴B⸴Γ} → A∧B⸴Γ ⊆⊇ A ∧ B ⸴ Γ → A⸴B⸴Γ ⊆⊇ A ⸴ B ⸴ Γ
  → n ⊢ A∧B⸴Γ ⇒ Δ → n ⊢ A⸴B⸴Γ ⇒ Δ
inv-L∧ {Γ = Γ} x y (L⊥ A p) = L⊥ A {!!}
  where P : ⊥' ∈ Γ
        P = tail (λ {()}) (∈→⊆→∈ p (proj₁ x))
inv-L∧ x y (Ax x₁) = {!!}
inv-L∧ x y (L∧ x₁ x₂ z) = {!!}
inv-L∧ x y (R∧ x₁ x₂ x₃ z z₁) = {!!}
inv-L∧ x y (L∨ x₁ x₂ x₃ z z₁) = {!!}
inv-L∧ x y (L→ x₁ x₂ x₃ z z₁) = {!!}
inv-L∧ x y (R∨ x₁ x₂ z) = {!!}
inv-L∧ x y (R→ x₁ x₂ x₃ z) = {!!}

m⊔n≤m'⊔n' : ∀ {m n m' n'} → m ≤ m' → n ≤ n' → m ⊔ n ≤ m' ⊔ n'
m⊔n≤m'⊔n' pn pm = ⊔-least (≤-trans pn (m≤m⊔n _ _)) (≤-trans pm (n≤m⊔n _ _))

-- inv-L∨ : ∀ {A B Γ Δ n} → n ⊢ A ∨ B ⸴ Γ ⇒ Δ → (n ⊢ A ⸴ Γ ⇒ Δ) × (n ⊢ B ⸴ Γ ⇒ Δ)

-- inv-R∧ : ∀ {n A B Γ Δ} → n ⊢ Γ ⇒ A ∧ B ⸴ Δ → (n ≤⊢ Γ ⇒ A ⸴ Δ) × (n ≤⊢ Γ ⇒ B ⸴ Δ)



-- inv-L∧ : ∀ {n A B Γ Δ} → n ⊢ A ∧ B ⸴ Γ ⇒ Δ → n ⊢ A ⸴ B ⸴ Γ ⇒ Δ
-- inv-L∧ {n} (L⊥ (there x) A , snd) = L⊥ (there (there x)) A , snd
-- inv-L∧ {n} (Ax (there x) , snd) = Ax (there (there x)) , snd
-- inv-L∧ {suc n} (L∧ fst , snd) = fst , aux (≤-pred snd)
--   where aux : ∀ {n m} → n ≤ m → n ≤ suc m
--         aux z≤n = z≤n
--         aux (s≤s p) = s≤s (aux p)
-- inv-L∧ {suc n} (R∧ l r , snd) = R∧ {!!} {!!} , {!!}
-- inv-L∧ {suc n} (R∨ fst , snd) = {!!} , {!!}
-- inv-L∧ {suc n} (R→ fst , snd) = {!!}
-- inv-L∧ {n} (SR a b , snd) = {!!}
-- inv-L∧ {n} (SL a b , snd) = {!!}

Assignment : Set
Assignment = Var → Bool

_[_] : Fm → Assignment → Bool
(a ↝ b) [ v ] = not (a [ v ]) || b [ v ]
(a ∧ b) [ v ] = a [ v ] & b [ v ]
(a ∨ b) [ v ] = a [ v ] || b [ v ]
⊥' [ v ] = false
(var x) [ v ] = v x


-- _⊨2_ : Assignment → Fm → Set
-- v ⊨2 (A ∨ B) = A [ v ] ≡ true ⊎ B [ v ] ≡ true
-- v ⊨2 (A ↝ B) = A [ v ] ≡ false ⊎ B [ v ] ≡ true
-- v ⊨2 ⊥' = ⊥
-- v ⊨2 (var x) = T (v x)

infix 0 _⊭2_ _⊨2_

_⊨2_ : Assignment → Fm → Set
v ⊨2 A = T (A [ v ])

_⊭2_ : Assignment → Fm → Set
v ⊭2 A = T (not (A [ v ]))

⊨∧ : ∀ {v} {A B : Fm} → v ⊨2 A ∧ B → (v ⊨2 A) × (v ⊨2 B)
⊨∧ {v} {A} {B} x with A [ v ] | B [ v ]
⊨∧ {v} {A} {B} x | true | true = tt , tt

⊨∨ : ∀ {v} {A B : Fm} → v ⊨2 A ∨ B → (v ⊨2 A) ⊎ (v ⊨2 B)
⊨∨ {v} {A} {B} x with A [ v ] | B [ v ]
⊨∨ {v} {A} {B} x | false | true = inj₂ tt
⊨∨ {v} {A} {B} x | true | false = inj₁ tt
⊨∨ {v} {A} {B} x | true | true = inj₁ tt

⊨↝ : ∀ {v} {A B : Fm} → v ⊨2 A ↝ B → (v ⊭2 A) ⊎ (v ⊨2 B)
⊨↝ {v} {A} {B} x with A [ v ] | B [ v ]
⊨↝ {v} {A} {B} x | false | false = inj₁ tt
⊨↝ {v} {A} {B} x | false | true = inj₁ tt
⊨↝ {v} {A} {B} x | true | true = inj₂ tt

⊨∨⊭2 : ∀ (v A) → (v ⊨2 A) ⊎ (v ⊭2 A)
⊨∨⊭2 v A with A [ v ]
... | true = inj₁ tt
... | false = inj₂ tt

⊨→¬⊭2 : ∀ {v A} → v ⊨2 A → ¬ (v ⊭2 A)
⊨→¬⊭2 {v} {A} x y with A [ v ]
... | true = y
... | false = x

¬⊭→⊨2 : ∀ {v A} → ¬ (v ⊭2 A) → v ⊨2 A
¬⊭→⊨2 {v} {A} x with A [ v ]
... | true = tt
... | false = x tt

⊭→¬⊨2 : ∀ {v A} → v ⊭2 A → ¬ (v ⊨2 A)
⊭→¬⊨2 {v} {A} x y with A [ v ]
... | true = x
... | false = y

T? : (x : Bool) → Dec (T x)
T? x with true Bool=? x
... | yes refl = yes tt
... | no f = no λ y → f (a y)
  where a : ∀ {b} → T b → true ≡ b
        a {true} x = refl

_⊨2?_ : Decidable₂ _⊨2_
v ⊨2? A = T? (A [ v ])

_⊭2?_ : Decidable₂ _⊭2_
v ⊭2? A = T? (not (A [ v ]))

infix 0 _⊭_ _⊨_
data _⊭_ (v : Assignment) : Fm → Set
data _⊨_ (v : Assignment) : Fm → Set

data _⊭_ v where
  _∨_ : ∀ {A B} → (v ⊭ A) × (v ⊭ B) → v ⊭ A ∨ B
  _∧_ : ∀ {A B} → (v ⊭ A) ⊎ (v ⊭ B) → v ⊭ A ∧ B
  _↝_ : ∀ {A B} → (v ⊨ A) × (v ⊭ B) → v ⊭ A ↝ B
  ⊥' : v ⊭ ⊥'
  var : ∀ {x} → T (not (v x)) → v ⊭ var x

data _⊨_ v where
  _∨_ : ∀ {A B} → (v ⊨ A) ⊎ (v ⊨ B) → v ⊨ A ∨ B
  _∧_ : ∀ {A B} → (v ⊨ A) × (v ⊨ B) → v ⊨ A ∧ B
  _↝_ : ∀ {A B} → (v ⊭ A) ⊎ (v ⊨ B) → v ⊨ A ↝ B
  ⊥' : ⊥ → v ⊨ ⊥'
  var : ∀ {x} → T (v x) → v ⊨ var x

⊨∨⊭ : ∀ (v A) → (v ⊨ A) ⊎ (v ⊭ A)
⊨∨⊭ v (A ↝ B) with ⊨∨⊭ v A | ⊨∨⊭ v B
... | inj₁ x | inj₁ x₁ = inj₁ (_↝_ (inj₂ x₁))
... | inj₁ x | inj₂ y = inj₂ (_↝_ (x , y))
... | inj₂ y | inj₁ x = inj₁ (_↝_ (inj₁ y))
... | inj₂ y | inj₂ y₁ = inj₁ (_↝_ (inj₁ y))
⊨∨⊭ v (A ∧ B) with ⊨∨⊭ v A | ⊨∨⊭ v B
... | inj₁ x | inj₁ x₁ = inj₁ (_∧_ (x , x₁))
... | inj₁ x | inj₂ y = inj₂ (_∧_ (inj₂ y))
... | inj₂ y | inj₁ x = inj₂ (_∧_ (inj₁ y))
... | inj₂ y | inj₂ y₁ = inj₂ (_∧_ (inj₁ y))
⊨∨⊭ v (A ∨ B) with ⊨∨⊭ v A | ⊨∨⊭ v B
... | inj₁ x | inj₁ x₁ = inj₁ (_∨_ (inj₁ x))
... | inj₁ x | inj₂ y = inj₁ (_∨_ (inj₁ x))
... | inj₂ y | inj₁ x = inj₁ (_∨_ (inj₂ x))
... | inj₂ y | inj₂ y₁ = inj₂ (_∨_ (y , y₁))
⊨∨⊭ v (⊥') = inj₂ ⊥'
⊨∨⊭ v (var x) with T? (v x)
... | yes t = inj₁ (var t)
... | no f = inj₂ (var (a f))
  where a : ∀ {b} → ¬ (T b) → T (not b)
        a {false} x = tt
        a {true} x = x tt

¬T : ∀ {b} → T (not b) → ¬ (T b)
¬T {false} x x₁ = x₁
¬T {true} x x₁ = x

⊭→¬⊨ : ∀ {A v} → v ⊭ A → ¬ (v ⊨ A)
⊭→¬⊨ (_∨_ (fst , snd)) (_∨_ (inj₁ x)) = ⊭→¬⊨ fst x
⊭→¬⊨ (_∨_ (fst , snd)) (_∨_ (inj₂ y)) = ⊭→¬⊨ snd y
⊭→¬⊨ (_∧_ (inj₁ x)) (_∧_ (fst , snd)) = ⊭→¬⊨ x fst
⊭→¬⊨ (_∧_ (inj₂ y)) (_∧_ (fst , snd)) = ⊭→¬⊨ y snd
⊭→¬⊨ (_↝_ (fst , snd)) (_↝_ (inj₁ x)) = ⊭→¬⊨ x fst
⊭→¬⊨ (_↝_ (fst , snd)) (_↝_ (inj₂ y)) = ⊭→¬⊨ snd y
⊭→¬⊨ {var x} (var x₂) (var x₁) = ¬T x₂ x₁

⊨→¬⊭ : ∀ {A v} → v ⊨ A → ¬ (v ⊭ A)
⊨→¬⊭ {A} {v} x y = ⊭→¬⊨ y x

¬⊨→⊭ : ∀ {A v} → ¬ (v ⊨ A) → v ⊭ A
¬⊨→⊭ {A} {v} x with ⊨∨⊭ v A
¬⊨→⊭ {A} {v} x | inj₁ x₁ = ⊥-elim (x x₁)
¬⊨→⊭ {A} {v} x | inj₂ y = y

_⊨?_ : Decidable₂ _⊨_
_⊭?_ : Decidable₂ _⊭_

v ⊨? A with ⊨∨⊭ v A
(v ⊨? A) | inj₁ x = yes x
(v ⊨? A) | inj₂ y = no (⊭→¬⊨ y)

v ⊭? A with ⊨∨⊭ v A
(v ⊭? A) | inj₁ x = no (⊨→¬⊭ x)
(v ⊭? A) | inj₂ y = yes y

_⊨×_ : Assignment → FmSet → Set
v ⊨× Γ = All (v ⊨_) Γ

_⊨+_ : Assignment → FmSet → Set
v ⊨+ Γ = Any (v ⊨_) Γ

_⊨_⇒_ : Assignment → FmSet → FmSet → Set
v ⊨ Γ ⇒ Δ = v ⊨× Γ → v ⊨+ Δ

All-∈ : ∀ {L x} {P : Pred Fm lzero} → All P L → x ∈ L → x Pred∈ P
All-∈ (px ∷ p) (here refl) = px
All-∈ (px ∷ p) (there e) = All-∈ p e

-- soundness : ∀ {Γ Δ v} → Γ ⇒ Δ → v ⊨ Γ ⇒ Δ
-- soundness = {!!}
-- soundness (L⊥ x A) p with All-∈ p x
-- ... | ⊥' ()
-- soundness (Ax x) p = here (All-∈ p x)
-- soundness (L∧ x) (_∧_ (fst , snd) ∷ p) = soundness x (fst ∷ snd ∷ p)
-- soundness (R∧ pA pB) p with soundness pA p , soundness pB p
-- ... | here ha , here hb = here (_∧_ (ha , hb))
-- ... | here ha , there hb = there hb
-- ... | there ha , here hb = there ha
-- ... | there ha , there hb = there hb
-- soundness (L∨ pA pB) (_∨_ (inj₁ x) ∷ p) = soundness pA (x ∷ p)
-- soundness (L∨ pA pB) (_∨_ (inj₂ y) ∷ p) = soundness pB (y ∷ p)
-- soundness (R∨ x) p with soundness x p
-- ... | here px = here (_∨_ (inj₁ px))
-- ... | there (here px) = here (_∨_ (inj₂ px))
-- ... | there (there a) = there a
-- soundness (L→ p1 p2) (p ∷ z) with soundness p1 z
-- soundness (L→ p1 p2) (_↝_ (inj₁ x) ∷ z) | here px = ⊥-elim (⊭→¬⊨ x px)
-- soundness (L→ p1 p2) (_↝_ (inj₂ y) ∷ z) | here px = soundness p2 (y ∷ z)
-- ... | there w = w
-- soundness {_} {_} {v} (R→ {_} {A} {B} x) p with v ⊨? A
-- ... | no y = here (_↝_ (inj₁ (¬⊨→⊭ y)))
-- ... | yes y with v ⊨? B
-- ... | yes b = here (_↝_ (inj₂ b))
-- ... | no b with soundness x (y ∷ p)
-- ... | here h = ⊥-elim (b h)
-- ... | there h = there h

-- completeness-contra : ∀ {Γ Δ} → ¬ (Γ ⇒ Δ) → (Σ Assignment (λ v → v ⊭ Γ ⇒ Δ))
-- completeness-contra x = {!!}

⊨⇒∷Γ : ∀ {Γ Δ A v} → v ⊨ A → v ⊨ (A ∷ Γ) ⇒ Δ → v ⊨ Γ ⇒ Δ
⊨⇒∷Γ x y z = y (x ∷ z)

⊨⇒∷Δ : ∀ {Γ Δ A v} → v ⊭ A → v ⊨ Γ ⇒ (A ∷ Δ) → v ⊨ Γ ⇒ Δ
⊨⇒∷Δ x y z with y z
... | here px = ⊥-elim (⊭→¬⊨ x px)
... | there px = px


⊨⇒-Any : ∀ {Γ Δ v} → v ⊨ Γ ⇒ Δ → Any (v ⊭_) Γ ⊎ Any (v ⊨_) Δ
⊨⇒-Any {[]} {[]} x with x []
... | ()
⊨⇒-Any {[]} {B ∷ Δ} x with x []
... | here px = inj₂ (here px)
... | there px = inj₂ (there px)
⊨⇒-Any {A ∷ Γ} {[]} {v} x with ⊨∨⊭ v A
... | inj₂ y = inj₁ (here y)
... | inj₁ vA with ⊨⇒-Any {Γ} {[]} {v} (⊨⇒∷Γ vA x)
... | inj₁ p = inj₁ (there p)
⊨⇒-Any {A ∷ Γ} {B ∷ Δ} {v} x with ⊨∨⊭ v A
... | inj₂ ¬pA = inj₁ (here ¬pA)
... | inj₁ pA with ⊨⇒-Any (⊨⇒∷Γ pA x)
... | inj₁ z = inj₁ (there z)
... | inj₂ y = inj₂ y


-- lweak : ∀ {Γ Δ Γ'} → Γ ⊆ Γ' → Γ ⇒ Δ → Γ' ⇒ Δ
-- lweak x (L⊥ x₁ A) = L⊥ (All-∈ x x₁) A
-- lweak x (Ax x₁) = Ax (All-∈ x x₁)
-- lweak (here refl ∷ x) (L∧ p) = L∧ (lweak ((here refl) ∷ ((there (here refl)) ∷ {!!})) p)
-- lweak {.(_ ∧ _ ∷ _)} {Δ} {.(_ ∷ _)} (there px ∷ x) (L∧ p) = {!!}
-- lweak x (R∧ p p₁) = R∧ (lweak x p) (lweak x p₁)
-- lweak x (L∨ p p₁) = {!!}
-- lweak x (R∨ p) = R∨ (lweak x p)
-- lweak x (L→ p p₁) = {!!}
-- lweak x (R→ p) = R→ {!!}

-- rweak : ∀ {Γ Δ Δ'} → Δ ⊆ Δ' → Γ ⇒ Δ → Γ ⇒ Δ'
-- rweak = {!!}


weak-⇒ : ∀ {Γ Δ} (A) → Γ ⇒ Δ → Γ ⇒ A ∷ Δ
weak-⇒ = {!!}


*⊨_⇒_ : FmSet → FmSet → Set
*⊨ Γ ⇒ Δ = (∀ (v) → v ⊨ Γ ⇒ Δ)


⊨-R→ : ∀ {A B Γ Δ} → *⊨ Γ ⇒ (A ↝ B ∷ Δ) → *⊨ A ∷ Γ ⇒ (B ∷ Δ)
⊨-R→ {A} {B} x v (px ∷ a) with x v a
... | here (_↝_ (inj₁ x₁)) = ⊥-elim (⊨→¬⊭ px x₁)
... | here (_↝_ (inj₂ y)) = here y
... | there py = there py

⊨-R∧ : ∀ {A B Γ Δ} → *⊨ Γ ⇒ (A ∧ B ∷ Δ) → *⊨ Γ ⇒ (A ∷ Δ) × *⊨ Γ ⇒ (B ∷ Δ)
⊨-R∧ {A} {B} {Γ} {Δ} x = (λ v w → proj₁ (aux w)) , (λ v w → proj₂ (aux w))
  where aux : ∀ {v} → v ⊨× Γ → v ⊨+ (A ∷ Δ) × v ⊨+ (B ∷ Δ)
        aux {v} h with x v h
        ... | here (_∧_ (a , b)) = (here a) , (here b)
        ... | there px = (there px) , (there px)

size : Fm → Nat
size (A ↝ B) = suc (size A + size B)
size (A ∧ B) = suc (size A + size B)
size (A ∨ B) = suc (size A + size B)
size ⊥' = suc zero
size (var x) = suc zero

sizeS : FmSet → Nat
sizeS = sum ∘ map size

size⇒ : FmSet → FmSet → Nat
size⇒ Γ Δ = sizeS (Γ ++ Δ)

size⇒-r : ∀ (Γ A Δ) → size⇒ Γ (A ∷ Δ) ≡ size A + (size⇒ Γ Δ)
size⇒-r [] A Δ = refl
size⇒-r (B ∷ Γ) A Δ =
  begin
  size⇒ (B ∷ Γ) (A ∷ Δ) ≡⟨⟩
  size B + size⇒ Γ (A ∷ Δ) ≡⟨ cong (size B +_) (size⇒-r Γ A Δ) ⟩
  size B + (size A + (size⇒ Γ Δ)) ≡⟨ sym (+-assoc (size B) (size A) (size⇒ Γ Δ)) ⟩
  (size B + size A) + size⇒ Γ Δ ≡⟨ cong (_+ size⇒ Γ Δ) (+-comm (size B) (size A)) ⟩
  (size A + size B) + size⇒ Γ Δ ≡⟨ (+-assoc (size A) (size B) (size⇒ Γ Δ)) ⟩
  size A + (size B + size⇒ Γ Δ) ≡⟨⟩
  size A + (size⇒ (B ∷ Γ) Δ) ∎

⊨-R∨ : ∀ {A B Γ Δ} → *⊨ Γ ⇒ (A ∨ B ∷ Δ) → *⊨ Γ ⇒ (A ∷ B ∷ Δ)
⊨-R∨ x v w with x v w
... | here (_∨_ (inj₁ a)) = here a
... | here (_∨_ (inj₂ a)) = there (here a)
... | there o = there (there o)


aux : ∀ (Γ A B Δ) → size⇒ Γ (A ↝ B ∷ Δ) ≡ suc (size A + (size B + (size⇒ Γ Δ)))
aux Γ A B Δ = trans (size⇒-r Γ (A ↝ B) Δ) (cong suc ((+-assoc (size A) (size B) (size⇒ Γ Δ))))

size-op-r : ∀ (Γ A B Δ) (op : Fm → Fm → Fm) → size (op A B) ≡ suc (size A + size B) → size⇒ Γ (op A B ∷ Δ) ≡ suc (size A + size B) + (size⇒ Γ Δ)
size-op-r Γ A B Δ op x = begin
  size⇒ Γ (op A B ∷ Δ) ≡⟨ size⇒-r Γ (op A B) Δ ⟩
  size (op A B) + size⇒ Γ Δ ≡⟨ cong (_+ size⇒ Γ Δ) x ⟩
  suc (size A + size B) + size⇒ Γ Δ ∎

is-var : Fm → Set
is-var (_ ↝ _) = ⊥
is-var (_ ∧ _) = ⊥
is-var (_ ∨ _) = ⊥
is-var ⊥' = ⊥
is-var (var x) = ⊤

is-var? : Decidable is-var
is-var? (_ ↝ _) = no (λ z → z)
is-var? (_ ∧ _) = no (λ z → z)
is-var? (_ ∨ _) = no (λ z → z)
is-var? ⊥' = no (λ z → z)
is-var? (var x) = yes tt


select : (X : FmSet) → All is-var X ⊎ Σ (FmSet × Fm × FmSet) λ { (L , A , R) → L ++ A ∷ R ≡ X × ¬ (is-var A) }
select [] = inj₁ []
select (x ∷ X) with select X | is-var? x
... | inj₁ x₁ | yes y = inj₁ (y ∷ x₁)
... | inj₁ x₁ | no n = inj₂ (([] , x , X) , refl , n)
select (x ∷ X) | inj₂ ((L , A , R) , fst , snd) | w = inj₂ ((x ∷ L , A , R) , cong (_∷_ x) fst , snd)


-- compl : ∀ (Γ Δ) → (n : Nat) → sizeS Γ + sizeS Δ ≤ n → *⊨ Γ ⇒ Δ → Γ ⇒ Δ
-- compl-left : ∀ (A Γ Δ) → (n : Nat) → sizeS (A ∷ Γ) + sizeS Δ ≤ n → *⊨ A ∷ Γ ⇒ Δ → A ∷ Γ ⇒ Δ
-- compl-right : ∀ (Γ A Δ) → (n : Nat) → sizeS Γ + sizeS (A ∷ Δ) ≤ n → *⊨ Γ ⇒ (A ∷ Δ) → Γ ⇒ A ∷ Δ

-- compl-left (A ↝ B) Γ Δ (suc n) pn t = L→ (compl Γ (A ∷ Δ) n {!!} {!!}) (compl (B ∷ Γ) Δ n {!!} {!!})
-- compl-left (A ∧ B) Γ Δ (suc n) pn t = L∧ (compl (A ⸴ B ⸴ Γ) Δ n {!!} {!!})
-- compl-left (A ∨ B) Γ Δ (suc n) pn t = L∨ (compl (A ⸴ Γ) Δ n {!!} {!!}) (compl (B ∷ Γ) Δ n {!!} {!!})
-- compl-left ⊥' Γ [] (suc n) pn t = {!!}
-- compl-left ⊥' Γ (x ∷ Δ) (suc n) pn t = L⊥ (here refl) x
-- compl-left (var x) Γ Δ (suc n) pn t = {!!}

-- compl-right Γ A Δ n pn t = {!!}

-- compl Γ Δ n pn t  with select Γ | select Δ
-- compl .(L ++ A ↝ B ∷ R) Δ n pn t | inj₂ ((L , A ↝ B , R) , refl , _) | _ = {!!}
-- compl Γ Δ n pn t | inj₂ ((L , A ∧ B , R) , f , _) | _ = {!!}
-- compl Γ Δ n pn t | inj₂ ((L , A ∨ B , R) , f , _) | _ = {!!}
-- compl Γ Δ n pn t | inj₂ ((L , ⊥' , R) , f , _) | _ = {!!}
-- compl Γ Δ n pn t | inj₂ ((L , var x , R) , f , s) | _ = ⊥-elim (s tt)
-- ... | inj₁ x | inj₁ x₁ = {!!}
-- ... | inj₁ x | inj₂ y = {!!}

-- completeness : ∀ {Γ Δ} → (*⊨ Γ ⇒ Δ) → Γ ⇒ Δ
-- completeness = {!!}
