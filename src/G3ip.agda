-- Logic G3ip
module _ where

open import Agda.Builtin.Nat using (Nat; suc)
open import Agda.Builtin.Unit using (tt; ⊤)
open import Agda.Primitive using (Level; lzero; lsuc)
open import Data.Bool using (Bool; false; true; not; T) renaming (_∨_ to _||_ ; _∧_ to _&_)
open import Data.Empty using (⊥; ⊥-elim)
open import Data.Fin using (Fin)
open import Data.List using (List; []; _∷_)
open import Data.List.Relation.Unary.All using (All; _∷_; [])
open import Data.List.Relation.Unary.Any using (Any; here; there)
open import Data.Nat.Properties renaming (_≟_ to _ℕ≟_)
open import Data.Product using (Σ; proj₁; proj₂; _×_; _,_)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Vec using (Vec; [] ; _∷_; map; foldr)
open import Function using (id; const; _∘_)
open import Relation.Binary using (REL; Rel; IsDecEquivalence)
open import Relation.Binary.Bundles using (DecPoset; DecSetoid)
open import Relation.Binary.Definitions renaming (Decidable to Decidable₂)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; setoid; isDecEquivalence; cong)
open import Relation.Binary.Structures using (IsDecPartialOrder; IsPartialOrder; IsPreorder; IsEquivalence)
open import Relation.Nullary using (yes; no; Dec; ¬_)
open import Relation.Unary using (Decidable; ∁; Pred; _⊆_) renaming (_∈_ to _Pred∈_; _∉_ to _Pred∉_)
open import Relation.Unary.Properties using (∁?)
open import Relation.Nary using (∃⟨_⟩)

Var : Set
Var = Nat

infixr 20 _↝_
infix 30 _∨_
infix 40 _∧_

data Fm : Set where
  _↝_ : Fm → Fm → Fm
  _∧_ : Fm → Fm → Fm
  _∨_ : Fm → Fm → Fm
  ⊥' : Fm
  var : Var → Fm

decSetoid-Fm : DecSetoid _ _
decSetoid-Fm = record
  { Carrier = Fm
  ; _≈_ = _≡_
  ; isDecEquivalence = isDecEquivalence dec }
  -- TODO can this be shorter?
  where
    inj-var : ∀ {x y} → var x ≡ var y → x ≡ y
    inj-var refl = refl
    inj-∨ : ∀ {a b c d} → a ∨ b ≡ c ∨ d → a ≡ c × b ≡ d
    inj-∨ refl = refl , refl
    inj-↝ : ∀ {a b c d} → a ↝ b ≡ c ↝ d → a ≡ c × b ≡ d
    inj-↝ refl = refl , refl
    inj-∧ : ∀ {a b c d} → a ∧ b ≡ c ∧ d → a ≡ c × b ≡ d
    inj-∧ refl = refl , refl
    dec : Decidable₂ _≡_
    dec (a ↝ b) (c ↝ d) with dec a c | dec b d
    ... | yes refl | yes refl = yes refl
    ... | yes refl | no p1 = no λ x → p1 (proj₂ (inj-↝ x))
    ... | no p | _ = no λ x → p (proj₁ (inj-↝ x))
    dec (x ↝ x₁) (y ∧ y₁) = no (λ ())
    dec (x ↝ x₁) (y ∨ y₁) = no (λ ())
    dec (x ↝ x₁) ⊥' = no (λ ())
    dec (x ↝ x₁) (var x₂) = no (λ ())
    dec (x ∧ x₁) (y ↝ y₁) = no (λ ())
    dec (a ∧ b) (c ∧ d) with dec a c | dec b d
    ... | yes refl | yes refl = yes refl
    ... | yes refl | no p1 = no λ x → p1 (proj₂ (inj-∧ x))
    ... | no p | _ = no λ x → p (proj₁ (inj-∧ x))
    dec (x ∧ x₁) (y ∨ y₁) = no (λ ())
    dec (x ∧ x₁) ⊥' = no (λ ())
    dec (x ∧ x₁) (var x₂) = no (λ ())
    dec (x ∨ x₁) (y ↝ y₁) = no (λ ())
    dec (x ∨ x₁) (y ∧ y₁) = no (λ ())
    dec (a ∨ b) (c ∨ d) with dec a c | dec b d
    ... | yes refl | yes refl = yes refl
    ... | yes refl | no p1 = no λ x → p1 (proj₂ (inj-∨ x))
    ... | no p | _ = no λ x → p (proj₁ (inj-∨ x))
    dec (x ∨ x₁) ⊥' = no (λ ())
    dec (x ∨ x₁) (var x₂) = no (λ ())
    dec ⊥' (y ↝ y₁) = no (λ ())
    dec ⊥' (y ∧ y₁) = no (λ ())
    dec ⊥' (y ∨ y₁) = no (λ ())
    dec ⊥' ⊥' = yes refl
    dec ⊥' (var x) = no (λ ())
    dec (var x) (y ↝ y₁) = no (λ ())
    dec (var x) (y ∧ y₁) = no (λ ())
    dec (var x) (y ∨ y₁) = no (λ ())
    dec (var x) ⊥' = no (λ ())
    dec (var x) (var y) with x ℕ≟ y
    ... | yes p = yes (cong var p)
    ... | no np = no λ h → np (inj-var h)

_≟_ : Decidable₂ (_≡_)
_≟_ = IsDecEquivalence._≟_ (DecSetoid.isDecEquivalence decSetoid-Fm)

open import Data.List.Membership.Setoid (setoid Fm) using (_∈_; _∉_)
open import Data.List.Membership.DecPropositional {A = Fm} (_≟_) using (_∈?_)


¬' : Fm → Fm
¬' ϕ = ϕ ↝ ⊥'

⊤' : Fm
⊤' = ¬' ⊥'

_↔_ : Fm → Fm → Fm
ϕ ↔ ψ = (ϕ ↝ ψ) ∧ (ψ ↝ ϕ)

FmSet : Set
FmSet = List Fm

infixr 10 _⸴_
_⸴_ : Fm → FmSet → FmSet
(A ⸴ Γ) =  A ∷ Γ

REL' : Set → Set → Set₁
REL' a b = REL a b lzero

-- G3ip is the propositional fragment of G3i as described in Basic Proof Theory
-- (2n Ed) Troelstra (page 78)
infixl 0 _⇒_
data _⇒_ : FmSet → Fm → Set₁ where
  L⊥ : ∀ {Γ} → ⊥' ∈ Γ → (A : Fm) → Γ ⇒ A
  Ax : ∀ {x Γ} → var x ∈ Γ → Γ ⇒ var x
  L∧ : ∀ {Γ A B C} → A ⸴ B ⸴ Γ ⇒ C → A ∧ B ⸴ Γ ⇒ C
  R∧ : ∀ {Γ A B} → Γ ⇒ A → Γ ⇒ B → Γ ⇒ A ∧ B
  L∨ : ∀ {Γ A B C} → A ⸴ Γ ⇒ C → B ⸴ Γ ⇒ C → A ∨ B ⸴ Γ ⇒ C
  R∨₁ : ∀ {Γ A B} → Γ ⇒ A → Γ ⇒ A ∨ B
  R∨₂ : ∀ {Γ A B} → Γ ⇒ B → Γ ⇒ A ∨ B
  L→ : ∀ {Γ A B C} → A ↝ B ⸴ Γ ⇒ A → B ⸴ Γ ⇒ C → A ↝ B ⸴ Γ ⇒ C
  R→ : ∀ {Γ A B} → A ⸴ Γ ⇒ B → Γ ⇒ A ↝ B

record KripkeFrame : Set₁ where
  infix 4 _≤_
  field
    Carrier           : Set
    _≤_               : Rel Carrier lzero
    isDecPartialOrder : IsDecPartialOrder _≡_ _≤_

World : KripkeFrame → Set
World = KripkeFrame.Carrier

-- A valuation is a function from variables to subsets of worlds.
-- v : World → 𝒫(Var)
Valuation : KripkeFrame → Set₁
Valuation F = World F → Σ (Pred Var lzero) Decidable

[_]_≤_ : (F : KripkeFrame) → Rel (World F) lzero
[ F ] w ≤ w' = KripkeFrame._≤_ F w w'


-- TODO lacks monotonicity!!!
record KripkeModel : Set₁ where
  field
    Frame : KripkeFrame
    valuation : Valuation Frame
    monotonicity : ∀ (w w') → [ Frame ] w ≤ w' → proj₁ (valuation w) ⊆ proj₁ (valuation w')

World' : KripkeModel → Set
World' = World ∘ KripkeModel.Frame

_⟨_⟩ : (M : KripkeModel) → (World' M) → Pred Var lzero
M ⟨ x ⟩ = (proj₁ (KripkeModel.valuation M x))


[_]≤refl : (F : KripkeFrame) → (w : World F) → [ F ] w ≤ w
[ F ]≤refl w = IsPreorder.reflexive
                 (IsPartialOrder.isPreorder
                  (IsDecPartialOrder.isPartialOrder
                   (KripkeFrame.isDecPartialOrder F)))
                 refl

[_]≤trans : (F : KripkeFrame) → {w1 w2 w3 : World F} → [ F ] w1 ≤ w2 → [ F ] w2 ≤ w3 → [ F ] w1 ≤ w3
[ F ]≤trans p1 p2 = IsPreorder.trans
                 (IsPartialOrder.isPreorder
                  (IsDecPartialOrder.isPartialOrder
                   (KripkeFrame.isDecPartialOrder F))) p1 p2


infixl 0 _⊩_ _⊮_ _⊩*_
data _⊮_ : Σ KripkeModel World' → Fm → Set
data _⊩_ : Σ KripkeModel World' → Fm → Set

{-# NO_POSITIVITY_CHECK #-}
data _⊩_ where
  var : {x : Var} → (M : KripkeModel) → (w : World' M) → x Pred∈ M ⟨ w ⟩ → M , w ⊩ var x
  ∧' : ∀ {M A B} → (w : World' M) → M , w ⊩ A → M , w ⊩ B → M , w ⊩ A ∧ B
  ∨' : ∀ {M A B} → (w : World' M) → (M , w ⊩ A) ⊎ (M , w ⊩ B) → M , w ⊩ A ∨ B
  ⊥' : ∀ {M} → (w : World' M) → ⊥ → (M , w) ⊩ ⊥'
  -- The following constructor is not strictly positive since M , w' ⊩ A appears on the left of an →
  -- Look into this. Is it safe?
  ↝' : ∀ {M A B} → (w : World' M)
      → ((w' : World' M) → [ KripkeModel.Frame M ] w ≤ w' → (M , w' ⊩ A) → (M , w' ⊩ B))
      → M , w ⊩ A ↝ B

_⊩*_ : Σ KripkeModel World' → FmSet → Set
Mw ⊩* Γ = All (Mw ⊩_) Γ

⊩-monotonicity : ∀ {A M w w'} → [ KripkeModel.Frame M ] w ≤ w' → M , w ⊩ A → M , w' ⊩ A
⊩-monotonicity {A ↝ B} {M} {w} {w'} w≤w' (↝' w p) = ↝' w' λ w'' w'≤w'' w''⊩A → p w'' ([ KripkeModel.Frame M ]≤trans w≤w' w'≤w'') w''⊩A
⊩-monotonicity {A ∧ B} {M} {w} {w'} x (∧' w pA pB) = ∧' w' (⊩-monotonicity x pA) (⊩-monotonicity x pB)
⊩-monotonicity {A ∨ B} {M} {w} {w'} x (∨' w (inj₁ p)) = ∨' w' (inj₁ (⊩-monotonicity x p))
⊩-monotonicity {A ∨ B} {M} {w} {w'} x (∨' w (inj₂ p)) = ∨' w' (inj₂ (⊩-monotonicity x p))
⊩-monotonicity {⊥'} x (⊥' w ())
⊩-monotonicity {var y} {M} {w} {w'} x (var M w x₁) = var M w' (KripkeModel.monotonicity M w w' x x₁)

⊩*-monotonicity : ∀ {Γ M w w'} → [ KripkeModel.Frame M ] w ≤ w' → M , w ⊩* Γ → M , w' ⊩* Γ
⊩*-monotonicity x [] = []
⊩*-monotonicity x (px ∷ l) = ⊩-monotonicity x px ∷ ⊩*-monotonicity x l

data _⊮_ where
  var : {x : Var} → (M : KripkeModel) → (w : World' M) → x Pred∉ M ⟨ w ⟩ → M , w ⊮ var x
  ∧' : ∀ {M A B} → (w : World' M) → (M , w ⊮ A) ⊎ (M , w ⊮ B) → M , w ⊮ A ∧ B
  ∨' : ∀ {M A B} → (w : World' M) → M , w ⊮ A → M , w ⊮ B → M , w ⊮ A ∨ B
  ⊥' : ∀ {M} → (w : World' M) → (M , w) ⊮ ⊥'
  ↝' : ∀ {M A B} → (w : World' M) → (w' : World' M) → [ KripkeModel.Frame M ] w ≤ w' → (M , w' ⊩ A) → (M , w' ⊮ B) → M , w ⊮ A ↝ B


⊮⇒¬⊩ : ∀ {M w A} → M , w ⊮ A → ¬ (M , w ⊩ A)
⊮⇒¬⊩ (var M w x) (var .M .w x₁) = x x₁
⊮⇒¬⊩ (∧' w (inj₁ x)) (∧' .w y y₁) = ⊮⇒¬⊩ x y
⊮⇒¬⊩ (∧' w (inj₂ z)) (∧' .w y y₁) = ⊮⇒¬⊩ z y₁
⊮⇒¬⊩ (∨' w x x₁) (∨' .w (inj₁ x₂)) = ⊮⇒¬⊩ x x₂
⊮⇒¬⊩ (∨' w x x₁) (∨' .w (inj₂ y)) = ⊮⇒¬⊩ x₁ y
⊮⇒¬⊩ (↝' w w' x x₁ ¬B) (↝' .w x₂) = ⊮⇒¬⊩ ¬B (x₂ w' x x₁)

⊩⇒¬⊮ : ∀ {M w A} → M , w ⊮ A → ¬ (M , w ⊩ A)
⊩⇒¬⊮ = {!!}

All-∈ : ∀ {L x} {P : Pred Fm lzero} → All P L → x ∈ L → x Pred∈ P
All-∈ (px ∷ p) (here refl) = px
All-∈ (px ∷ p) (there e) = All-∈ p e

-- The following cannot be proved. The problem is in the case (A ↝ B)
-- ⊩||⊮ : ∀ (A M w) → (M , w ⊩ A) ⊎ (M , w ⊮ A)

soundness : ∀ {A M w Γ} → Γ ⇒ A → M , w ⊩* Γ → M , w ⊩ A
soundness {A} {M} {w} (L⊥ x A) p with All-∈ p x
... | ⊥' w ()
soundness {A} {M} {w} (Ax x) p = All-∈ p x
soundness {C} {M} {w} (L∧ {Γ1} {A} {B} h) (∧' w pA pB ∷ p) = soundness h (pA ∷ pB ∷ p)
soundness {A ∧ B} {M} {w} (R∧ hA hB) p = ∧' w (soundness hA p) (soundness hB p)
soundness {C} {M} {w} (L∨ {Γ1} {A} {B} hA hB) (∨' w (inj₁ x) ∷ p) = soundness hA (x ∷ p)
soundness {C} {M} {w} (L∨ {Γ1} {A} {B} hA hB) (∨' w (inj₂ y) ∷ p) = soundness hB (y ∷ p)
soundness {A ∨ B} {M} {w} (R∨₁ h) p = ∨' w (inj₁ (soundness h p))
soundness {A ∨ B} {M} {w} (R∨₂ h) p = ∨' w (inj₂ (soundness h p))
soundness {C} {M} {w} (L→ {Γ1} {A} {B} h1 h2) (↝' w x ∷ p) = soundness h2 (x w ([ KripkeModel.Frame M ]≤refl w) (soundness h1 (↝' w x ∷ p)) ∷ p)
soundness {A ↝ B} {M} {w} {Γ} (R→ h) p = ↝' w λ w' w≤w' w'⊩A → soundness h (w'⊩A ∷ ⊩*-monotonicity w≤w' p)

deduction : ∀ {A B Γ} → A ⸴ Γ ⇒ B → Γ ⇒ A ↝ B
deduction x = R→ x

-- completeness : ∀ (A) → (∀ (M w) → M , w ⊩ A) → [] ⇒ A
-- completeness (A ↝ A₁) p = {!!}
-- completeness (A ∧ B) p = R∧ (completeness A λ M w → ∧₁ (p M w)) (completeness B λ M w → ∧₂ (p M w))
--   where ∧₁ : ∀ {M w A B} → M , w ⊩ A ∧ B → M , w ⊩ A
--         ∧₁ (∧' _ x _) = x
--         ∧₂ : ∀ {M w A B} → M , w ⊩ A ∧ B → M , w ⊩ B
--         ∧₂ (∧' _ _ x) = x
-- completeness (A ∨ B) p = {!!}
-- completeness ⊥' p = {!!}
-- completeness (var x) p = {!!}

∃⊮_  : Fm → Set₁
∃⊮_ A = Σ (Σ KripkeModel World') λ {(M , w) → M , w ⊮ A}

completeness-contra : ∀ (A) → ¬ ([] ⇒ A) → ∃⊮ A
completeness-contra A x = {!!}

infixl 0 _⇏_
data _⇏_ : FmSet → Fm → Set₁ where
  -- Ax : ∀ {P Γ} → P ∈ Γ → Γ ⇒ P
  -- L∧ : ∀ {Γ A B C} → A ⸴ B ⸴ Γ ⇒ C → A ∧ B ⸴ Γ ⇒ C
  -- R∧ : ∀ {Γ A B} → Γ ⇒ A → Γ ⇒ B → Γ ⇒ A ∧ B
  -- L∨ : ∀ {Γ A B C} → A ⸴ Γ ⇒ C → B ⸴ Γ ⇒ C → A ∨ B ⸴ Γ ⇒ C
  -- R∨₁ : ∀ {Γ A B} → Γ ⇒ A → Γ ⇒ A ∨ B
  -- R∨₂ : ∀ {Γ A B} → Γ ⇒ B → Γ ⇒ A ∨ B
  -- L→ : ∀ {Γ A B C} → A ↝ B ⸴ Γ ⇒ A → B ⸴ Γ ⇒ C → A ↝ B ⸴ Γ ⇒ C
  -- R→ : ∀ {Γ A B} → A ⸴ Γ ⇒ B → Γ ⇒ A ↝ B

l⊥ : ∀ (Γ A) → ¬ (Γ ⇒ A) → ¬ (⊥' ∈ Γ)
l⊥ .(⊥' ∷ _) A x (here refl) = x (L⊥ (here refl) A)
l⊥ .(_ ∷ _) A x (there y) = x (L⊥ (there y) A)


l∨ : ∀ (Γ A B) → ¬ (Γ ⇒ A ∨ B) → ¬ (Γ ⇒ A) × ¬ (Γ ⇒ B)
l∨ Γ A B x = {!!}

aa : ∀ (Γ A B) → ¬ (Γ ⇒ A ∧ B) → (¬ (Γ ⇒ A)) ⊎ (¬ (Γ ⇒ B))
aa = {!!}

-- completeness : ∀ (Γ A) → (∀ (M w) → M , w ⊩* Γ → M , w ⊩ A) → Γ ⇒ A
-- completeness Γ (A ↝ A₁) p = {!!}
-- completeness Γ (A ∧ A₁) p = {!!}
-- completeness Γ (A ∨ A₁) p = {!!}
-- completeness Γ ⊥' p = {!!}
-- completeness Γ (var x) p = {!!}
