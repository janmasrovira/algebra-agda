module _ where

import Agda.Builtin.Unit as U
import Data.Empty as Empty
import Data.Fin as Fin
import Data.List as Lst
import Data.Maybe as M
import Function as Fun
import Relation.Binary.PropositionalEquality as Eq
open Eq using (_≡_; refl; subst)
open Fin using (Fin; inject+; fromℕ; inject₁; zero; suc; #_)
open Fun using (id)
open Lst using (List; []; _∷_)
open M using (nothing; just; Maybe)
open import Agda.Builtin.Equality
open import Agda.Builtin.FromNat
open import Agda.Builtin.Nat
open import Data.Bool renaming (_∨_ to _||_ ; _∧_ to _&_) hiding (_≤_)
open import Data.Nat using (_≤?_)
open import Data.Product

Var : Set
Var = Nat

infixr 20 _↝_
data Fm : Set where
  _! : Var → Fm
  _↝_ : Fm → Fm → Fm
  □ : Fm → Fm
  ⊥ : Fm

¬ : Fm → Fm
¬ ϕ = ϕ ↝ ⊥

⊤ : Fm
⊤ = ¬ ⊥

infix 40 _∨_
_∨_ : Fm → Fm → Fm
ϕ ∨ ψ = ¬ ϕ ↝ ψ

infix 30 _∧_
_∧_ : Fm → Fm → Fm
ϕ ∧ ψ = ¬ (ϕ ↝ ¬ ψ)

♢ : Fm → Fm
♢ ϕ = ¬ (□ (¬ ϕ))

_↔_ : Fm → Fm → Fm
ϕ ↔ ψ = (ϕ ↝ ψ) ∧ (ψ ↝ ϕ)

instance
  NumNat : Number Nat
  NumNat .Number.Constraint _ = U.⊤
  NumNat .Number.fromNat    m = m

-- Proof of membership
data _∈_ : Fm → List Fm → Set where
  zero : ∀ {ϕ : Fm} {Σ : List Fm} → ϕ ∈ (ϕ ∷ Σ)
  suc : ∀ {ϕ Σ ψ} → ϕ ∈ Σ → ϕ ∈ (ψ ∷ Σ)

_!_ : ∀ {A : Set} → (L : List A) → (i : Nat) → Maybe A
[] ! _ = nothing
(x ∷ l) ! zero = just x
(x ∷ l) ! suc i = l ! i

makeMember : ∀ {Σ i ϕ} → Σ ! i ≡ just ϕ → ϕ ∈ Σ
makeMember {a ∷ l} {zero} refl = zero
makeMember {a ∷ l} {suc i} x = suc (makeMember {l} {i} x)

instance
  NumMember : ∀ {ϕ Σ} → Number (ϕ ∈ Σ)
  NumMember {ϕ} {Σ} .Number.Constraint i = Σ ! i ≡ just ϕ
  NumMember {ϕ} {Σ} .Number.fromNat i {{p}} = makeMember p

_≤_ : (m n : Nat) → Set
zero  ≤ n     = U.⊤
suc m ≤ zero  = Empty.⊥
suc m ≤ suc n = m ≤ n

fromN≤ : (m n : Nat) → m ≤ n → Fin (suc n)
fromN≤ zero    _       _  = zero
fromN≤ (suc _) zero    ()
fromN≤ (suc m) (suc n) p  = suc (fromN≤ m n p)

instance
  NumFin : ∀ {n} → Number (Fin (suc n))
  NumFin {n} .Number.Constraint m         = m ≤ n
  NumFin {n} .Number.fromNat    m {{m≤n}} = fromN≤ m n m≤n

infix 0 _⊢_
-- A proof in K given a (finite) set of premisses Σ ⊢ ϕ
data _⊢_ (Σ : List Fm) : Fm → Set where
  C1 : ∀ {ϕ ψ : Fm} → Σ ⊢ ϕ ↝ (ψ ↝ ϕ)
  C2 : ∀ {ϕ ψ ξ : Fm} → Σ ⊢ (ϕ ↝ (ψ ↝ ξ)) ↝ (ϕ ↝ ψ) ↝ (ϕ ↝ ξ)
  C3 : ∀ {ϕ ψ : Fm} → Σ ⊢ (¬ ϕ ↝ ¬ ψ) ↝ (ψ ↝ ϕ)
  distribution : {ϕ ψ : Fm} → Σ ⊢ □ (ϕ ↝ ψ) ↝ (□ ϕ ↝ □ ψ)
  necessitation : ∀ {ϕ : Fm} → [] ⊢ ϕ → Σ ⊢ □ ϕ
  mp : ∀ {ϕ ψ : Fm} → Σ ⊢ ϕ ↝ ψ → Σ ⊢ ϕ → Σ ⊢ ψ
  iden : ∀ {ϕ : Fm} → ϕ ∈ Σ → Σ ⊢ ϕ

data Single {A : Set} : A → Set where
  single : (n : A) → Single n

instance
  NumSing : ∀ {n} → Number (Single {Nat} n)
  NumSing {n} .Number.Constraint m = n ≡ m
  NumSing .Number.fromNat m ⦃ refl ⦄ = single m

data HilbertProof : List Fm → Fm → Nat → Set
lookup-all : ∀ {Σ ϕ n} → HilbertProof Σ ϕ n → Fin (suc n) → List Fm × (Fm × Nat)
lookup-may : ∀ {Σ ϕ n} → HilbertProof Σ ϕ n → Nat → Maybe Fm

lookup : ∀ {Σ ϕ n} → HilbertProof Σ ϕ n → Fin (suc n) → Fm
lookup H i = proj₁ (proj₂ (lookup-all H i))

compile-instr : ∀ {n Σ ϕ} → (H : HilbertProof Σ ϕ n) → (i : Fin (suc n)) → Σ ⊢ lookup H i

data HilbertRef {Σ ϕ n} (H : HilbertProof Σ ϕ n) (fψ : Fm) (f : Fm → Fm) : Set where
  ref : (i : Nat) → M.map f (lookup-may H i) ≡ just fψ → HilbertRef H fψ f

data HilbertProof where
  begin : ∀ {Σ ϕ} → Σ ⊢ ϕ → HilbertProof Σ ϕ 0
  by : ∀ {Σ ϕ ψ n} → Σ ⊢ ψ → HilbertProof Σ ϕ n → HilbertProof Σ ψ (suc n)
  iden : ∀ {Σ ψ n} → (ϕ : Fm) → HilbertProof Σ ψ n → HilbertProof (ϕ ∷ Σ) ϕ (suc n)
  nec : ∀ {n Σ □ϕ ζ} (H : HilbertProof [] ζ n) (i : HilbertRef H □ϕ □) → HilbertProof Σ □ϕ (suc n)
  mp : ∀ {n Σ ϕ ψ ζ} (H : HilbertProof Σ ζ n) → HilbertRef H (ϕ ↝ ψ) id → HilbertRef H ϕ id → HilbertProof Σ ψ (suc n)

lookup-all {Σ} {ϕ} {n} x zero = Σ , ϕ , n
lookup-all (by x H) (suc i) = lookup-all H i
lookup-all (mp {ϕ} {ψ} H i' j') (suc i) = lookup-all H i
lookup-all (begin x) (suc ())
lookup-all (iden x y) (suc h) = lookup-all y h
lookup-all (nec H j) (suc i) = lookup-all H i

-- nothing if the result would be a negative number
_-≥_ : (n m : Nat) → Maybe (Fin (suc n))
n -≥ zero = just (fromℕ n)
zero -≥ suc m = nothing
suc n -≥ suc m = M.map inject₁ (n -≥ m)

lookup-may {Σ} {ϕ} {n} H i = M.map (lookup H) (n -≥ i)

is-just-map : ∀ {A B : Set} {may : Maybe A} {f : A → B}
  → M.is-just (M.map f may) ≡ M.is-just may
is-just-map {A} {B} {just _} = refl
is-just-map {A} {B} {nothing} = refl

-≥-is-just : ∀ {n i} → M.is-just (n -≥ i) ≡ true → i ≤ n
-≥-is-just {zero} {zero} x = U.tt
-≥-is-just {suc n} {zero} x = U.tt
-≥-is-just {suc n} {suc i} x = -≥-is-just {n} {i} (Eq.trans (Eq.sym is-just-map) x)

is-just-≡ : ∀ {A : Set} {a : Maybe A} {b} → a ≡ just b → M.is-just a ≡ true
is-just-≡ {_} {just _} x = refl

subst-⊢ : ∀ {Σ ϕ ϕ'} → ϕ ≡ ϕ' → Σ ⊢ ϕ → Σ ⊢ ϕ'
subst-⊢ {Σ} x y = subst (λ x →  Σ ⊢ x) x y

l-weakening : {Σ : List Fm} {ϕ ψ : Fm} (Π : Σ ⊢ ϕ) → (ψ ∷ Σ) ⊢ ϕ
l-weakening C1 = C1
l-weakening C2 = C2
l-weakening C3 = C3
l-weakening (distribution) = distribution
l-weakening (necessitation Π) = necessitation Π
l-weakening (mp Π Π₁) = mp (l-weakening Π) (l-weakening Π₁)
l-weakening (iden i) = iden (suc i)

ϕ↝ϕ : ∀ {Σ ϕ} → Σ ⊢ ϕ ↝ ϕ
ϕ↝ϕ {Σ} {ϕ} = mp (mp C2 C1) (C1 {ψ = ϕ})

injective-just : ∀ {A : Set} {a b : A} → just a ≡ just b → a ≡ b
injective-just refl = refl

_-Fin_ : (n m : Nat) → Fin (suc n)
n -Fin zero = fromℕ n
zero -Fin suc m = zero
suc n -Fin suc m = inject₁ (n -Fin m)

lookup-may-just : ∀ {n i ϕ ψ Σ} {H : HilbertProof Σ ψ n}
  → lookup-may H i ≡ just ϕ
  → lookup H (n -Fin i) ≡ ϕ
lookup-may-just {n} {i} {ϕ} {_} {Σ} {H} p = injective-just p'
  where
    aux-i≤n : ∀ {n i Σ ϕ ψ} {H : HilbertProof Σ ψ n} → lookup-may H i ≡ just ϕ → i ≤ n
    aux-i≤n {n} {i} x = -≥-is-just {n} {i} (Eq.trans (Eq.sym is-just-map) (is-just-≡ x))
    aux--≥ : ∀ {n i} → i ≤ n → n -≥ i ≡ just (n -Fin i)
    aux--≥ {n} {zero} x = refl
    aux--≥ {suc n} {suc i} x = Eq.cong (M.map inject₁) (aux--≥ {n} {i} x)
    i≤n : i ≤ n
    i≤n = aux-i≤n {n} {i} p
    n-i-just : n -≥ i ≡ just (n -Fin i)
    n-i-just = aux--≥ {n} {i} i≤n
    p-rewrite : ∀ {n i ψ Σ} {H : HilbertProof Σ ψ n}
      → n -≥ i ≡ just (n -Fin i)
      → M.map (lookup H) (n -≥ i) ≡ just ϕ
      → M.map (lookup H) (just (n -Fin i)) ≡ just ϕ
    p-rewrite x p rewrite x = p
    p' : just (lookup H (n -Fin i)) ≡ just ϕ
    p' rewrite (p-rewrite {n} {i} n-i-just p) = refl

compile-nec : ∀ {□ϕ n Σ ζ}
  → (H : HilbertProof [] ζ n) → HilbertRef H □ϕ □ → Σ ⊢ □ϕ
compile-nec {v !} H (ref i x) = absurd! {_} {lookup-may H i} {v} x
  where
  absurd! : ∀ {A : Set} {a b} → M.map □ a ≡ just (b !) → A
  absurd! {_} {just y} {b} x with injective-just (Eq.sym x)
  ... | ()
compile-nec {a ↝ b} H (ref i x) = absurd↝ {_} {lookup-may H i} {a} {b} x
  where
  absurd↝ : ∀ {A : Set} {a b c} → M.map □ a ≡ just (b ↝ c) → A
  absurd↝ {A} {just y} {b} x with injective-just (Eq.sym x)
  ... | ()
compile-nec {□ ϕ} {n} H (ref i x) =
  necessitation (subst-⊢ (lookup-may-just {n} {i} p) (compile-instr H (n -Fin i)))
  where
  aux□ : ∀ {a b} →  M.map □ a ≡ just (□ b) → a ≡ just b
  aux□ {just a} refl = refl
  p : lookup-may H i ≡ just ϕ
  p = aux□ x
compile-nec {⊥} H (ref i x) = absurd⊥ {_} {lookup-may H i} x
  where
  absurd⊥ : ∀ {A : Set} {a} → M.map □ a ≡ just ⊥ → A
  absurd⊥ {_} {just y} x with injective-just (Eq.sym x)
  ... | ()

l-weakening[] : {Σ : List Fm} {A : Fm} (Π : [] ⊢ A) → Σ ⊢ A
l-weakening[] C1 = C1
l-weakening[] C2 = C2
l-weakening[] C3 = C3
l-weakening[] distribution = distribution
l-weakening[] (mp a b) = mp (l-weakening[] a) (l-weakening[] b)
l-weakening[] (necessitation p) = necessitation p


compile-mp : ∀ {ϕ ψ n Σ ζ}
  → (H : HilbertProof Σ ζ n) → HilbertRef H (ϕ ↝ ψ) id → HilbertRef H ϕ id
  → Σ ⊢ ψ
compile-mp {ϕ} {ψ} {n} {Σ} H (ref i pi) (ref j pj) = mp Σ⊢ϕ↝ψ Σ⊢ϕ
  where
  map-id : ∀ {A : Set} {may : Maybe A} → M.map id may ≡ may
  map-id {A} {just x₁} = refl
  map-id {A} {nothing} = refl
  pi' : lookup-may H i ≡ just (ϕ ↝ ψ)
  pi' = Eq.trans (Eq.sym map-id) pi
  pj' : lookup-may H j ≡ just ϕ
  pj' = Eq.trans (Eq.sym map-id) pj
  Σ⊢ϕ : Σ ⊢ ϕ
  Σ⊢ϕ = subst-⊢ (lookup-may-just {n} {j} pj') (compile-instr H (n -Fin j))
  Σ⊢ϕ↝ψ : Σ ⊢ ϕ ↝ ψ
  Σ⊢ϕ↝ψ = subst-⊢ (lookup-may-just {n} {i} pi') (compile-instr H (n -Fin i))

compile-instr (begin x) zero = x
compile-instr (begin x) (suc ())
compile-instr (by x H) zero = x
compile-instr (by x H) (suc i) = compile-instr H i
compile-instr (mp {n} H i j) zero = compile-mp H i j
compile-instr (mp H _ _) (suc l) = compile-instr H l
compile-instr (iden ϕ x) zero = iden zero
compile-instr (iden ϕ H) (suc i) = l-weakening (compile-instr H i)
compile-instr {suc n} {Σ} (nec {n} {Σ} {□ϕ} H i) zero = l-weakening[] (compile-nec H i)
compile-instr (nec {n} H i) (suc j) = l-weakening[] (compile-instr H j)

compile : ∀ {n Σ ϕ} → HilbertProof Σ ϕ n → Σ ⊢ ϕ
compile H = compile-instr H zero

injective-□ : ∀ {ϕ ψ} → □ ϕ ≡ □ ψ → ϕ ≡ ψ
injective-□ refl = refl

instance
  NumHilbertRef : ∀ {ϕ Σ n} {H : HilbertProof Σ ϕ n} {fψ} {f} → Number (HilbertRef H fψ f)
  NumHilbertRef {ϕ} {Σ} {n} {H} {fψ} {f} .Number.Constraint t = M.map f (lookup-may H t) ≡ just fψ
  NumHilbertRef {ϕ} {Σ} {n} {H} .Number.fromNat t ⦃ x ⦄ = ref t x

begin[_]_By_ : ∀ {Σ} → Single {Nat} 0 → (ψ : Fm) → Σ ⊢ ψ → HilbertProof Σ ψ 0
begin[ z ] ψ By p = begin p

infixl 10 _[_]_By_
_[_]_By_ : ∀ {Σ ζ n} → (H : HilbertProof Σ ζ n) → Single {Nat} (suc n) → (ψ : Fm) → Σ ⊢ ψ → HilbertProof Σ ψ (suc n)
H [ n ] ψ By p = by p H

infixl 10 _[_]_ByNec_
_[_]_ByNec_ : ∀ {ζ n}
  → (H : HilbertProof [] ζ n) → Single {Nat} (suc n)
  → (□ψ : Fm) → HilbertRef H □ψ □
  → HilbertProof [] □ψ (suc n)
H [ i ] □ϕ ByNec ix = nec H ix

infixl 10 _[_]_ByMP_,_
_[_]_ByMP_,_ : ∀ {Σ ζ ϕ n} → (H : HilbertProof Σ ζ n) → Single {Nat} (suc n)
  → (ψ : Fm) (i : HilbertRef H (ϕ ↝ ψ) id) (j : HilbertRef H ϕ id) → HilbertProof Σ ψ (suc n)
H [ n ] ψ ByMP i , j = mp H i j

infix 0 _■
_■ : ∀ {n Σ ϕ} → (H : HilbertProof Σ ϕ n) → Σ ⊢ ϕ
H ■ = compile H

-- A Hilbert style proof of □ (ϕ ↝ ϕ)
□⟨ϕ↝ϕ⟩ : ∀ {ϕ} → [] ⊢ □ (ϕ ↝ ϕ)
□⟨ϕ↝ϕ⟩ {ϕ} =
  begin[ 0 ] (ϕ ↝ ((ϕ ↝ ϕ) ↝ ϕ)) ↝ ((ϕ ↝ (ϕ ↝ ϕ)) ↝ (ϕ ↝ ϕ)) By C2
       [ 1 ] ϕ ↝ ((ϕ ↝ ϕ) ↝ ϕ)                                    By C1
       [ 2 ] ϕ ↝ (ϕ ↝ ϕ)                                           By C1
       [ 3 ] (ϕ ↝ (ϕ ↝ ϕ)) ↝ ϕ ↝ ϕ                               ByMP 0 , 1
       [ 4 ] ϕ ↝ ϕ                                                  ByMP 3 , 2
       [ 5 ] □ (ϕ ↝ ϕ)                                              ByNec 4
       ■
