module _ where

import Agda.Builtin.Unit as U
import Data.Fin as Fin
import Data.List as Lst
import Relation.Binary.PropositionalEquality as Eq
import Data.Product as P
import Data.Maybe as M
import Data.Empty as Empty
import Function as Fun
open Fun using (id)
open M using (nothing; just; Maybe)
open Eq using (_≡_; refl; subst)
open Fin using (Fin; inject+; fromℕ; inject₁; zero; suc; #_)
open Lst using (List; []; _∷_)
open import Agda.Builtin.Equality
open import Agda.Builtin.Nat
open import Agda.Builtin.FromNat
open import Agda.Primitive
open import Data.Bool using (true) renaming (_∨_ to _||_ ; _∧_ to _&_)
open import Data.Nat using (_≤?_)
open import Data.Product
open import Relation.Nullary.Decidable using (True)

Var : Set
Var = Nat

infixr 20 _↝_
data Fm : Set where
  _! : Var → Fm
  _↝_ : Fm → Fm → Fm
  □ : Fm → Fm
  ⊥ : Fm

instance
  NumNat : Number Nat
  NumNat .Number.Constraint _ = U.⊤
  NumNat .Number.fromNat    m = m

-- Proof of membership
data Member : Fm → List Fm → Set where
  zero : ∀ {ϕ : Fm} {Σ : List Fm} → Member ϕ (ϕ ∷ Σ)
  suc : ∀ {ϕ Σ ψ} → Member ϕ Σ → Member ϕ (ψ ∷ Σ)

_!_ : ∀ {A : Set} → (L : List A) → (i : Nat) → Maybe A
[] ! _ = nothing
(x ∷ l) ! zero = just x
(x ∷ l) ! suc i = l ! i

makeMember : ∀ {Σ i ϕ} → Σ ! i ≡ just ϕ → Member ϕ Σ
makeMember {a ∷ l} {zero} refl = zero
makeMember {a ∷ l} {suc i} x = suc (makeMember {l} {i} x)

instance
  NumMember : ∀ {ϕ Σ} → Number (Member ϕ Σ)
  NumMember {ϕ} {Σ} .Number.Constraint i = Σ ! i ≡ just ϕ
  NumMember {ϕ} {Σ} .Number.fromNat i {{p}} = makeMember p

_≤_ : (m n : Nat) → Set
zero  ≤ n     = U.⊤
suc m ≤ zero  = Empty.⊥
suc m ≤ suc n = m ≤ n

fromN≤ : (m n : Nat) → m ≤ n → Fin (suc n)
fromN≤ zero    _       _  = zero
fromN≤ (suc _) zero    ()
fromN≤ (suc m) (suc n) p  = suc (fromN≤ m n p)

instance
  NumFin : ∀ {n} → Number (Fin (suc n))
  NumFin {n} .Number.Constraint m         = m ≤ n
  NumFin {n} .Number.fromNat    m {{m≤n}} = fromN≤ m n m≤n

¬ : Fm → Fm
¬ ϕ = ϕ ↝ ⊥

⊤ : Fm
⊤ = ¬ ⊥

_∨_ : Fm → Fm → Fm
ϕ ∨ ψ = ¬ ϕ ↝ ψ

infix 30 _∧_
_∧_ : Fm → Fm → Fm
ϕ ∧ ψ = ¬ (ϕ ↝ ¬ ψ)

♢ : Fm → Fm
♢ ϕ = ¬ (□ (¬ ϕ))

_↔_ : Fm → Fm → Fm
ϕ ↔ ψ = (ϕ ↝ ψ) ∧ (ψ ↝ ϕ)


infix 0 _⊢_
-- A proof in K given a set of (finite) premisses Σ ⊢ ϕ
data _⊢_ (Σ : List Fm) : Fm → Set where
  K1 : (ϕ ψ : Fm) → Σ ⊢ ϕ ↝ (ψ ↝ ϕ)
  K2 : (ϕ ψ ξ : Fm) → Σ ⊢ (ϕ ↝ (ψ ↝ ξ)) ↝ ((ϕ ↝ ψ) ↝ (ϕ ↝ ξ))
  K3 : (ϕ ψ : Fm) → Σ ⊢ (¬ ϕ ↝ ¬ ψ) ↝ (ψ ↝ ϕ)
  distribution : {ϕ ψ : Fm} → Σ ⊢ □ (ϕ ↝ ψ) ↝ (□ ϕ ↝ □ ψ)
  necessitation : ∀ {ϕ} → Σ ⊢ ϕ → Σ ⊢ □ ϕ
  mp : ∀ {ϕ ψ} → Σ ⊢ (ϕ ↝ ψ) → Σ ⊢ ϕ → Σ ⊢ ψ
  premise : ∀ {ϕ} → Member ϕ Σ → Σ ⊢ ϕ

data Single {A : Set} : A → Set where
  single : (n : A) → Single n

instance
  NumSing : ∀ {n} → Number (Single {Nat} n)
  NumSing {n} .Number.Constraint m = n ≡ m
  NumSing .Number.fromNat m ⦃ refl ⦄ = single m

data HilbertProof : List Fm → Fm → Nat → Set

at' : ∀ {Σ ϕ n} → HilbertProof Σ ϕ n → Fin (suc n) → List Fm × (Fm × Nat)

atMϕ' : ∀ {Σ ϕ n} → HilbertProof Σ ϕ n → Nat → Maybe Fm

atϕ : ∀ {Σ ϕ n} → HilbertProof Σ ϕ n → Fin (suc n) → Fm
atϕ H i = proj₁ (proj₂ (at' H i))

fromHilbert' : ∀ {n Σ ϕ} → (H : HilbertProof Σ ϕ n) → (i : Fin (suc n)) → Σ ⊢ atϕ H i

data HilbertProof where
  begin : ∀ {Σ ϕ} → Σ ⊢ ϕ → HilbertProof Σ ϕ 0
  next : ∀ {Σ ϕ ψ n} → Σ ⊢ ψ → HilbertProof Σ ϕ n → HilbertProof Σ ψ (suc n)
  premise : ∀ {Σ ψ n} → (ϕ : Fm) → HilbertProof Σ ψ n → HilbertProof (ϕ ∷ Σ) ϕ (suc n)
  nec : ∀ {n Σ ϕ ζ} (i : Nat) → (H : HilbertProof Σ ζ n) → atMϕ' H i ≡ just ϕ → HilbertProof Σ (□ ϕ) (suc n)
  mp : ∀ {n Σ ϕ ψ ζ} (i j : Nat) → (H : HilbertProof Σ ζ n) → atMϕ' H i ≡ just (ϕ ↝ ψ) → atMϕ' H j ≡ just ϕ → HilbertProof Σ ψ (suc n)

at' {Σ} {ϕ} {n} x zero = Σ , ϕ , n
at' (next x H) (suc i) = at' H i
at' (mp {ϕ} {ψ} i' j' H _ _) (suc i) = at' H i
at' (begin x) (suc ())
at' (premise x y) (suc h) = at' y h
at' (nec j H z) (suc i) = at' H i

-- nothing if the result would be a negative number
_-≥_ : (n m : Nat) → Maybe (Fin (suc n))
n -≥ zero = just (fromℕ n)
zero -≥ suc m = nothing
suc n -≥ suc m = M.map inject₁ (n -≥ m)

atMϕ' {Σ} {ϕ} {n} H i = M.map (atϕ H) (n -≥ i)

is-just-map : ∀ {A B : Set} {may : Maybe A} {f : A → B}
  → M.is-just (M.map f may) ≡ M.is-just may
is-just-map {A} {B} {just _} = refl
is-just-map {A} {B} {nothing} = refl

-≥-is-just : ∀ {n i} → M.is-just (n -≥ i) ≡ true → i ≤ n
-≥-is-just {zero} {zero} x = U.tt
-≥-is-just {suc n} {zero} x = U.tt
-≥-is-just {suc n} {suc i} x = -≥-is-just {n} {i} (Eq.trans (Eq.sym is-just-map) x)

is-just-≡ : ∀ {A : Set} {a : Maybe A} {b} → a ≡ just b → M.is-just a ≡ true
is-just-≡ {_} {just _} x = refl

subst-⊢ : ∀ {Σ ϕ ϕ'} → ϕ ≡ ϕ' → Σ ⊢ ϕ → Σ ⊢ ϕ'
subst-⊢ {Σ} x y = subst (λ x →  Σ ⊢ x) x y

l-weakening : {Σ : List Fm} {ϕ ψ : Fm} (Π : Σ ⊢ ϕ) → (ψ ∷ Σ) ⊢ ϕ
l-weakening (K1 ϕ ψ) = K1 ϕ ψ
l-weakening (K2 ϕ ψ ξ) = K2 ϕ ψ ξ
l-weakening (K3 ϕ ψ) = K3 ϕ ψ
l-weakening (distribution) = distribution
l-weakening (necessitation Π) = necessitation (l-weakening Π)
l-weakening (mp Π Π₁) = mp (l-weakening Π) (l-weakening Π₁)
l-weakening (premise i) = premise (suc i)

ϕ↝ϕ : ∀ {Σ ϕ} → Σ ⊢ (ϕ ↝ ϕ)
ϕ↝ϕ {Σ} {ϕ} = mp (mp (K2 ϕ (ϕ ↝ ϕ) ϕ) (K1 ϕ (ϕ ↝ ϕ))) (K1 ϕ ϕ)

l*-weakening : ∀ {ϕ} → (Σ : List Fm) → [] ⊢ ϕ → Σ ⊢ ϕ
l*-weakening Σ (K1 ϕ ψ) = K1 ϕ ψ
l*-weakening Σ (K2 ϕ ψ ξ) = K2 ϕ ψ ξ
l*-weakening Σ (K3 ϕ ψ) = K3 ϕ ψ
l*-weakening Σ (distribution) = distribution
l*-weakening Σ (necessitation x) = necessitation (l*-weakening Σ x)
l*-weakening Σ (mp x x₁) = mp (l*-weakening Σ x) (l*-weakening Σ x₁)
l*-weakening Σ (premise ())

r-weakening : ∀ {ϕ Σ ψ} (Π : Σ ⊢ ψ) → Σ ⊢ (ϕ ↝ ψ)
r-weakening Π = mp (K1 _ _) Π

-- or-left : ∀ {a b} → a || b ≡ false → a ≡ false
-- or-left {false} {b} e = refl
-- or-left {true} {b} ()

-- or-right : ∀ {a b} → a || b ≡ false → b ≡ false
-- or-right {a} {false} e = refl
-- or-right {false} {true} e = e
-- or-right {true} {true} e = e

injective-just : ∀ {A : Set} {a b : A} → just a ≡ just b → a ≡ b
injective-just refl = refl

_-Fin_ : (n m : Nat) → Fin (suc n)
n -Fin zero = fromℕ n
zero -Fin suc m = zero
suc n -Fin suc m = inject₁ (n -Fin m)

atMϕ'-just : ∀ {n i ϕ ψ Σ} {H : HilbertProof Σ ψ n}
  → atMϕ' H i ≡ just ϕ
  → atϕ H (n -Fin i) ≡ ϕ
atMϕ'-just {n} {i} {ϕ} {_} {Σ} {H} p = injective-just p'
  where
    aux-i≤n : ∀ {n i Σ ϕ ψ} {H : HilbertProof Σ ψ n} → atMϕ' H i ≡ just ϕ → i ≤ n
    aux-i≤n {n} {i} x = -≥-is-just {n} {i} (Eq.trans (Eq.sym is-just-map) (is-just-≡ x))
    aux--≥ : ∀ {n i} → i ≤ n → n -≥ i ≡ just (n -Fin i)
    aux--≥ {n} {zero} x = refl
    aux--≥ {suc n} {suc i} x = Eq.cong (M.map inject₁) (aux--≥ {n} {i} x)
    i≤n : i ≤ n
    i≤n = aux-i≤n {n} {i} p
    n-i-just : n -≥ i ≡ just (n -Fin i)
    n-i-just = aux--≥ {n} {i} i≤n
    p-rewrite : ∀ {n i ψ Σ} {H : HilbertProof Σ ψ n}
      → n -≥ i ≡ just (n -Fin i)
      → M.map (atϕ H) (n -≥ i) ≡ just ϕ
      → M.map (atϕ H) (just (n -Fin i)) ≡ just ϕ
    p-rewrite x p rewrite x = p
    p' : just (atϕ H (n -Fin i)) ≡ just ϕ
    p' rewrite (p-rewrite {n} {i} n-i-just p) = refl

fromHilbert' (begin x) zero = x
fromHilbert' (begin x) (suc ())
fromHilbert' (next x H) zero = x
fromHilbert' (next x H) (suc i) = fromHilbert' H i
fromHilbert' (mp {n} i j H pi pj) zero = mp (subst-⊢ (atMϕ'-just {n} {i} pi) (fromHilbert' H (n -Fin i))) (subst-⊢ (atMϕ'-just {n} {j} pj) (fromHilbert' H (n -Fin j)))
fromHilbert' (mp _ _ H _ _) (suc l) = fromHilbert' H l
fromHilbert' (premise ϕ x) zero = premise zero
fromHilbert' (premise ϕ H) (suc i) = l-weakening (fromHilbert' H i)
fromHilbert' {suc n} {Σ} (nec {n} {ϕ = ϕ} i H p) zero =
  necessitation (subst-⊢ (atMϕ'-just {n} {i} p) (fromHilbert' H (n -Fin i)))
fromHilbert' (nec {n} i H p) (suc j) = fromHilbert' H j

fromHilbert : ∀ {n Σ ϕ} → (H : HilbertProof Σ ϕ n) → Σ ⊢ ϕ
fromHilbert H = fromHilbert' H zero

begin[_]_By_ : ∀ {Σ} → Single {Nat} 0 → (ψ : Fm) → Σ ⊢ ψ → HilbertProof Σ ψ 0
begin[ n ] ψ By p = begin p

infixl 10 _[_]_By_
_[_]_By_ : ∀ {Σ ζ n} → (H : HilbertProof Σ ζ n) → Single {Nat} (suc n) → (ψ : Fm) → Σ ⊢ ψ → HilbertProof Σ ψ (suc n)
H [ n ] ψ By p = next p H

data HilbertIx {Σ ϕ n} (H : HilbertProof Σ ϕ n) (fψ : Fm) (f : Fm → Fm) : Set where
  hilb-ix : (i : Nat) → M.map f (atMϕ' H i) ≡ just fψ → HilbertIx H fψ f

injective-□ : ∀ {ϕ ψ} → □ ϕ ≡ □ ψ → ϕ ≡ ψ
injective-□ refl = refl

instance
  NumNecIx : ∀ {ϕ Σ n} {H : HilbertProof Σ ϕ n} {fψ} {f} → Number (HilbertIx H fψ f)
  NumNecIx {ϕ} {Σ} {n} {H} {fψ} {f} .Number.Constraint t = M.map f (atMϕ' H t) ≡ just fψ
  NumNecIx {ϕ} {Σ} {n} {H} .Number.fromNat t ⦃ x ⦄ = hilb-ix t x

infixl 10 _[_]_ByNec_
_[_]_ByNec_ : ∀ {Σ ζ n}
  → (H : HilbertProof Σ ζ n) → Single {Nat} (suc n)
  → (□ψ : Fm) → HilbertIx H □ψ □
  → HilbertProof Σ □ψ (suc n)
H [ _ ] v ! ByNec hilb-ix i x = absurd! {_} {atMϕ' H i} {v} x
  where
  absurd! : ∀ {A : Set} {a b} → M.map □ a ≡ just (b !) → A
  absurd! {_} {just y} {b} x with injective-just (Eq.sym x)
  ... | ()
H [ _ ] a ↝ b ByNec hilb-ix i x = absurd↝ {_} {atMϕ' H i} {a} {b} x
  where
  absurd↝ : ∀ {A : Set} {a b c} → M.map □ a ≡ just (b ↝ c) → A
  absurd↝ {A} {just y} {b} x with injective-just (Eq.sym x)
  ... | ()
H [ _ ] □ ψ ByNec hilb-ix i x = nec i H (aux□ x)
  where
  aux□ : ∀ {a b} →  M.map □ a ≡ just (□ b) → a ≡ just b
  aux□ {just a} refl = refl
H [ _ ] ⊥ ByNec hilb-ix i x =  absurd⊥ {_} {atMϕ' H i} x
  where
  absurd⊥ : ∀ {A : Set} {a} → M.map □ a ≡ just ⊥ → A
  absurd⊥ {_} {just y} x with injective-just (Eq.sym x)
  ... | ()

infixl 10 _[_]_ByMP_,_
_[_]_ByMP_,_ : ∀ {Σ ζ ϕ n} → (H : HilbertProof Σ ζ n) → Single {Nat} (suc n)
  → (ψ : Fm) (i : HilbertIx H (ϕ ↝ ψ) id) (j : HilbertIx H ϕ id) → HilbertProof Σ ψ (suc n)
H [ n ] ψ ByMP (hilb-ix i pi) , (hilb-ix j pj) =
  mp i j H (Eq.trans (Eq.sym map-id) pi) (Eq.trans (Eq.sym map-id) pj)
  where
    map-id : ∀ {A : Set} {may : Maybe A} → M.map id may ≡ may
    map-id {A} {just x₁} = refl
    map-id {A} {nothing} = refl

infix 0 _■
_■ : ∀ {n Σ ϕ} → (H : HilbertProof Σ ϕ n) → Σ ⊢ ϕ
H ■ = fromHilbert H

k1 : ∀ {ϕ ψ Σ} → Σ ⊢ ϕ ↝ (ψ ↝ ϕ)
k1 {ϕ} {ψ} = K1 ϕ ψ

k2 : ∀ {ϕ ψ ξ Σ} → Σ ⊢ (ϕ ↝ ψ ↝ ξ) ↝ (ϕ ↝ ψ) ↝ (ϕ ↝ ξ)
k2 {ϕ} {ψ} {ξ} = K2 ϕ ψ ξ

k3 : ∀ {ϕ ψ Σ} → Σ ⊢ (¬ ϕ ↝ ¬ ψ) ↝ (ψ ↝ ϕ)
k3 {ϕ} {ψ} = K3 ϕ ψ

-- A Hilbert style proof of □ (ϕ ↝ ϕ)
□⟨ϕ↝ϕ⟩ : ∀ {Σ ϕ} → Σ ⊢ □ (□ (ϕ ↝ ϕ))
□⟨ϕ↝ϕ⟩ {Σ} {ϕ} =
  begin[ 0 ] (ϕ ↝ ((ϕ ↝ ϕ) ↝ ϕ)) ↝ ((ϕ ↝ (ϕ ↝ ϕ)) ↝ (ϕ ↝ ϕ)) By k2
       [ 1 ] ϕ ↝ ((ϕ ↝ ϕ) ↝ ϕ)                                  By k1
       [ 2 ] ϕ ↝ (ϕ ↝ ϕ)                                        By k1
       [ 3 ] (ϕ ↝ (ϕ ↝ ϕ)) ↝ ϕ ↝ ϕ                             ByMP 0 , 1
       [ 4 ] ϕ ↝ ϕ                                               ByMP 3 , 2
       [ 5 ] □ (ϕ ↝ ϕ)                                           ByNec 4
       [ 6 ] □ (□ (ϕ ↝ ϕ))                                       ByNec 5
       ■

-- returns true iff the proof uses necessitation.
-- usesNecessitation : ∀ {Σ ϕ} → Σ ⊢ ϕ → Bool
-- usesNecessitation (K1 ϕ ψ) = false
-- usesNecessitation (K2 ϕ ψ ξ) = false
-- usesNecessitation (K3 ϕ ψ) = false
-- usesNecessitation (distribution) = false
-- usesNecessitation (necessitation x) = true
-- usesNecessitation (mp x y) = usesNecessitation x || usesNecessitation y
-- usesNecessitation (premise x) = false


-- ϕ↝ψ∷ϕ∷Σ⊢ψ : ∀ {ϕ ψ Σ} → ϕ ↝ ψ ∷ ϕ ∷ Σ ⊢ ψ
-- ϕ↝ψ∷ϕ∷Σ⊢ψ {ϕ} {ψ} =
--   begin[ 0 ] ϕ ↝ ψ By premise 0
--        [ 1 ] ϕ By premise 1
--        [ 2 ] ψ ByMP  0 , 1 ⟨ refl , refl ⟩
--   ■

-- deductThm2-aux : ∀ {Σ : List Fm} {ϕ ψ : Fm} (Π : (ϕ ∷ Σ) ⊢ ψ) → usesNecessitation Π ≡ false →  Σ ⊢ (ϕ ↝ ψ)
-- deductThm2-aux {Σ} {ϕ} {.(γ ↝ (ψ ↝ γ))} (K1 γ ψ) n = r-weakening (K1 _ _)
-- deductThm2-aux {Σ} {ϕ} {.((γ ↝ (ψ ↝ ξ)) ↝ ((γ ↝ ψ) ↝ (γ ↝ ξ)))} (K2 γ ψ ξ) x =
--   r-weakening (K2 _ _ _)
-- deductThm2-aux {Σ} {ϕ} {.(((γ ↝ ⊥) ↝ (ψ ↝ ⊥)) ↝ (ψ ↝ γ))} (K3 γ ψ) x = r-weakening (K3 _ _)
-- deductThm2-aux {Σ} {ϕ} (distribution) x =
--   r-weakening (distribution)
-- deductThm2-aux {Σ} {ϕ} {ψ} (mp {x} {y} a b) (u) = mp (mp (K2 ϕ x ψ) (deductThm2-aux a (or-left u))) (deductThm2-aux b (or-right u))
-- deductThm2-aux {Σ} {.ψ} {ψ} (premise (zero)) u = l*-weakening Σ ϕ↝ϕ
-- deductThm2-aux {Σ} {ϕ} {ψ} (premise (suc x)) u = r-weakening (premise x)
-- deductThm2-aux {Σ} {ϕ} {.(□ _)} (necessitation p) ()


-- -- A proof in K that does not use necessitation.
-- record _⊢l_ (Σ : List Fm) (ϕ : Fm) : Set where
--   constructor local
--   field
--     prf : Σ ⊢ ϕ
--     noNec : usesNecessitation prf ≡ false

-- -- A proof of a tautology in K, ∅ ⊢ ϕ
-- K : Fm → Set
-- K ϕ = [] ⊢ ϕ

-- -- A proof of ϕ ↝ ϕ without using the deduction theorem.
-- deductThm1 : {Σ : List Fm} {ϕ ψ : Fm} (Π : Σ ⊢ (ϕ ↝ ψ)) → (ϕ ∷ Σ) ⊢ ψ
-- deductThm1 {Σ} {ϕ} Π = mp (l-weakening Π) (premise 0)

-- -- TODO: Too restrictive!
-- deductThm : ∀ {Σ : List Fm} {ϕ ψ : Fm} (Π : (ϕ ∷ Σ) ⊢l ψ) →  Σ ⊢ (ϕ ↝ ψ)
-- deductThm (local prf noNec) = deductThm2-aux prf noNec

-- -- A proof of ϕ ↝ ϕ using the deduction theorem.
-- ϕ↝ϕ' : ∀ {Σ ϕ} → Σ ⊢ (ϕ ↝ ϕ)
-- ϕ↝ϕ' = deductThm (local (premise 0) refl)

-- cut : ∀ {Σ ϕ ψ} → Σ ⊢ ϕ → (ϕ ∷ Σ) ⊢ ψ → Σ ⊢ ψ
-- cut x (K1 ϕ ψ) = K1 ϕ ψ
-- cut x (K2 ϕ ψ ξ) = K2 ϕ ψ ξ
-- cut x (K3 ϕ ψ) = K3 ϕ ψ
-- cut x distribution = distribution
-- cut x (necessitation y) = necessitation (cut x y)
-- cut x (mp y y₁) = mp (cut x y) (cut x y₁)
-- cut x (premise zero) = x
-- cut x (premise (suc y)) = premise y

-- -- Some theorems.

-- -- excluded middle.
-- ϕ∨¬ϕ : ∀ {ϕ Σ} → Σ ⊢ ϕ ∨ ¬ ϕ
-- ϕ∨¬ϕ = ϕ↝ϕ

-- -- transitivity.
-- trans : ∀ {ϕ ψ ξ Σ} → Σ ⊢ (ϕ ↝ ψ) ↝ ((ψ ↝ ξ) ↝ (ϕ ↝ ξ))
-- trans {ϕ} {ψ} {ξ} =
--  deductThm (local (deductThm (local (mp (mp (K2 ϕ ψ ξ) (r-weakening (premise 0))) (premise 1)) refl)) refl)

-- -- double ¬ intro.
-- ϕ↝¬¬ϕ : ∀ {ϕ Σ} → Σ ⊢ ϕ ↝ (¬ (¬ ϕ))
-- ϕ↝¬¬ϕ {ϕ} = deductThm (local (deductThm (local (mp (premise 0) (premise (suc zero))) refl)) refl)

-- -- double ¬ elim.
-- ¬¬ϕ↝ϕ : ∀ {ϕ : Fm} {Σ} → Σ ⊢ (¬ (¬ ϕ)) ↝ ϕ
-- ¬¬ϕ↝ϕ {ϕ} = mp (K3 ϕ (¬ (¬ ϕ))) ϕ↝¬¬ϕ

-- -- ∨ commut
-- ϕ∨ψ↝ψ∨ϕ : ∀ {Σ ϕ ψ} → Σ ⊢ (ϕ ∨ ψ) ↝ (ψ ∨ ϕ)
-- ϕ∨ψ↝ψ∨ϕ {Σ} {ϕ} {ψ} = deductThm (local (deductThm (local (mp ¬¬ϕ↝ϕ (mp (mp trans (premise (suc zero))) (premise 0))) refl)) refl)

-- -- contraposition
-- ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ↝ ψ) ↝ (¬ ψ ↝ ¬ ϕ)
-- ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ {ϕ} {ψ} = deductThm (local (deductThm (local (mp (mp trans (premise (suc zero))) (premise 0)) refl)) refl)

-- -- ex falso quodlibet
-- ⊥↝ϕ : ∀ {ϕ Σ} → Σ ⊢ ⊥ ↝ ϕ
-- ⊥↝ϕ {ϕ} {Σ} = deductThm (local (
--   let
--       s0 : (⊥ ∷ Σ) ⊢ (¬ ⊥)
--       s0 = r-weakening {⊥} (premise 0)
--       s1 : (¬ ⊥ ∷ ⊥ ∷ Σ) ⊢ (¬ ϕ ↝ ⊥)
--       s1 = mp (K1 ⊥ (¬ ϕ)) (premise (suc zero))
--       s2 = mp ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ s1
--   in cut s0 (mp ¬¬ϕ↝ϕ (mp s2 (premise 0)))) refl)

-- -- absurd
-- ¬ϕ↝ϕ↝ψ : ∀ {ϕ ψ Σ} → Σ ⊢ (¬ ϕ) ↝ (ϕ ↝ ψ)
-- ¬ϕ↝ϕ↝ψ {ϕ} {ψ} {Σ} = mp (mp trans (K1 (¬ ϕ) (¬ ψ))) (K3 ψ ϕ)

-- -- absorb
-- ⟨ϕ↝⟨ϕ↝ψ⟩⟩↝⟨ϕ↝ψ⟩⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ↝ (ϕ ↝ ψ)) ↝ (ϕ ↝ ψ)
-- ⟨ϕ↝⟨ϕ↝ψ⟩⟩↝⟨ϕ↝ψ⟩⟩ = deductThm (local (deductThm (local (mp (mp (premise (suc zero)) (premise 0)) (premise 0)) refl)) refl)

-- -- ∨ elim
-- ϕ∨ϕ↝ϕ : ∀ {ϕ Σ} → Σ ⊢ (ϕ ∨ ϕ) ↝ ϕ
-- ϕ∨ϕ↝ϕ {ϕ} {Σ} = deductThm (local (mp ¬¬ϕ↝ϕ (mp ⟨ϕ↝⟨ϕ↝ψ⟩⟩↝⟨ϕ↝ψ⟩⟩ (mp (mp trans (premise 0)) ϕ↝¬¬ϕ))) refl)

-- -- reductio ad absurdum
-- ¬ϕ↝¬ψ↝¬ϕ↝ψ↝ϕ : ∀ {ϕ ψ Σ} → Σ ⊢ (¬ ϕ ↝ ¬ ψ) ↝ ((¬ ϕ ↝ ψ) ↝ ϕ)
-- ¬ϕ↝¬ψ↝¬ϕ↝ψ↝ϕ {ϕ} {ψ} {Σ} = deductThm (local (deductThm (local (
--   let
--     s1 : ((¬ ϕ ↝ ψ) ∷ (¬ ϕ ↝ ¬ ψ) ∷ Σ) ⊢ (ψ ↝ ϕ)
--     s1 = mp (K3 ϕ ψ) (premise (suc zero))
--     s2 = mp (mp trans (premise 0)) s1
--     in mp ϕ∨ϕ↝ϕ s2) refl)) refl)

-- -- top
-- ⊢⊤ : ∀ {Σ} → Σ ⊢ ⊤
-- ⊢⊤ = ⊥↝ϕ

-- -- top elim
-- ⟨⟨⊤↝ϕ⟩↝ϕ⟩ : ∀ {Σ ϕ} → Σ ⊢ (⊤ ↝ ϕ) ↝ ϕ
-- ⟨⟨⊤↝ϕ⟩↝ϕ⟩ = deductThm (local (mp (premise 0) ⊢⊤) refl)

-- -- ∧ r-elim
-- ⟨ϕ∧ψ⟩↝ϕ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ∧ ψ) ↝ ϕ
-- ⟨ϕ∧ψ⟩↝ϕ = mp (mp trans (mp ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ ¬ϕ↝ϕ↝ψ)) ¬¬ϕ↝ϕ

-- -- ∧ l-elim
-- ⟨ϕ∧ψ⟩↝ψ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ∧ ψ) ↝ ψ
-- ⟨ϕ∧ψ⟩↝ψ = mp (mp trans (mp ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ k1)) ¬¬ϕ↝ϕ

-- -- ∧ commut
-- ϕ∧ψ↝ψ∧ϕ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ∧ ψ) ↝ (ψ ∧ ϕ)
-- ϕ∧ψ↝ψ∧ϕ {ϕ} {ψ} = deductThm (local (deductThm (local (
--  begin[ 0 ] (ϕ ∧ ψ) ↝ ψ By ⟨ϕ∧ψ⟩↝ψ
--    [ 1 ] (ϕ ∧ ψ) ↝ ϕ By ⟨ϕ∧ψ⟩↝ϕ
--    [ 2 ] ϕ ∧ ψ By (premise (suc zero))
--    [ 3 ] ϕ ByMP  1 ,  2 ⟨ refl , refl ⟩
--    [ 4 ] ψ ByMP  0 ,  2 ⟨ refl , refl ⟩
--    [ 5 ] ψ ↝ (¬ ϕ) By (premise 0)
--    [ 6 ] ¬ ϕ ByMP  5 ,  4 ⟨ refl , refl ⟩
--    [ 7 ] ⊥ ByMP  6 ,  3 ⟨ refl , refl ⟩
--    ■
--   ) refl)) refl)

-- -- ↔ commut
-- ⟨ϕ↔ψ⟩↝⟨ψ↔ϕ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ↔ ψ) ↝ (ψ ↔ ϕ)
-- ⟨ϕ↔ψ⟩↝⟨ψ↔ϕ⟩ = ϕ∧ψ↝ψ∧ϕ

-- -- ↔ r-elim
-- ⟨ϕ↔ψ⟩↝⟨ϕ↝ψ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ↔ ψ) ↝ (ϕ ↝ ψ)
-- ⟨ϕ↔ψ⟩↝⟨ϕ↝ψ⟩ = ⟨ϕ∧ψ⟩↝ϕ

-- -- ↔ l-elim
-- ⟨ϕ↔ψ⟩↝⟨ψ↝ϕ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ (ϕ ↔ ψ) ↝ (ψ ↝ ϕ)
-- ⟨ϕ↔ψ⟩↝⟨ψ↝ϕ⟩ = ⟨ϕ∧ψ⟩↝ψ

-- -- ∧ intro
-- ϕ↝ψ↝⟨ϕ∧ψ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ ϕ ↝ (ψ ↝ (ϕ ∧ ψ))
-- ϕ↝ψ↝⟨ϕ∧ψ⟩ {ϕ} {ψ} = deductThm (local (deductThm (local (deductThm (local (
--   begin[ 0 ] ϕ By premise (suc (suc zero))
--        [ 1 ] ψ By premise (suc zero)
--        [ 2 ] ϕ ↝ (¬ ψ) By premise 0
--        [ 3 ] ¬ ψ ByMP  2 ,  0 ⟨ refl , refl ⟩
--        [ 4 ] ⊥ ByMP  3 ,  1 ⟨ refl , refl ⟩
--   ■
--   ) refl)) refl)) refl)

-- -- ∧ intro 2 TODO IDK agda loops when type checking this proof. Probably a bug.
-- -- ⟨ϕ↝ψ⟩↝⟨ϕ↝ζ⟩↝ϕ↝⟨ψ∧ζ⟩ : ∀ {ϕ ψ ζ Σ} → Σ ⊢ (ϕ ↝ ψ) ↝ (ϕ ↝ ζ) ↝ ϕ ↝ (ψ ∧ ζ)
-- -- ⟨ϕ↝ψ⟩↝⟨ϕ↝ζ⟩↝ϕ↝⟨ψ∧ζ⟩ {ϕ} {ψ} {ζ} = deductThm (local (deductThm (local (deductThm (local (deductThm (local (
-- --   begin[ 0 ] ψ ↝ ¬ ζ By premise 0
-- --    [ 1 ]  ϕ By premise (suc zero)
-- --    [ 2 ] ϕ ↝ ζ By premise (suc (suc zero))
-- --    [ 3 ] ϕ ↝ ψ By premise (suc (suc (suc zero)))
-- --    [ 4 ] ψ ByMP  3 ,  1 ⟨ refl , refl ⟩
-- --    [ 5 ] ζ ByMP  2 ,  1 ⟨ refl , refl ⟩
-- --    [ 6 ] ¬ ζ ByMP  0 ,  4 ⟨ refl , refl ⟩
-- --    [ 7 ] ⊥ ByMP  6 ,  5 ⟨ refl , refl ⟩
-- --    ■
-- --   ) refl)) refl)) refl)) refl)

-- -- conjunction property
-- conj-prop : ∀ {ϕ ψ Σ} → Σ ⊢ ϕ → Σ ⊢ ψ → Σ ⊢ ϕ ∧ ψ
-- conj-prop a b = mp (mp ϕ↝ψ↝⟨ϕ∧ψ⟩ a) b

-- -- distrib nec
-- ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ : ∀ {ϕ ψ Σ} → Σ ⊢ ϕ ↝ ψ → Σ ⊢ □ ϕ ↝ □ ψ
-- ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ {ϕ} {ψ} x = mp distribution (necessitation x)

-- -- □ ↔ distrib
-- ⊢⟨ϕ↔ψ⟩⇒⊢⟨□ϕ↔□ψ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ ϕ ↔ ψ → Σ ⊢ (□ ϕ) ↔ (□ ψ)
-- ⊢⟨ϕ↔ψ⟩⇒⊢⟨□ϕ↔□ψ⟩ {ϕ} {ψ} {Σ} p = let
--   s1 : Σ ⊢ (□ ψ ↝ □ ϕ)
--   s1 = ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ (mp ⟨ϕ↔ψ⟩↝⟨ψ↝ϕ⟩ p)
--   s2 : Σ ⊢ (□ ϕ ↝ □ ψ)
--   s2 = ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ (mp ⟨ϕ↔ψ⟩↝⟨ϕ↝ψ⟩ p)
--   in conj-prop s2 s1

-- -- curry
-- ⟨ϕ∧ψ↝ζ⟩↝ϕ↝ψ↝ζ : ∀ {ϕ ψ ζ Σ} → Σ ⊢ ((ϕ ∧ ψ) ↝ ζ) ↝ ϕ ↝ ψ ↝ ζ
-- ⟨ϕ∧ψ↝ζ⟩↝ϕ↝ψ↝ζ = deductThm (local (deductThm (local (deductThm (local
--   (mp (premise (suc (suc zero))) (mp (mp ϕ↝ψ↝⟨ϕ∧ψ⟩ (premise (suc zero))) (premise 0)))
--   refl)) refl)) refl)

-- -- uncurry
-- ⟨ϕ↝ψ↝ζ⟩↝ϕ∧ψ↝ζ : ∀ {ϕ ψ ζ Σ} → Σ ⊢ (ϕ ↝ ψ ↝ ζ) ↝ (ϕ ∧ ψ) ↝ ζ
-- ⟨ϕ↝ψ↝ζ⟩↝ϕ∧ψ↝ζ {ϕ} {ψ} {ζ} = deductThm (local (deductThm (local (
--   begin[ 0 ] ϕ By mp ⟨ϕ∧ψ⟩↝ϕ (premise 0)
--        [ 1 ] ψ By mp ⟨ϕ∧ψ⟩↝ψ (premise 0)
--        [ 2 ] ϕ ↝ ψ ↝ ζ By premise (suc zero)
--        [ 3 ] ψ ↝ ζ ByMP 2 , 0 ⟨ refl , refl ⟩
--        [ 4 ] ζ ByMP 3 , 1 ⟨ refl , refl ⟩
--   ■
--   ) refl)) refl)

-- -- □ ∧ distrib
-- -- □⟨ϕ∧ψ⟩↝⟨□ϕ∧□ψ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ □ (ϕ ∧ ψ) ↝ □ ϕ ∧ □ ψ
-- -- □⟨ϕ∧ψ⟩↝⟨□ϕ∧□ψ⟩ {ϕ} {ψ} {Σ} = let
-- --   s1 : Σ ⊢ □ (ϕ ∧ ψ) ↝ □ ϕ
-- --   s1 = ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ ⟨ϕ∧ψ⟩↝ϕ
-- --   s2 : Σ ⊢ □ (ϕ ∧ ψ) ↝ □ ψ
-- --   s2 = ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ ⟨ϕ∧ψ⟩↝ψ
-- --   in mp (mp ⟨ϕ↝ψ⟩↝⟨ϕ↝ζ⟩↝ϕ↝⟨ψ∧ζ⟩ s1) s2

-- -- □ ∧ distrib
-- ⟨□ϕ∧□ψ⟩↝□⟨ϕ∧ψ⟩ : ∀ {ϕ ψ Σ} → Σ ⊢ □ ϕ ∧ □ ψ  ↝ □ (ϕ ∧ ψ)
-- ⟨□ϕ∧□ψ⟩↝□⟨ϕ∧ψ⟩ {ϕ} {ψ} {Σ} = let
--   s3 : Σ ⊢ □ ϕ ↝ □ (ψ ↝ ϕ ∧ ψ)
--   s3 = ⊢ϕ↝ψ⇒⊢□ϕ↝□ψ ϕ↝ψ↝⟨ϕ∧ψ⟩
--   s4 : Σ ⊢ □ (ψ ↝ ϕ ∧ ψ) ↝ (□ ψ ↝ □ (ϕ ∧ ψ))
--   s4 = distribution
--   s5 : Σ ⊢ □ ϕ ↝ (□ ψ ↝ □ (ϕ ∧ ψ))
--   s5 = mp (mp trans s3) s4
--   in mp ⟨ϕ↝ψ↝ζ⟩↝ϕ∧ψ↝ζ s5

-- -- Consistency implication
-- ⊢ϕ↝ψ⇒⊢♢ϕ↝♢ψ : ∀ {ϕ ψ Σ} → Σ ⊢ ϕ ↝ ψ → Σ ⊢ ♢ ϕ ↝ ♢ ψ
-- ⊢ϕ↝ψ⇒⊢♢ϕ↝♢ψ x = let
--   s1 = mp ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ x
--   in mp ⟨ϕ↝ψ⟩↝⟨¬ψ↝¬ϕ⟩ (⊢ϕ↝ψ⇒⊢□ϕ↝□ψ s1)
