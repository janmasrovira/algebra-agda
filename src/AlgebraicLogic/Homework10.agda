module AlgebraicLogic.Homework10 where

open import Relation.Binary.Lattice using (Infimum; Supremum; IsLattice; IsDistributiveLattice)
open import Relation.Binary.Properties.Lattice using (∨-absorbs-∧; ∧-absorbs-∨)
open import Relation.Binary using (REL; Rel; IsDecEquivalence; IsEquivalence)
open import Relation.Binary.Definitions using (Reflexive; Symmetric; Transitive; Antisymmetric; Minimum)
open import Relation.Binary.PropositionalEquality using (_≡_; refl; setoid; isDecEquivalence; cong; subst; sym; trans; module ≡-Reasoning)
open import Relation.Binary.Structures using (IsPartialOrder; IsPreorder; IsEquivalence)
open import Agda.Builtin.Unit using (tt; ⊤)
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Empty using (⊥; ⊥-elim)
open import Agda.Primitive using (Level; lzero; lsuc)
open import Data.Product using (Σ; proj₁; proj₂; _×_; _,_)
open import Function using (_∘_)

data 𝑩 : Set where
  Z : 𝑩
  A : 𝑩
  B : 𝑩
  C : 𝑩
  D : 𝑩
  O : 𝑩

¬ : 𝑩 → 𝑩
¬ Z = O
¬ A = A
¬ B = B
¬ C = D
¬ D = C
¬ O = Z

_∧_ : 𝑩 → 𝑩 → 𝑩
Z ∧ y = Z
A ∧ Z = Z
A ∧ A = A
A ∧ B = D
A ∧ C = A
A ∧ D = D
A ∧ O = A
B ∧ Z = Z
B ∧ A = D
B ∧ B = B
B ∧ C = B
B ∧ D = D
B ∧ O = B
C ∧ Z = Z
C ∧ A = A
C ∧ B = B
C ∧ C = C
C ∧ D = D
C ∧ O = C
D ∧ Z = Z
D ∧ A = D
D ∧ B = D
D ∧ C = D
D ∧ D = D
D ∧ O = D
O ∧ y = y

_∨_ : 𝑩 → 𝑩 → 𝑩
Z ∨ y = y
A ∨ Z = A
A ∨ A = A
A ∨ B = C
A ∨ C = C
A ∨ D = A
A ∨ O = O
B ∨ Z = B
B ∨ A = C
B ∨ B = B
B ∨ C = C
B ∨ D = B
B ∨ O = O
C ∨ Z = C
C ∨ A = C
C ∨ B = C
C ∨ C = C
C ∨ D = C
C ∨ O = O
D ∨ Z = D
D ∨ A = A
D ∨ B = B
D ∨ C = C
D ∨ D = D
D ∨ O = O
O ∨ y = O

θ₁ : Rel 𝑩 lzero
θ₁ Z Z = ⊤
θ₁ O O = ⊤
{-# CATCHALL #-}
θ₁ Z b = ⊥
{-# CATCHALL #-}
θ₁ b Z = ⊥
{-# CATCHALL #-}
θ₁ O b = ⊥
{-# CATCHALL #-}
θ₁ a O = ⊥
{-# CATCHALL #-}
θ₁ a b = ⊤

θ₂ : Rel 𝑩 lzero
θ₂ C O = ⊤
θ₂ O C = ⊤
θ₂ D Z = ⊤
θ₂ Z D = ⊤
θ₂ Z Z = ⊤
θ₂ C C = ⊤
θ₂ A A = ⊤
θ₂ B B = ⊤
θ₂ D D = ⊤
θ₂ O O = ⊤
{-# CATCHALL #-}
θ₂ a b = ⊥

θ₂-equiv : IsEquivalence θ₂
θ₂-equiv = record { refl = θ₂-ref ; sym = θ₂-sym ; trans = θ₂-trans }
  where θ₂-ref : Reflexive θ₂
        θ₂-ref {Z} = tt
        θ₂-ref {A} = tt
        θ₂-ref {B} = tt
        θ₂-ref {C} = tt
        θ₂-ref {D} = tt
        θ₂-ref {O} = tt

        θ₂-sym : Symmetric θ₂
        θ₂-sym {Z} {Z} x = tt
        θ₂-sym {Z} {D} x = tt
        θ₂-sym {A} {A} x = tt
        θ₂-sym {B} {B} x = tt
        θ₂-sym {C} {C} x = tt
        θ₂-sym {C} {O} x = tt
        θ₂-sym {D} {Z} x = tt
        θ₂-sym {D} {D} x = tt
        θ₂-sym {O} {C} x = tt
        θ₂-sym {O} {O} x = tt

        θ₂-trans : Transitive θ₂
        θ₂-trans {Z} {Z} {Z} x x₁ = tt
        θ₂-trans {Z} {Z} {D} x x₁ = tt
        θ₂-trans {Z} {D} {Z} x x₁ = tt
        θ₂-trans {Z} {D} {D} x x₁ = tt
        θ₂-trans {A} {A} {A} x x₁ = tt
        θ₂-trans {B} {B} {B} x x₁ = tt
        θ₂-trans {C} {C} {C} x x₁ = tt
        θ₂-trans {C} {C} {O} x x₁ = tt
        θ₂-trans {C} {O} {C} x x₁ = tt
        θ₂-trans {C} {O} {O} x x₁ = tt
        θ₂-trans {D} {Z} {Z} x x₁ = tt
        θ₂-trans {D} {Z} {D} x x₁ = tt
        θ₂-trans {D} {D} {Z} x x₁ = tt
        θ₂-trans {D} {D} {D} x x₁ = tt
        θ₂-trans {O} {C} {C} x x₁ = tt
        θ₂-trans {O} {C} {O} x x₁ = tt
        θ₂-trans {O} {O} {C} x x₁ = tt
        θ₂-trans {O} {O} {O} x x₁ = tt


θ₁-equiv : IsEquivalence θ₁
θ₁-equiv = record { refl = θ₁-ref ; sym = θ₁-sym ; trans = θ₁-trans }
  where θ₁-ref : Reflexive θ₁
        θ₁-ref {Z} = tt
        θ₁-ref {A} = tt
        θ₁-ref {B} = tt
        θ₁-ref {C} = tt
        θ₁-ref {D} = tt
        θ₁-ref {O} = tt

        θ₁-sym : Symmetric θ₁
        θ₁-sym {Z} {Z} x = tt
        θ₁-sym {A} {A} x = tt
        θ₁-sym {A} {B} x = tt
        θ₁-sym {A} {C} x = tt
        θ₁-sym {A} {D} x = tt
        θ₁-sym {B} {A} x = tt
        θ₁-sym {B} {B} x = tt
        θ₁-sym {B} {C} x = tt
        θ₁-sym {B} {D} x = tt
        θ₁-sym {C} {A} x = tt
        θ₁-sym {C} {B} x = tt
        θ₁-sym {C} {C} x = tt
        θ₁-sym {C} {D} x = tt
        θ₁-sym {D} {A} x = tt
        θ₁-sym {D} {B} x = tt
        θ₁-sym {D} {C} x = tt
        θ₁-sym {D} {D} x = tt
        θ₁-sym {O} {O} x = tt

        θ₁-trans : Transitive θ₁
        θ₁-trans {Z} {Z} {c} x y = y
        θ₁-trans {A} {A} {c} x y = y
        θ₁-trans {A} {B} {A} x y = tt
        θ₁-trans {A} {B} {B} x y = tt
        θ₁-trans {A} {B} {C} x y = tt
        θ₁-trans {A} {B} {D} x y = tt
        θ₁-trans {A} {C} {A} x y = tt
        θ₁-trans {A} {C} {B} x y = tt
        θ₁-trans {A} {C} {C} x y = tt
        θ₁-trans {A} {C} {D} x y = tt
        θ₁-trans {A} {D} {A} x y = tt
        θ₁-trans {A} {D} {B} x y = tt
        θ₁-trans {A} {D} {C} x y = tt
        θ₁-trans {A} {D} {D} x y = tt
        θ₁-trans {B} {A} {A} x y = tt
        θ₁-trans {B} {A} {B} x y = tt
        θ₁-trans {B} {A} {C} x y = tt
        θ₁-trans {B} {A} {D} x y = tt
        θ₁-trans {B} {B} {c} x y = y
        θ₁-trans {B} {C} {A} x y = tt
        θ₁-trans {B} {C} {B} x y = tt
        θ₁-trans {B} {C} {C} x y = tt
        θ₁-trans {B} {C} {D} x y = tt
        θ₁-trans {B} {D} {A} x y = tt
        θ₁-trans {B} {D} {B} x y = tt
        θ₁-trans {B} {D} {C} x y = tt
        θ₁-trans {B} {D} {D} x y = tt
        θ₁-trans {C} {A} {A} x y = tt
        θ₁-trans {C} {A} {B} x y = tt
        θ₁-trans {C} {A} {C} x y = tt
        θ₁-trans {C} {A} {D} x y = tt
        θ₁-trans {C} {B} {A} x y = tt
        θ₁-trans {C} {B} {B} x y = tt
        θ₁-trans {C} {B} {C} x y = tt
        θ₁-trans {C} {B} {D} x y = tt
        θ₁-trans {C} {C} {c} x y = y
        θ₁-trans {C} {D} {A} x y = tt
        θ₁-trans {C} {D} {B} x y = tt
        θ₁-trans {C} {D} {C} x y = tt
        θ₁-trans {C} {D} {D} x y = tt
        θ₁-trans {D} {A} {A} x y = tt
        θ₁-trans {D} {A} {B} x y = tt
        θ₁-trans {D} {A} {C} x y = tt
        θ₁-trans {D} {A} {D} x y = tt
        θ₁-trans {D} {B} {A} x y = tt
        θ₁-trans {D} {B} {B} x y = tt
        θ₁-trans {D} {B} {C} x y = tt
        θ₁-trans {D} {B} {D} x y = tt
        θ₁-trans {D} {C} {A} x y = tt
        θ₁-trans {D} {C} {B} x y = tt
        θ₁-trans {D} {C} {C} x y = tt
        θ₁-trans {D} {C} {D} x y = tt
        θ₁-trans {D} {D} {c} x y = y
        θ₁-trans {O} {O} {c} x y = y

θ₂-¬ : ∀ a b → θ₂ a b → θ₂ (¬ a) (¬ b)
θ₂-¬ Z Z x = tt
θ₂-¬ Z D x = tt
θ₂-¬ A A x = tt
θ₂-¬ B B x = tt
θ₂-¬ C C x = tt
θ₂-¬ C O x = tt
θ₂-¬ D Z x = tt
θ₂-¬ D D x = tt
θ₂-¬ O C x = tt
θ₂-¬ O O x = tt

θ₁-∧∨ : ∀ a₁ b₁ a₂ b₂ → θ₁ a₁ b₁ → θ₁ a₂ b₂ → θ₁ (a₁ ∧ a₂) (b₁ ∧ b₂) × θ₁ (a₁ ∨ a₂) (b₁ ∨ b₂)
θ₁-∧∨ Z Z Z Z x x₁ = tt , tt
θ₁-∧∨ Z Z A A x x₁ = tt , tt
θ₁-∧∨ Z Z A B x x₁ = tt , tt
θ₁-∧∨ Z Z A C x x₁ = tt , tt
θ₁-∧∨ Z Z A D x x₁ = tt , tt
θ₁-∧∨ Z Z B A x x₁ = tt , tt
θ₁-∧∨ Z Z B B x x₁ = tt , tt
θ₁-∧∨ Z Z B C x x₁ = tt , tt
θ₁-∧∨ Z Z B D x x₁ = tt , tt
θ₁-∧∨ Z Z C A x x₁ = tt , tt
θ₁-∧∨ Z Z C B x x₁ = tt , tt
θ₁-∧∨ Z Z C C x x₁ = tt , tt
θ₁-∧∨ Z Z C D x x₁ = tt , tt
θ₁-∧∨ Z Z D A x x₁ = tt , tt
θ₁-∧∨ Z Z D B x x₁ = tt , tt
θ₁-∧∨ Z Z D C x x₁ = tt , tt
θ₁-∧∨ Z Z D D x x₁ = tt , tt
θ₁-∧∨ Z Z O O x x₁ = tt , tt
θ₁-∧∨ A A Z Z x x₁ = tt , tt
θ₁-∧∨ A A A A x x₁ = tt , tt
θ₁-∧∨ A A A B x x₁ = tt , tt
θ₁-∧∨ A A A C x x₁ = tt , tt
θ₁-∧∨ A A A D x x₁ = tt , tt
θ₁-∧∨ A A B A x x₁ = tt , tt
θ₁-∧∨ A A B B x x₁ = tt , tt
θ₁-∧∨ A A B C x x₁ = tt , tt
θ₁-∧∨ A A B D x x₁ = tt , tt
θ₁-∧∨ A A C A x x₁ = tt , tt
θ₁-∧∨ A A C B x x₁ = tt , tt
θ₁-∧∨ A A C C x x₁ = tt , tt
θ₁-∧∨ A A C D x x₁ = tt , tt
θ₁-∧∨ A A D A x x₁ = tt , tt
θ₁-∧∨ A A D B x x₁ = tt , tt
θ₁-∧∨ A A D C x x₁ = tt , tt
θ₁-∧∨ A A D D x x₁ = tt , tt
θ₁-∧∨ A A O O x x₁ = tt , tt
θ₁-∧∨ A B Z Z x x₁ = tt , tt
θ₁-∧∨ A B A A x x₁ = tt , tt
θ₁-∧∨ A B A B x x₁ = tt , tt
θ₁-∧∨ A B A C x x₁ = tt , tt
θ₁-∧∨ A B A D x x₁ = tt , tt
θ₁-∧∨ A B B A x x₁ = tt , tt
θ₁-∧∨ A B B B x x₁ = tt , tt
θ₁-∧∨ A B B C x x₁ = tt , tt
θ₁-∧∨ A B B D x x₁ = tt , tt
θ₁-∧∨ A B C A x x₁ = tt , tt
θ₁-∧∨ A B C B x x₁ = tt , tt
θ₁-∧∨ A B C C x x₁ = tt , tt
θ₁-∧∨ A B C D x x₁ = tt , tt
θ₁-∧∨ A B D A x x₁ = tt , tt
θ₁-∧∨ A B D B x x₁ = tt , tt
θ₁-∧∨ A B D C x x₁ = tt , tt
θ₁-∧∨ A B D D x x₁ = tt , tt
θ₁-∧∨ A B O O x x₁ = tt , tt
θ₁-∧∨ A C Z Z x x₁ = tt , tt
θ₁-∧∨ A C A A x x₁ = tt , tt
θ₁-∧∨ A C A B x x₁ = tt , tt
θ₁-∧∨ A C A C x x₁ = tt , tt
θ₁-∧∨ A C A D x x₁ = tt , tt
θ₁-∧∨ A C B A x x₁ = tt , tt
θ₁-∧∨ A C B B x x₁ = tt , tt
θ₁-∧∨ A C B C x x₁ = tt , tt
θ₁-∧∨ A C B D x x₁ = tt , tt
θ₁-∧∨ A C C A x x₁ = tt , tt
θ₁-∧∨ A C C B x x₁ = tt , tt
θ₁-∧∨ A C C C x x₁ = tt , tt
θ₁-∧∨ A C C D x x₁ = tt , tt
θ₁-∧∨ A C D A x x₁ = tt , tt
θ₁-∧∨ A C D B x x₁ = tt , tt
θ₁-∧∨ A C D C x x₁ = tt , tt
θ₁-∧∨ A C D D x x₁ = tt , tt
θ₁-∧∨ A C O O x x₁ = tt , tt
θ₁-∧∨ A D Z Z x x₁ = tt , tt
θ₁-∧∨ A D A A x x₁ = tt , tt
θ₁-∧∨ A D A B x x₁ = tt , tt
θ₁-∧∨ A D A C x x₁ = tt , tt
θ₁-∧∨ A D A D x x₁ = tt , tt
θ₁-∧∨ A D B A x x₁ = tt , tt
θ₁-∧∨ A D B B x x₁ = tt , tt
θ₁-∧∨ A D B C x x₁ = tt , tt
θ₁-∧∨ A D B D x x₁ = tt , tt
θ₁-∧∨ A D C A x x₁ = tt , tt
θ₁-∧∨ A D C B x x₁ = tt , tt
θ₁-∧∨ A D C C x x₁ = tt , tt
θ₁-∧∨ A D C D x x₁ = tt , tt
θ₁-∧∨ A D D A x x₁ = tt , tt
θ₁-∧∨ A D D B x x₁ = tt , tt
θ₁-∧∨ A D D C x x₁ = tt , tt
θ₁-∧∨ A D D D x x₁ = tt , tt
θ₁-∧∨ A D O O x x₁ = tt , tt
θ₁-∧∨ B A Z Z x x₁ = tt , tt
θ₁-∧∨ B A A A x x₁ = tt , tt
θ₁-∧∨ B A A B x x₁ = tt , tt
θ₁-∧∨ B A A C x x₁ = tt , tt
θ₁-∧∨ B A A D x x₁ = tt , tt
θ₁-∧∨ B A B A x x₁ = tt , tt
θ₁-∧∨ B A B B x x₁ = tt , tt
θ₁-∧∨ B A B C x x₁ = tt , tt
θ₁-∧∨ B A B D x x₁ = tt , tt
θ₁-∧∨ B A C A x x₁ = tt , tt
θ₁-∧∨ B A C B x x₁ = tt , tt
θ₁-∧∨ B A C C x x₁ = tt , tt
θ₁-∧∨ B A C D x x₁ = tt , tt
θ₁-∧∨ B A D A x x₁ = tt , tt
θ₁-∧∨ B A D B x x₁ = tt , tt
θ₁-∧∨ B A D C x x₁ = tt , tt
θ₁-∧∨ B A D D x x₁ = tt , tt
θ₁-∧∨ B A O O x x₁ = tt , tt
θ₁-∧∨ B B Z Z x x₁ = tt , tt
θ₁-∧∨ B B A A x x₁ = tt , tt
θ₁-∧∨ B B A B x x₁ = tt , tt
θ₁-∧∨ B B A C x x₁ = tt , tt
θ₁-∧∨ B B A D x x₁ = tt , tt
θ₁-∧∨ B B B A x x₁ = tt , tt
θ₁-∧∨ B B B B x x₁ = tt , tt
θ₁-∧∨ B B B C x x₁ = tt , tt
θ₁-∧∨ B B B D x x₁ = tt , tt
θ₁-∧∨ B B C A x x₁ = tt , tt
θ₁-∧∨ B B C B x x₁ = tt , tt
θ₁-∧∨ B B C C x x₁ = tt , tt
θ₁-∧∨ B B C D x x₁ = tt , tt
θ₁-∧∨ B B D A x x₁ = tt , tt
θ₁-∧∨ B B D B x x₁ = tt , tt
θ₁-∧∨ B B D C x x₁ = tt , tt
θ₁-∧∨ B B D D x x₁ = tt , tt
θ₁-∧∨ B B O O x x₁ = tt , tt
θ₁-∧∨ B C Z Z x x₁ = tt , tt
θ₁-∧∨ B C A A x x₁ = tt , tt
θ₁-∧∨ B C A B x x₁ = tt , tt
θ₁-∧∨ B C A C x x₁ = tt , tt
θ₁-∧∨ B C A D x x₁ = tt , tt
θ₁-∧∨ B C B A x x₁ = tt , tt
θ₁-∧∨ B C B B x x₁ = tt , tt
θ₁-∧∨ B C B C x x₁ = tt , tt
θ₁-∧∨ B C B D x x₁ = tt , tt
θ₁-∧∨ B C C A x x₁ = tt , tt
θ₁-∧∨ B C C B x x₁ = tt , tt
θ₁-∧∨ B C C C x x₁ = tt , tt
θ₁-∧∨ B C C D x x₁ = tt , tt
θ₁-∧∨ B C D A x x₁ = tt , tt
θ₁-∧∨ B C D B x x₁ = tt , tt
θ₁-∧∨ B C D C x x₁ = tt , tt
θ₁-∧∨ B C D D x x₁ = tt , tt
θ₁-∧∨ B C O O x x₁ = tt , tt
θ₁-∧∨ B D Z Z x x₁ = tt , tt
θ₁-∧∨ B D A A x x₁ = tt , tt
θ₁-∧∨ B D A B x x₁ = tt , tt
θ₁-∧∨ B D A C x x₁ = tt , tt
θ₁-∧∨ B D A D x x₁ = tt , tt
θ₁-∧∨ B D B A x x₁ = tt , tt
θ₁-∧∨ B D B B x x₁ = tt , tt
θ₁-∧∨ B D B C x x₁ = tt , tt
θ₁-∧∨ B D B D x x₁ = tt , tt
θ₁-∧∨ B D C A x x₁ = tt , tt
θ₁-∧∨ B D C B x x₁ = tt , tt
θ₁-∧∨ B D C C x x₁ = tt , tt
θ₁-∧∨ B D C D x x₁ = tt , tt
θ₁-∧∨ B D D A x x₁ = tt , tt
θ₁-∧∨ B D D B x x₁ = tt , tt
θ₁-∧∨ B D D C x x₁ = tt , tt
θ₁-∧∨ B D D D x x₁ = tt , tt
θ₁-∧∨ B D O O x x₁ = tt , tt
θ₁-∧∨ C A Z Z x x₁ = tt , tt
θ₁-∧∨ C A A A x x₁ = tt , tt
θ₁-∧∨ C A A B x x₁ = tt , tt
θ₁-∧∨ C A A C x x₁ = tt , tt
θ₁-∧∨ C A A D x x₁ = tt , tt
θ₁-∧∨ C A B A x x₁ = tt , tt
θ₁-∧∨ C A B B x x₁ = tt , tt
θ₁-∧∨ C A B C x x₁ = tt , tt
θ₁-∧∨ C A B D x x₁ = tt , tt
θ₁-∧∨ C A C A x x₁ = tt , tt
θ₁-∧∨ C A C B x x₁ = tt , tt
θ₁-∧∨ C A C C x x₁ = tt , tt
θ₁-∧∨ C A C D x x₁ = tt , tt
θ₁-∧∨ C A D A x x₁ = tt , tt
θ₁-∧∨ C A D B x x₁ = tt , tt
θ₁-∧∨ C A D C x x₁ = tt , tt
θ₁-∧∨ C A D D x x₁ = tt , tt
θ₁-∧∨ C A O O x x₁ = tt , tt
θ₁-∧∨ C B Z Z x x₁ = tt , tt
θ₁-∧∨ C B A A x x₁ = tt , tt
θ₁-∧∨ C B A B x x₁ = tt , tt
θ₁-∧∨ C B A C x x₁ = tt , tt
θ₁-∧∨ C B A D x x₁ = tt , tt
θ₁-∧∨ C B B A x x₁ = tt , tt
θ₁-∧∨ C B B B x x₁ = tt , tt
θ₁-∧∨ C B B C x x₁ = tt , tt
θ₁-∧∨ C B B D x x₁ = tt , tt
θ₁-∧∨ C B C A x x₁ = tt , tt
θ₁-∧∨ C B C B x x₁ = tt , tt
θ₁-∧∨ C B C C x x₁ = tt , tt
θ₁-∧∨ C B C D x x₁ = tt , tt
θ₁-∧∨ C B D A x x₁ = tt , tt
θ₁-∧∨ C B D B x x₁ = tt , tt
θ₁-∧∨ C B D C x x₁ = tt , tt
θ₁-∧∨ C B D D x x₁ = tt , tt
θ₁-∧∨ C B O O x x₁ = tt , tt
θ₁-∧∨ C C Z Z x x₁ = tt , tt
θ₁-∧∨ C C A A x x₁ = tt , tt
θ₁-∧∨ C C A B x x₁ = tt , tt
θ₁-∧∨ C C A C x x₁ = tt , tt
θ₁-∧∨ C C A D x x₁ = tt , tt
θ₁-∧∨ C C B A x x₁ = tt , tt
θ₁-∧∨ C C B B x x₁ = tt , tt
θ₁-∧∨ C C B C x x₁ = tt , tt
θ₁-∧∨ C C B D x x₁ = tt , tt
θ₁-∧∨ C C C A x x₁ = tt , tt
θ₁-∧∨ C C C B x x₁ = tt , tt
θ₁-∧∨ C C C C x x₁ = tt , tt
θ₁-∧∨ C C C D x x₁ = tt , tt
θ₁-∧∨ C C D A x x₁ = tt , tt
θ₁-∧∨ C C D B x x₁ = tt , tt
θ₁-∧∨ C C D C x x₁ = tt , tt
θ₁-∧∨ C C D D x x₁ = tt , tt
θ₁-∧∨ C C O O x x₁ = tt , tt
θ₁-∧∨ C D Z Z x x₁ = tt , tt
θ₁-∧∨ C D A A x x₁ = tt , tt
θ₁-∧∨ C D A B x x₁ = tt , tt
θ₁-∧∨ C D A C x x₁ = tt , tt
θ₁-∧∨ C D A D x x₁ = tt , tt
θ₁-∧∨ C D B A x x₁ = tt , tt
θ₁-∧∨ C D B B x x₁ = tt , tt
θ₁-∧∨ C D B C x x₁ = tt , tt
θ₁-∧∨ C D B D x x₁ = tt , tt
θ₁-∧∨ C D C A x x₁ = tt , tt
θ₁-∧∨ C D C B x x₁ = tt , tt
θ₁-∧∨ C D C C x x₁ = tt , tt
θ₁-∧∨ C D C D x x₁ = tt , tt
θ₁-∧∨ C D D A x x₁ = tt , tt
θ₁-∧∨ C D D B x x₁ = tt , tt
θ₁-∧∨ C D D C x x₁ = tt , tt
θ₁-∧∨ C D D D x x₁ = tt , tt
θ₁-∧∨ C D O O x x₁ = tt , tt
θ₁-∧∨ D A Z Z x x₁ = tt , tt
θ₁-∧∨ D A A A x x₁ = tt , tt
θ₁-∧∨ D A A B x x₁ = tt , tt
θ₁-∧∨ D A A C x x₁ = tt , tt
θ₁-∧∨ D A A D x x₁ = tt , tt
θ₁-∧∨ D A B A x x₁ = tt , tt
θ₁-∧∨ D A B B x x₁ = tt , tt
θ₁-∧∨ D A B C x x₁ = tt , tt
θ₁-∧∨ D A B D x x₁ = tt , tt
θ₁-∧∨ D A C A x x₁ = tt , tt
θ₁-∧∨ D A C B x x₁ = tt , tt
θ₁-∧∨ D A C C x x₁ = tt , tt
θ₁-∧∨ D A C D x x₁ = tt , tt
θ₁-∧∨ D A D A x x₁ = tt , tt
θ₁-∧∨ D A D B x x₁ = tt , tt
θ₁-∧∨ D A D C x x₁ = tt , tt
θ₁-∧∨ D A D D x x₁ = tt , tt
θ₁-∧∨ D A O O x x₁ = tt , tt
θ₁-∧∨ D B Z Z x x₁ = tt , tt
θ₁-∧∨ D B A A x x₁ = tt , tt
θ₁-∧∨ D B A B x x₁ = tt , tt
θ₁-∧∨ D B A C x x₁ = tt , tt
θ₁-∧∨ D B A D x x₁ = tt , tt
θ₁-∧∨ D B B A x x₁ = tt , tt
θ₁-∧∨ D B B B x x₁ = tt , tt
θ₁-∧∨ D B B C x x₁ = tt , tt
θ₁-∧∨ D B B D x x₁ = tt , tt
θ₁-∧∨ D B C A x x₁ = tt , tt
θ₁-∧∨ D B C B x x₁ = tt , tt
θ₁-∧∨ D B C C x x₁ = tt , tt
θ₁-∧∨ D B C D x x₁ = tt , tt
θ₁-∧∨ D B D A x x₁ = tt , tt
θ₁-∧∨ D B D B x x₁ = tt , tt
θ₁-∧∨ D B D C x x₁ = tt , tt
θ₁-∧∨ D B D D x x₁ = tt , tt
θ₁-∧∨ D B O O x x₁ = tt , tt
θ₁-∧∨ D C Z Z x x₁ = tt , tt
θ₁-∧∨ D C A A x x₁ = tt , tt
θ₁-∧∨ D C A B x x₁ = tt , tt
θ₁-∧∨ D C A C x x₁ = tt , tt
θ₁-∧∨ D C A D x x₁ = tt , tt
θ₁-∧∨ D C B A x x₁ = tt , tt
θ₁-∧∨ D C B B x x₁ = tt , tt
θ₁-∧∨ D C B C x x₁ = tt , tt
θ₁-∧∨ D C B D x x₁ = tt , tt
θ₁-∧∨ D C C A x x₁ = tt , tt
θ₁-∧∨ D C C B x x₁ = tt , tt
θ₁-∧∨ D C C C x x₁ = tt , tt
θ₁-∧∨ D C C D x x₁ = tt , tt
θ₁-∧∨ D C D A x x₁ = tt , tt
θ₁-∧∨ D C D B x x₁ = tt , tt
θ₁-∧∨ D C D C x x₁ = tt , tt
θ₁-∧∨ D C D D x x₁ = tt , tt
θ₁-∧∨ D C O O x x₁ = tt , tt
θ₁-∧∨ D D Z Z x x₁ = tt , tt
θ₁-∧∨ D D A A x x₁ = tt , tt
θ₁-∧∨ D D A B x x₁ = tt , tt
θ₁-∧∨ D D A C x x₁ = tt , tt
θ₁-∧∨ D D A D x x₁ = tt , tt
θ₁-∧∨ D D B A x x₁ = tt , tt
θ₁-∧∨ D D B B x x₁ = tt , tt
θ₁-∧∨ D D B C x x₁ = tt , tt
θ₁-∧∨ D D B D x x₁ = tt , tt
θ₁-∧∨ D D C A x x₁ = tt , tt
θ₁-∧∨ D D C B x x₁ = tt , tt
θ₁-∧∨ D D C C x x₁ = tt , tt
θ₁-∧∨ D D C D x x₁ = tt , tt
θ₁-∧∨ D D D A x x₁ = tt , tt
θ₁-∧∨ D D D B x x₁ = tt , tt
θ₁-∧∨ D D D C x x₁ = tt , tt
θ₁-∧∨ D D D D x x₁ = tt , tt
θ₁-∧∨ D D O O x x₁ = tt , tt
θ₁-∧∨ O O Z Z x x₁ = tt , tt
θ₁-∧∨ O O A A x x₁ = tt , tt
θ₁-∧∨ O O A B x x₁ = tt , tt
θ₁-∧∨ O O A C x x₁ = tt , tt
θ₁-∧∨ O O A D x x₁ = tt , tt
θ₁-∧∨ O O B A x x₁ = tt , tt
θ₁-∧∨ O O B B x x₁ = tt , tt
θ₁-∧∨ O O B C x x₁ = tt , tt
θ₁-∧∨ O O B D x x₁ = tt , tt
θ₁-∧∨ O O C A x x₁ = tt , tt
θ₁-∧∨ O O C B x x₁ = tt , tt
θ₁-∧∨ O O C C x x₁ = tt , tt
θ₁-∧∨ O O C D x x₁ = tt , tt
θ₁-∧∨ O O D A x x₁ = tt , tt
θ₁-∧∨ O O D B x x₁ = tt , tt
θ₁-∧∨ O O D C x x₁ = tt , tt
θ₁-∧∨ O O D D x x₁ = tt , tt
θ₁-∧∨ O O O O x x₁ = tt , tt

θ₂-∧∨ : ∀ a₁ b₁ a₂ b₂ → θ₂ a₁ b₁ → θ₂ a₂ b₂ → θ₂ (a₁ ∧ a₂) (b₁ ∧ b₂) × θ₂ (a₁ ∨ a₂) (b₁ ∨ b₂)
θ₂-∧∨ Z Z Z Z x x₁ = tt , tt
θ₂-∧∨ Z Z Z D x x₁ = tt , tt
θ₂-∧∨ Z Z A A x x₁ = tt , tt
θ₂-∧∨ Z Z B B x x₁ = tt , tt
θ₂-∧∨ Z Z C C x x₁ = tt , tt
θ₂-∧∨ Z Z C O x x₁ = tt , tt
θ₂-∧∨ Z Z D Z x x₁ = tt , tt
θ₂-∧∨ Z Z D D x x₁ = tt , tt
θ₂-∧∨ Z Z O C x x₁ = tt , tt
θ₂-∧∨ Z Z O O x x₁ = tt , tt
θ₂-∧∨ Z D Z Z x x₁ = tt , tt
θ₂-∧∨ Z D Z D x x₁ = tt , tt
θ₂-∧∨ Z D A A x x₁ = tt , tt
θ₂-∧∨ Z D B B x x₁ = tt , tt
θ₂-∧∨ Z D C C x x₁ = tt , tt
θ₂-∧∨ Z D C O x x₁ = tt , tt
θ₂-∧∨ Z D D Z x x₁ = tt , tt
θ₂-∧∨ Z D D D x x₁ = tt , tt
θ₂-∧∨ Z D O C x x₁ = tt , tt
θ₂-∧∨ Z D O O x x₁ = tt , tt
θ₂-∧∨ A A Z Z x x₁ = tt , tt
θ₂-∧∨ A A Z D x x₁ = tt , tt
θ₂-∧∨ A A A A x x₁ = tt , tt
θ₂-∧∨ A A B B x x₁ = tt , tt
θ₂-∧∨ A A C C x x₁ = tt , tt
θ₂-∧∨ A A C O x x₁ = tt , tt
θ₂-∧∨ A A D Z x x₁ = tt , tt
θ₂-∧∨ A A D D x x₁ = tt , tt
θ₂-∧∨ A A O C x x₁ = tt , tt
θ₂-∧∨ A A O O x x₁ = tt , tt
θ₂-∧∨ B B Z Z x x₁ = tt , tt
θ₂-∧∨ B B Z D x x₁ = tt , tt
θ₂-∧∨ B B A A x x₁ = tt , tt
θ₂-∧∨ B B B B x x₁ = tt , tt
θ₂-∧∨ B B C C x x₁ = tt , tt
θ₂-∧∨ B B C O x x₁ = tt , tt
θ₂-∧∨ B B D Z x x₁ = tt , tt
θ₂-∧∨ B B D D x x₁ = tt , tt
θ₂-∧∨ B B O C x x₁ = tt , tt
θ₂-∧∨ B B O O x x₁ = tt , tt
θ₂-∧∨ C C Z Z x x₁ = tt , tt
θ₂-∧∨ C C Z D x x₁ = tt , tt
θ₂-∧∨ C C A A x x₁ = tt , tt
θ₂-∧∨ C C B B x x₁ = tt , tt
θ₂-∧∨ C C C C x x₁ = tt , tt
θ₂-∧∨ C C C O x x₁ = tt , tt
θ₂-∧∨ C C D Z x x₁ = tt , tt
θ₂-∧∨ C C D D x x₁ = tt , tt
θ₂-∧∨ C C O C x x₁ = tt , tt
θ₂-∧∨ C C O O x x₁ = tt , tt
θ₂-∧∨ C O Z Z x x₁ = tt , tt
θ₂-∧∨ C O Z D x x₁ = tt , tt
θ₂-∧∨ C O A A x x₁ = tt , tt
θ₂-∧∨ C O B B x x₁ = tt , tt
θ₂-∧∨ C O C C x x₁ = tt , tt
θ₂-∧∨ C O C O x x₁ = tt , tt
θ₂-∧∨ C O D Z x x₁ = tt , tt
θ₂-∧∨ C O D D x x₁ = tt , tt
θ₂-∧∨ C O O C x x₁ = tt , tt
θ₂-∧∨ C O O O x x₁ = tt , tt
θ₂-∧∨ D Z Z Z x x₁ = tt , tt
θ₂-∧∨ D Z Z D x x₁ = tt , tt
θ₂-∧∨ D Z A A x x₁ = tt , tt
θ₂-∧∨ D Z B B x x₁ = tt , tt
θ₂-∧∨ D Z C C x x₁ = tt , tt
θ₂-∧∨ D Z C O x x₁ = tt , tt
θ₂-∧∨ D Z D Z x x₁ = tt , tt
θ₂-∧∨ D Z D D x x₁ = tt , tt
θ₂-∧∨ D Z O C x x₁ = tt , tt
θ₂-∧∨ D Z O O x x₁ = tt , tt
θ₂-∧∨ D D Z Z x x₁ = tt , tt
θ₂-∧∨ D D Z D x x₁ = tt , tt
θ₂-∧∨ D D A A x x₁ = tt , tt
θ₂-∧∨ D D B B x x₁ = tt , tt
θ₂-∧∨ D D C C x x₁ = tt , tt
θ₂-∧∨ D D C O x x₁ = tt , tt
θ₂-∧∨ D D D Z x x₁ = tt , tt
θ₂-∧∨ D D D D x x₁ = tt , tt
θ₂-∧∨ D D O C x x₁ = tt , tt
θ₂-∧∨ D D O O x x₁ = tt , tt
θ₂-∧∨ O C Z Z x x₁ = tt , tt
θ₂-∧∨ O C Z D x x₁ = tt , tt
θ₂-∧∨ O C A A x x₁ = tt , tt
θ₂-∧∨ O C B B x x₁ = tt , tt
θ₂-∧∨ O C C C x x₁ = tt , tt
θ₂-∧∨ O C C O x x₁ = tt , tt
θ₂-∧∨ O C D Z x x₁ = tt , tt
θ₂-∧∨ O C D D x x₁ = tt , tt
θ₂-∧∨ O C O C x x₁ = tt , tt
θ₂-∧∨ O C O O x x₁ = tt , tt
θ₂-∧∨ O O Z Z x x₁ = tt , tt
θ₂-∧∨ O O Z D x x₁ = tt , tt
θ₂-∧∨ O O A A x x₁ = tt , tt
θ₂-∧∨ O O B B x x₁ = tt , tt
θ₂-∧∨ O O C C x x₁ = tt , tt
θ₂-∧∨ O O C O x x₁ = tt , tt
θ₂-∧∨ O O D Z x x₁ = tt , tt
θ₂-∧∨ O O D D x x₁ = tt , tt
θ₂-∧∨ O O O C x x₁ = tt , tt
θ₂-∧∨ O O O O x x₁ = tt , tt

θ₁-compat : ∀ a b → θ₁ a b → a ≡ O → b ≡ O
θ₁-compat O O x y = refl

∈⦃1,c⦄ : 𝑩 → Set
∈⦃1,c⦄ O = ⊤
∈⦃1,c⦄ C = ⊤
{-# CATCHALL #-}
∈⦃1,c⦄ x = ⊥

∈⦃1⦄ : 𝑩 → Set
∈⦃1⦄ O = ⊤
{-# CATCHALL #-}
∈⦃1⦄ _ = ⊥

θ₂-compat : ∀ a b → θ₂ a b → ∈⦃1,c⦄ a → ∈⦃1,c⦄ b
θ₂-compat C C x x₁ = tt
θ₂-compat C O x x₁ = tt
θ₂-compat O C x x₁ = tt
θ₂-compat O O x x₁ = tt

greatest-θ₁ : ∀ (θ' : Rel 𝑩 lzero) r t
  → (∀ {a₁ b₁ a₂ b₂} → θ' a₁ b₁ → θ' a₂ b₂ → θ' (a₁ ∧ a₂) (b₁ ∧ b₂) × θ' (a₁ ∨ a₂) (b₁ ∨ b₂))
  → (∀ {a b} → θ' a b → θ' (¬ a) (¬ b))
  → (∀ {a b} {_ : ∈⦃1⦄ a} → θ' a b → ∈⦃1⦄ b)
  → (∀ {a} → θ' a a)
  → (∀ {a b} → θ' a b → θ' b a)
  → (∀ {a b c} → θ' a b → θ' b c → θ' a c)
  → θ' r t → θ₁ r t
greatest-θ₁ θ' Z Z ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' Z A ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₁ θ' Z B ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₁ θ' Z C ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₁ θ' Z D ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₁ θ' Z O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₁ θ' A Z ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat ∘ sym) p
greatest-θ₁ θ' A A ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' A B ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' A C ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' A D ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' A O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₁ θ' B Z ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat ∘ sym) p
greatest-θ₁ θ' B A ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' B B ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' B C ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' B D ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' B O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₁ θ' C Z ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat ∘ sym) p
greatest-θ₁ θ' C A ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' C B ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' C C ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' C D ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' C O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₁ θ' D Z ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat ∘ sym) p
greatest-θ₁ θ' D A ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' D B ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' D C ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' D D ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₁ θ' D O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₁ θ' O Z ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₁ θ' O A ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₁ θ' O B ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₁ θ' O C ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₁ θ' O D ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₁ θ' O O ∧∨compat ¬compat compatF ref sym trans p = tt

greatest-θ₂ : ∀ (θ' : Rel 𝑩 lzero) r t
  → (∀ {a₁ b₁ a₂ b₂} → θ' a₁ b₁ → θ' a₂ b₂ → θ' (a₁ ∧ a₂) (b₁ ∧ b₂) × θ' (a₁ ∨ a₂) (b₁ ∨ b₂))
  → (∀ {a b} → θ' a b → θ' (¬ a) (¬ b))
  → (∀ {a b} {_ : ∈⦃1,c⦄ a} → θ' a b → ∈⦃1,c⦄ b)
  → (∀ {a} → θ' a a)
  → (∀ {a b} → θ' a b → θ' b a)
  → (∀ {a b c} → θ' a b → θ' b c → θ' a c)
  → θ' r t → θ₂ r t
greatest-θ₂ θ' Z Z ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' Z A ∧∨compat ¬compat compatF ref sym trans p = compatF (¬compat p)
greatest-θ₂ θ' Z B ∧∨compat ¬compat compatF ref sym trans p = compatF (¬compat p)
greatest-θ₂ θ' Z C ∧∨compat ¬compat compatF ref sym trans p = compatF (¬compat p)
greatest-θ₂ θ' Z D ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' Z O ∧∨compat ¬compat compatF ref sym trans p = compatF (¬compat p)
greatest-θ₂ θ' A Z ∧∨compat ¬compat compatF ref sym trans p = compatF (sym (¬compat p))
greatest-θ₂ θ' A A ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' A B ∧∨compat ¬compat compatF ref sym trans p = compatF (sym (¬compat (proj₁ (∧∨compat p (ref {A})))))
greatest-θ₂ θ' A C ∧∨compat ¬compat compatF ref sym trans p = compatF (sym p)
greatest-θ₂ θ' A D ∧∨compat ¬compat compatF ref sym trans p = compatF (¬compat (sym p))
greatest-θ₂ θ' A O ∧∨compat ¬compat compatF ref sym trans p = compatF (sym p)
greatest-θ₂ θ' B Z ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat ∘ sym ) p
greatest-θ₂ θ' B A ∧∨compat ¬compat compatF ref sym trans p = compatF (sym (¬compat (proj₁ (∧∨compat (sym p) (ref {A})))))
greatest-θ₂ θ' B B ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' B C ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₂ θ' B D ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat ∘ sym) p
greatest-θ₂ θ' B O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₂ θ' C Z ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' C A ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' C B ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' C C ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' C D ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' C O ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' D Z ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' D A ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₂ θ' D B ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₂ θ' D C ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ sym) p
greatest-θ₂ θ' D D ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' D O ∧∨compat ¬compat compatF ref sym trans p = (compatF ∘ ¬compat) p
greatest-θ₂ θ' O Z ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' O A ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' O B ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' O C ∧∨compat ¬compat compatF ref sym trans p = tt
greatest-θ₂ θ' O D ∧∨compat ¬compat compatF ref sym trans p = compatF p
greatest-θ₂ θ' O O ∧∨compat ¬compat compatF ref sym trans p = tt


----


_≤_ : Rel 𝑩 lzero
x ≤ y = x ∧ y ≡ x

reflexive : Reflexive _≤_
reflexive {Z} = refl
reflexive {A} = refl
reflexive {B} = refl
reflexive {C} = refl
reflexive {D} = refl
reflexive {O} = refl

transitive : Transitive _≤_
transitive {Z} {b} {c} x x₁ = refl
transitive {A} {A} {c} x x₁ = x₁
transitive {A} {C} {C} x x₁ = refl
transitive {A} {C} {O} x x₁ = refl
transitive {A} {O} {O} x x₁ = refl
transitive {B} {B} {c} x x₁ = x₁
transitive {B} {C} {C} x x₁ = refl
transitive {B} {C} {O} x x₁ = refl
transitive {B} {O} {O} x x₁ = refl
transitive {C} {C} {c} x x₁ = x₁
transitive {C} {O} {O} x x₁ = refl
transitive {D} {A} {A} x x₁ = refl
transitive {D} {A} {C} x x₁ = refl
transitive {D} {A} {O} x x₁ = refl
transitive {D} {B} {B} x x₁ = refl
transitive {D} {B} {C} x x₁ = refl
transitive {D} {B} {O} x x₁ = refl
transitive {D} {C} {C} x x₁ = refl
transitive {D} {C} {O} x x₁ = refl
transitive {D} {D} {A} x x₁ = refl
transitive {D} {D} {B} x x₁ = refl
transitive {D} {D} {C} x x₁ = refl
transitive {D} {D} {D} x x₁ = refl
transitive {D} {D} {O} x x₁ = refl
transitive {D} {O} {O} x x₁ = refl
transitive {O} {O} {c} x x₁ = x₁

antisym : Antisymmetric _≡_ _≤_
antisym {Z} {Z} x y = refl
antisym {A} {A} x y = refl
antisym {B} {B} x y = refl
antisym {C} {C} x y = refl
antisym {D} {D} x y = refl
antisym {O} {O} x y = refl

sup2 : ∀ a b z → b ≤ z → a ≤ z → (a ∨ b) ≤ z
sup2 Z b z x y = x
sup2 A Z z x y = y
sup2 A A z x y = y
sup2 A B C x y = refl
sup2 A B O x y = refl
sup2 A C C x y = refl
sup2 A C O x y = refl
sup2 A D z x y = y
sup2 A O z x y = x
sup2 B Z z x y = y
sup2 B A C x y = refl
sup2 B A O x y = refl
sup2 B B z x y = y
sup2 B C z x y = x
sup2 B D z x y = y
sup2 B O z x y = x
sup2 C Z z x y = y
sup2 C A z x y = y
sup2 C B z x y = y
sup2 C C z x y = y
sup2 C D z x y = y
sup2 C O z x y = x
sup2 D Z z x y = y
sup2 D A z x y = x
sup2 D B z x y = x
sup2 D C z x y = x
sup2 D D z x y = y
sup2 D O z x y = x
sup2 O b z x y = y

inf2 : ∀ a b z → z ≤ a → z ≤ b → z ≤ (a ∧ b)
inf2 Z b z x y = x
inf2 A Z z x y = y
inf2 A A z x y = y
inf2 A B Z x y = refl
inf2 A B D x y = refl
inf2 A C z x y = x
inf2 A D z x y = y
inf2 A O z x y = x
inf2 B Z z x y = y
inf2 B A Z x y = refl
inf2 B A D x y = refl
inf2 B B z x y = y
inf2 B C z x y = x
inf2 B D z x y = y
inf2 B O z x y = x
inf2 C Z z x y = y
inf2 C A z x y = y
inf2 C B z x y = y
inf2 C C z x y = y
inf2 C D z x y = y
inf2 C O z x y = x
inf2 D Z z x y = y
inf2 D A z x y = x
inf2 D B z x y = x
inf2 D C z x y = x
inf2 D D z x y = y
inf2 D O z x y = x
inf2 O b z x y = y

lat1 : ∀ a b → a ≤ (a ∨ b) × b ≤ (a ∨ b) × (a ∧ b) ≤ a × (a ∧ b) ≤ b
lat1 Z Z = refl , refl , refl , refl
lat1 Z A = refl , refl , refl , refl
lat1 Z B = refl , refl , refl , refl
lat1 Z C = refl , refl , refl , refl
lat1 Z D = refl , refl , refl , refl
lat1 Z O = refl , refl , refl , refl
lat1 A Z = refl , refl , refl , refl
lat1 A A = refl , refl , refl , refl
lat1 A B = refl , refl , refl , refl
lat1 A C = refl , refl , refl , refl
lat1 A D = refl , refl , refl , refl
lat1 A O = refl , refl , refl , refl
lat1 B Z = refl , refl , refl , refl
lat1 B A = refl , refl , refl , refl
lat1 B B = refl , refl , refl , refl
lat1 B C = refl , refl , refl , refl
lat1 B D = refl , refl , refl , refl
lat1 B O = refl , refl , refl , refl
lat1 C Z = refl , refl , refl , refl
lat1 C A = refl , refl , refl , refl
lat1 C B = refl , refl , refl , refl
lat1 C C = refl , refl , refl , refl
lat1 C D = refl , refl , refl , refl
lat1 C O = refl , refl , refl , refl
lat1 D Z = refl , refl , refl , refl
lat1 D A = refl , refl , refl , refl
lat1 D B = refl , refl , refl , refl
lat1 D C = refl , refl , refl , refl
lat1 D D = refl , refl , refl , refl
lat1 D O = refl , refl , refl , refl
lat1 O Z = refl , refl , refl , refl
lat1 O A = refl , refl , refl , refl
lat1 O B = refl , refl , refl , refl
lat1 O C = refl , refl , refl , refl
lat1 O D = refl , refl , refl , refl
lat1 O O = refl , refl , refl , refl

supremum : Supremum _≤_ _∨_
supremum a b = proj₁ (lat1 a b) , proj₁ (proj₂ (lat1 a b)) , λ z x y → sup2 a b z y x

infimum : Infimum _≤_ _∧_
infimum a b = (proj₁ ∘ proj₂ ∘ proj₂) (lat1 a b) , ((proj₂ ∘ proj₂ ∘ proj₂) (lat1 a b) , inf2 a b)

lattice : IsLattice _≡_ _≤_ _∨_ _∧_
lattice = record {
  isPartialOrder =
  record { isPreorder =
  record { isEquivalence =
  record { refl = λ {x} → refl ;
    sym = λ { refl → refl };
    trans = λ { refl refl → refl } }
    ; reflexive = λ { refl → reflexive}
    ; trans = transitive }
    ; antisym = antisym }
    ; supremum = supremum
    ; infimum = infimum }

distrib : ∀ x y z → (x ∧ (y ∨ z)) ≡ ((x ∧ y) ∨ (x ∧ z))
distrib Z y z = refl
distrib A Z z = refl
distrib A A Z = refl
distrib A A A = refl
distrib A A B = refl
distrib A A C = refl
distrib A A D = refl
distrib A A O = refl
distrib A B Z = refl
distrib A B A = refl
distrib A B B = refl
distrib A B C = refl
distrib A B D = refl
distrib A B O = refl
distrib A C Z = refl
distrib A C A = refl
distrib A C B = refl
distrib A C C = refl
distrib A C D = refl
distrib A C O = refl
distrib A D Z = refl
distrib A D A = refl
distrib A D B = refl
distrib A D C = refl
distrib A D D = refl
distrib A D O = refl
distrib A O Z = refl
distrib A O A = refl
distrib A O B = refl
distrib A O C = refl
distrib A O D = refl
distrib A O O = refl
distrib B Z Z = refl
distrib B Z A = refl
distrib B Z B = refl
distrib B Z C = refl
distrib B Z D = refl
distrib B Z O = refl
distrib B A Z = refl
distrib B A A = refl
distrib B A B = refl
distrib B A C = refl
distrib B A D = refl
distrib B A O = refl
distrib B B Z = refl
distrib B B A = refl
distrib B B B = refl
distrib B B C = refl
distrib B B D = refl
distrib B B O = refl
distrib B C Z = refl
distrib B C A = refl
distrib B C B = refl
distrib B C C = refl
distrib B C D = refl
distrib B C O = refl
distrib B D Z = refl
distrib B D A = refl
distrib B D B = refl
distrib B D C = refl
distrib B D D = refl
distrib B D O = refl
distrib B O Z = refl
distrib B O A = refl
distrib B O B = refl
distrib B O C = refl
distrib B O D = refl
distrib B O O = refl
distrib C Z Z = refl
distrib C Z A = refl
distrib C Z B = refl
distrib C Z C = refl
distrib C Z D = refl
distrib C Z O = refl
distrib C A Z = refl
distrib C A A = refl
distrib C A B = refl
distrib C A C = refl
distrib C A D = refl
distrib C A O = refl
distrib C B Z = refl
distrib C B A = refl
distrib C B B = refl
distrib C B C = refl
distrib C B D = refl
distrib C B O = refl
distrib C C Z = refl
distrib C C A = refl
distrib C C B = refl
distrib C C C = refl
distrib C C D = refl
distrib C C O = refl
distrib C D Z = refl
distrib C D A = refl
distrib C D B = refl
distrib C D C = refl
distrib C D D = refl
distrib C D O = refl
distrib C O Z = refl
distrib C O A = refl
distrib C O B = refl
distrib C O C = refl
distrib C O D = refl
distrib C O O = refl
distrib D Z Z = refl
distrib D Z A = refl
distrib D Z B = refl
distrib D Z C = refl
distrib D Z D = refl
distrib D Z O = refl
distrib D A Z = refl
distrib D A A = refl
distrib D A B = refl
distrib D A C = refl
distrib D A D = refl
distrib D A O = refl
distrib D B Z = refl
distrib D B A = refl
distrib D B B = refl
distrib D B C = refl
distrib D B D = refl
distrib D B O = refl
distrib D C Z = refl
distrib D C A = refl
distrib D C B = refl
distrib D C C = refl
distrib D C D = refl
distrib D C O = refl
distrib D D Z = refl
distrib D D A = refl
distrib D D B = refl
distrib D D C = refl
distrib D D D = refl
distrib D D O = refl
distrib D O Z = refl
distrib D O A = refl
distrib D O B = refl
distrib D O C = refl
distrib D O D = refl
distrib D O O = refl
distrib O y z = refl

dlattice : IsDistributiveLattice _≡_ _≤_ _∨_ _∧_
dlattice = record { isLattice = lattice ; ∧-distribˡ-∨ = distrib }

¬¬-elim : ∀ (a) → ¬ (¬ a) ≡ a
¬¬-elim Z = refl
¬¬-elim A = refl
¬¬-elim B = refl
¬¬-elim C = refl
¬¬-elim D = refl
¬¬-elim O = refl

de-morgan-1 : ∀ a b → ¬ (a ∨ b) ≡ ¬ a ∧ ¬ b
de-morgan-1 Z b = refl
de-morgan-1 A Z = refl
de-morgan-1 A A = refl
de-morgan-1 A B = refl
de-morgan-1 A C = refl
de-morgan-1 A D = refl
de-morgan-1 A O = refl
de-morgan-1 B Z = refl
de-morgan-1 B A = refl
de-morgan-1 B B = refl
de-morgan-1 B C = refl
de-morgan-1 B D = refl
de-morgan-1 B O = refl
de-morgan-1 C Z = refl
de-morgan-1 C A = refl
de-morgan-1 C B = refl
de-morgan-1 C C = refl
de-morgan-1 C D = refl
de-morgan-1 C O = refl
de-morgan-1 D Z = refl
de-morgan-1 D A = refl
de-morgan-1 D B = refl
de-morgan-1 D C = refl
de-morgan-1 D D = refl
de-morgan-1 D O = refl
de-morgan-1 O b = refl

de-morgan-2 : ∀ a b → ¬ (a ∧ b) ≡ ¬ a ∨ ¬ b
de-morgan-2 Z b = refl
de-morgan-2 A Z = refl
de-morgan-2 A A = refl
de-morgan-2 A B = refl
de-morgan-2 A C = refl
de-morgan-2 A D = refl
de-morgan-2 A O = refl
de-morgan-2 B Z = refl
de-morgan-2 B A = refl
de-morgan-2 B B = refl
de-morgan-2 B C = refl
de-morgan-2 B D = refl
de-morgan-2 B O = refl
de-morgan-2 C Z = refl
de-morgan-2 C A = refl
de-morgan-2 C B = refl
de-morgan-2 C C = refl
de-morgan-2 C D = refl
de-morgan-2 C O = refl
de-morgan-2 D Z = refl
de-morgan-2 D A = refl
de-morgan-2 D B = refl
de-morgan-2 D C = refl
de-morgan-2 D D = refl
de-morgan-2 D O = refl
de-morgan-2 O b = refl
