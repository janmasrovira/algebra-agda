module AlgebraicLogic.PropositionalLogic where

data Fm (X : Set) : Set where
  _! : X → Fm X
  _↝_ : Fm X → Fm X → Fm X
  ⊥ : Fm X

¬ : ∀ {X : Set} → Fm X → Fm X
¬ ϕ = ϕ ↝ ⊥

⊤ : ∀ {X : Set} → Fm X
⊤ = ¬ ⊥

subs : ∀ {X : Set} (f : X → Fm X) → Fm X → Fm X
subs f (x !) = f x
subs f (x ↝ y) = subs f x ↝ subs f y
subs f ⊥ = ⊥

