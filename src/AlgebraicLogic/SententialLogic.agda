module AlgebraicLogic.SententialLogic where

import Data.Empty as Empty
import Data.Unit as Unit
open import AlgebraicLogic.PropositionalLogic
open import Level using (Level; suc; zero)
open import Relation.Nullary using (yes; no; Dec)
open import Relation.Nullary.Decidable using (True)
open import Relation.Unary using (Pred; Decidable)

map_[_] : ∀ {l} {X : Set} {P : Pred (Fm X) l} (σ : Fm X → Fm X) → Decidable P → (∀ ϕ → Dec (P (σ ϕ)))
map σ [ dec ] ϕ = dec (σ ϕ)

_∈?_ : ∀ {l} {X : Set} {P : Pred X l} → (x : X) → Decidable P → Dec (P x)
ϕ ∈? Γ = Γ ϕ

_∈_ : ∀ {l X} {P : Pred X l} → (x : X) → Decidable P → Set
ϕ ∈ Γ = True (ϕ ∈? Γ)

data _⊆_ {l} {X : Set} {P1 P2 : Pred X l} (Γ : Decidable P1) (Δ : Decidable P2) : Set where
  ⊆-prf : (∀ {x : X} → x ∈ Γ → x ∈ Δ) → Γ ⊆ Δ

Logic : ∀ (l : Level) (X : Set) → Set (suc l)
Logic l X = ∀ {P : Pred (Fm X) l} → Decidable P → Fm X → Set


record SententialLogic {l : Level} {X : Set}
  (_⊢_ : Logic l X) : Set (suc l) where
  constructor sentential
  field
    identity : ∀ {P : Pred (Fm X) l} → {Γ : Decidable P} → (ϕ : Fm X)
      → ϕ ∈ Γ → Γ ⊢ ϕ
    cut : ∀ {ϕ : Fm X} {P1 P2 : Pred (Fm X) l} → {Γ : Decidable P1} → {Δ : Decidable P2}
      → Γ ⊢ ϕ → (∀ (ψ : Fm (X)) → ψ ∈ Γ → Δ ⊢ ψ) → Δ ⊢ ϕ
    invariant : ∀ {ϕ : Fm X} {P : Pred (Fm X) l} → (Γ : Decidable P)
      → Γ ⊢ ϕ → (σ : X → Fm X) → map (subs σ) [ Γ ] ⊢ subs σ ϕ

monotonicity : ∀ {l X} {_⊢_ : Logic l X} {ϕ : Fm X} {P1 P2 : Pred (Fm X) l} (S : SententialLogic {l} {X} _⊢_)
  →  (Γ : Decidable P1) → (Δ : Decidable P2) → Γ ⊆ Δ → Γ ⊢ ϕ → Δ ⊢ ϕ
monotonicity S Γ Δ (⊆-prf Γ⊆Δ) Γ⊢ϕ = SententialLogic.cut S Γ⊢ϕ λ ψ ψ∈Γ
  → SententialLogic.identity S ψ (Γ⊆Δ ψ∈Γ)
